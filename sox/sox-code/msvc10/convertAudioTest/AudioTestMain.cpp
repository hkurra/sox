
#include "sox.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

//#define FUNTEST(x,y) printf("sdfsdd");
int main(int argc, char * argv[])
{
	static sox_format_t * in, *out; /* input and output files */
	sox_effects_chain_t * chain;
	sox_signalinfo_t interm_signal;
	sox_effect_t * e;
	char * args[10];
	//char * inPath = "";
	//char * outPath = "";

	sox_init();

	/* Open the input file (with default parameters) */
	in = sox_open_read(argv[1], NULL, NULL, NULL);

	/* Open the output file; we must specify the output signal characteristics.
	* Since we are using only simple effects, they are the same as the input
	* file characteristics */
	out = sox_open_write(argv[2], &in->signal, NULL, NULL, NULL, NULL);

	chain = sox_create_effects_chain(&in->encoding, &out->encoding);

	 interm_signal = in->signal; /* NB: deep copy */

  e = sox_create_effect(sox_find_effect("input"));
  args[0] = (char *)in, assert(sox_effect_options(e, 1, args) == SOX_SUCCESS);
  assert(sox_add_effect(chain, e, &interm_signal, &in->signal) == SOX_SUCCESS);
  free(e);

   if (in->signal.channels != out->signal.channels) {
    e = sox_create_effect(sox_find_effect("channels"));
    assert(sox_effect_options(e, 0, NULL) == SOX_SUCCESS);
    assert(sox_add_effect(chain, e, &interm_signal, &out->signal) == SOX_SUCCESS);
    free(e);
  }

   if (in->signal.rate != out->signal.rate) {
    e = sox_create_effect(sox_find_effect("rate"));
    assert(sox_effect_options(e, 0, NULL) == SOX_SUCCESS);
    assert(sox_add_effect(chain, e, &interm_signal, &out->signal) == SOX_SUCCESS);
    free(e);
  }

    e = sox_create_effect(sox_find_effect("output"));
  args[0] = (char *)out, assert(sox_effect_options(e, 1, args) == SOX_SUCCESS);
  assert(sox_add_effect(chain, e, &interm_signal, &out->signal) == SOX_SUCCESS);
  free(e);

	

	sox_flow_effects(chain, NULL, NULL);

	sox_delete_effects_chain(chain);
	
	sox_close(out);
	sox_close(in);
	sox_quit();
	return 0;
}