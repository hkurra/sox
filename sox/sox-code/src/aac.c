
#include "sox_i.h"
#include <neaacdec.h>
#include <mp4ff.h>

#define MAX_CHANNELS 6 /* make this higher to support files with
                          more channels */

    int def_srate = 0;
    int downMatrix = 0;
    int format = 1;
	int outputFormat = FAAD_FMT_16BIT;
    int old_format = 0;
	int object_type = LC;
	int header_type = 0;
	NeAACDecHandle hDecoder;

/* FAAD file buffering routines */
typedef struct {
    long bytes_into_buffer;
    long bytes_consumed;
    long file_offset;
    unsigned char *buffer;
    int at_eof;
	int mp4file;
	mp4ff_t *infile;
	int track;
	unsigned long timescale;
	unsigned int framesize;
	mp4ff_callback_t *mp4cb;
	long sampleId;

} priv_t;

uint32_t read_callback(void *user_data, void *buffer, uint32_t length)
{
	sox_format_t* ft = (sox_format_t*)user_data;
	size_t ret = lsx_readbuf(ft, buffer, length);
	return ret ;
}

uint32_t seek_callback(void *user_data, uint64_t position)
{
	sox_format_t* ft = (sox_format_t*)user_data;
	int ret = ft->seekable ? lsx_seeki(ft, (off_t)position, SEEK_SET) : -1;

	if (ret == EBADF)
		ret = -1;
	return ret;    
}

double adts_sample_rates[] = {96000,88200,64000,48000,44100,32000,24000,22050,16000,12000,11025,8000,7350,0,0,0};

static int fill_buffer(sox_format_t * ft)
{
    int bread;
	priv_t *b = (priv_t *) ft->priv;

    if (b->bytes_consumed > 0)
    {
        if (b->bytes_into_buffer)
        {
            memmove((void*)b->buffer, (void*)(b->buffer + b->bytes_consumed),
                b->bytes_into_buffer*sizeof(unsigned char));
        }

        if (!b->at_eof)
        {
			bread = (int)lsx_readbuf(ft, (void*)(b->buffer + b->bytes_into_buffer), b->bytes_consumed);
            /*bread = fread((void*)(b->buffer + b->bytes_into_buffer), 1,
                b->bytes_consumed, b->infile);*/

            if (bread != b->bytes_consumed)
                b->at_eof = 1;

            b->bytes_into_buffer += bread;
        }

        b->bytes_consumed = 0;

        if (b->bytes_into_buffer > 3)
        {
            if (memcmp(b->buffer, "TAG", 3) == 0)
                b->bytes_into_buffer = 0;
        }
        if (b->bytes_into_buffer > 11)
        {
            if (memcmp(b->buffer, "LYRICSBEGIN", 11) == 0)
                b->bytes_into_buffer = 0;
        }
        if (b->bytes_into_buffer > 8)
        {
            if (memcmp(b->buffer, "APETAGEX", 8) == 0)
                b->bytes_into_buffer = 0;
        }
    }

    return 1;
}

void advance_buffer(priv_t *b, int bytes)
{
    b->file_offset += bytes;
    b->bytes_consumed = bytes;
    b->bytes_into_buffer -= bytes;
	if (b->bytes_into_buffer < 0)
		b->bytes_into_buffer = 0;
}

int adts_parse(sox_format_t *ft, int *bitrate, float *length)
{
	priv_t *b = (priv_t *) ft->priv;
    int frames, frame_length;
    int t_framelength = 0;
    int samplerate;
    float frames_per_sec, bytes_per_frame;

    /* Read all frames to ensure correct time and bitrate */
    for (frames = 0; /* */; frames++)
    {
        fill_buffer(ft);

        if (b->bytes_into_buffer > 7)
        {
            /* check syncword */
            if (!((b->buffer[0] == 0xFF)&&((b->buffer[1] & 0xF6) == 0xF0)))
                break;

            if (frames == 0)
                samplerate = adts_sample_rates[(b->buffer[2]&0x3c)>>2];

            frame_length = ((((unsigned int)b->buffer[3] & 0x3)) << 11)
                | (((unsigned int)b->buffer[4]) << 3) | (b->buffer[5] >> 5);

            t_framelength += frame_length;

            if (frame_length > b->bytes_into_buffer)
                break;

            advance_buffer(b, frame_length);
        } else {
            break;
        }
    }

    frames_per_sec = (float)samplerate/1024.0f;
    if (frames != 0)
        bytes_per_frame = (float)t_framelength/(float)(frames*1000);
    else
        bytes_per_frame = 0;
    *bitrate = (int)(8. * bytes_per_frame * frames_per_sec + 0.5);
    if (frames_per_sec != 0)
        *length = (float)frames/frames_per_sec;
    else
        *length = 1;

    return 1;
}

static int GetAACTrack(mp4ff_t *infile)
{
    /* find AAC track */
    int i, rc;
    int numTracks = mp4ff_total_tracks(infile);

    for (i = 0; i < numTracks; i++)
    {
        unsigned char *buff = NULL;
        int buff_size = 0;
        mp4AudioSpecificConfig mp4ASC;

        mp4ff_get_decoder_config(infile, i, &buff, &buff_size);

        if (buff)
        {
            rc = NeAACDecAudioSpecificConfig(buff, buff_size, &mp4ASC);
            free(buff);

            if (rc < 0)
                continue;
            return i;
        }
    }

    /* can't decode this */
    return -1;
}

static int startread(sox_format_t * ft)
{
	priv_t *b = (priv_t *) ft->priv;
	NeAACDecFrameInfo frameInfo;
	NeAACDecConfigurationPtr config;

	long bread = 0;
	int tagsize = 0;
	int bitrate;
	float length;
	int ret;
	unsigned long samplerate;
	unsigned char channels;
	off_t fileread;
	//MP4 specific
	
	
	unsigned char *buffer;
    int buffer_size;
	mp4AudioSpecificConfig mp4ASC;
	  /* for gapless decoding */
    
    
    
    

	memset(b, 0, sizeof(priv_t));

	if (!(b->buffer = (unsigned char*)malloc(FAAD_MIN_STREAMSIZE*MAX_CHANNELS)))
		{
			lsx_fail("Memory allocation error\n");
			return 0;
		}

	b->mp4file = 0;
	bread = (int)lsx_readbuf(ft, b->buffer, 8);

    if (b->buffer[4] == 'f' && b->buffer[5] == 't' && b->buffer[6] == 'y' && b->buffer[7] == 'p')
        b->mp4file = 1;
	ret= ft->seekable ? lsx_seeki(ft, (off_t)0, SEEK_SET) : -1;
	ft->tell_off = 0;

	hDecoder = NeAACDecOpen();


	/* Set the default object type and samplerate */
	/* This is useful for RAW AAC files */
	config = NeAACDecGetCurrentConfiguration(hDecoder);
	if (def_srate)
		config->defSampleRate = def_srate;
	config->defObjectType = object_type;
	config->outputFormat = outputFormat;
	config->downMatrix = downMatrix;
	config->useOldADTSFormat = old_format;
	//config->dontUpSampleImplicitSBR = 1;
	NeAACDecSetConfiguration(hDecoder, config);

	if (b->mp4file) {
		 /* initialise the callback structure */
		b->mp4cb = (mp4ff_callback_t*)malloc(sizeof(mp4ff_callback_t));

		b->mp4cb->read = read_callback;
		b->mp4cb->seek = seek_callback;
		b->mp4cb->user_data = ft;

		b->sampleId = 0;
		b->infile = mp4ff_open_read(b->mp4cb);
		if (!b->infile)
		{
			/* unable to open file */
			lsx_fail("Error opening file: %s\n");
			return 1;
		}

		if ((b->track = GetAACTrack(b->infile)) < 0)
		{
			lsx_fail("Unable to find correct AAC sound track in the MP4 file.\n");
			NeAACDecClose(hDecoder);
			mp4ff_close(b->infile);
			free(b->mp4cb);
			return 1;
		}

		buffer = NULL;
		buffer_size = 0;
		mp4ff_get_decoder_config(b->infile, b->track, &buffer, &buffer_size);

		if(NeAACDecInit2(hDecoder, buffer, buffer_size,
						&samplerate, &channels) < 0)
		{
			/* If some error initializing occured, skip the file */
			lsx_fail("Error initializing decoder library.\n");
			NeAACDecClose(hDecoder);
			mp4ff_close(b->infile);
			free(b->mp4cb);
			return 1;
		}

		b->timescale = mp4ff_time_scale(b->infile, b->track);
		b->framesize = 1024;
		/*useAacLength = 0;*/

		if (buffer)
		{
			if (NeAACDecAudioSpecificConfig(buffer, buffer_size, &mp4ASC) >= 0)
			{
				if (mp4ASC.frameLengthFlag == 1) b->framesize = 960;
				if (mp4ASC.sbr_present_flag == 1) b->framesize *= 2;
			}
			free(buffer);
		}

		
		
	}
	//AAC decoding
	else {
		ret = ft->seekable ? lsx_seeki(ft, (off_t)0, SEEK_END) : -1;
		fileread = lsx_tell(ft);
		ret= ft->seekable ? lsx_seeki(ft, (off_t)0, SEEK_SET) : -1;

		
		memset(b->buffer, 0, FAAD_MIN_STREAMSIZE*MAX_CHANNELS);
		bread = (int)lsx_readbuf(ft, b->buffer, FAAD_MIN_STREAMSIZE*MAX_CHANNELS);

		b->bytes_into_buffer = bread;
		b->bytes_consumed = 0;
		b->file_offset = 0;

		if (bread != FAAD_MIN_STREAMSIZE*MAX_CHANNELS)
			b->at_eof = 1;


		if (!memcmp(b->buffer, "ID3", 3))
		{
			/* high bit is not used */
			tagsize = (b->buffer[6] << 21) | (b->buffer[7] << 14) |
				(b->buffer[8] <<  7) | (b->buffer[9] <<  0);

			tagsize += 10;
			advance_buffer(b, tagsize);
			fill_buffer(ft);
		}
	
		

		/* get AAC infos for printing */
		header_type = 0;

		if ((b->buffer[0] == 0xFF) && ((b->buffer[1] & 0xF6) == 0xF0))
		{
			adts_parse(ft, &bitrate, &length);
			//fseek(b->infile, tagsize, SEEK_SET);//replace
			ret = ft->seekable ? lsx_seeki(ft, (off_t)tagsize, SEEK_SET) : -1;
			ft->tell_off = tagsize;

			bread = (int)lsx_readbuf(ft, b->buffer, FAAD_MIN_STREAMSIZE*MAX_CHANNELS);
			if (bread != FAAD_MIN_STREAMSIZE*MAX_CHANNELS)
				b->at_eof = 1;
			else
				b->at_eof = 0;
			b->bytes_into_buffer = bread;
			b->bytes_consumed = 0;
			b->file_offset = tagsize;

			header_type = 1;
		}
			else if (memcmp(b->buffer, "ADIF", 4) == 0) {
			int skip_size = (b->buffer[4] & 0x80) ? 9 : 0;
			bitrate = ((unsigned int)(b->buffer[4 + skip_size] & 0x0F)<<19) |
				((unsigned int)b->buffer[5 + skip_size]<<11) |
				((unsigned int)b->buffer[6 + skip_size]<<3) |
				((unsigned int)b->buffer[7 + skip_size] & 0xE0);

			length = (float)fileread;
			if (length != 0)
			{
				length = ((float)length*8.f)/((float)bitrate) + 0.5f;
			}

			bitrate = (int)((float)bitrate/1000.0f + 0.5f);

			header_type = 2;
		}

		fill_buffer(ft);

		if ((bread = NeAACDecInit(hDecoder, b->buffer,
			b->bytes_into_buffer, &samplerate, &channels)) < 0)
		{
			/* If some error initializing occured, skip the file */
			lsx_fail("Error initializing decoder library.\n");

			if (b->buffer)
				free(b->buffer);
			NeAACDecClose(hDecoder);
			return 1;
		}
		advance_buffer(b, bread);
		fill_buffer(ft);

  }

	ft->signal.rate = (sox_rate_t)samplerate;
	ft->encoding.encoding = SOX_ENCODING_AAC;
	ft->signal.channels = (unsigned)channels;
	//ft->signal.precision = ;

  return SOX_SUCCESS;
}

/*
 * Read up to len samples from p->Synth
 * If needed, read some more MP3 data, decode them and synth them
 * Place in buf[].
 * Return number of samples read.
 */
static size_t sox_aacread(sox_format_t * ft, sox_sample_t *buf, size_t len)
{
    priv_t *b = (priv_t *) ft->priv;
	size_t t_sample_read = 0, i = 0;
	long numSamples;
	void *sample_buffer;
	sox_sample_t *totalbuffer = NULL;
	NeAACDecFrameInfo frameInfo;
	sox_sample_t l;
	int noGapless = 0;
	//Gapless
	unsigned int useAacLength = 0;
	unsigned int initial = 1;
    /*size_t donow,i,done=0;*/
	unsigned char *buffer;
    int buffer_size;

	if (b->mp4file) 
	{
		numSamples = mp4ff_num_samples(b->infile, b->track);
		for (; b->sampleId < numSamples; (b->sampleId)++)
		{
			unsigned int sample_count;
			unsigned int delay = 0;
			int rc;
			long dur;

			/* get acces unit from MP4 file */
			buffer = NULL;
			//buffer_size = 0;
			buffer_size = 0;

			dur = mp4ff_get_sample_duration(b->infile, b->track, b->sampleId);
			rc = mp4ff_read_sample(b->infile, b->track, b->sampleId, &buffer,  &buffer_size);
			if (rc == 0)
			{
				lsx_fail("Reading from MP4 file failed.\n");
				NeAACDecClose(hDecoder);
				mp4ff_close(b->infile);
				return t_sample_read;
			}

			sample_buffer = NeAACDecDecode(hDecoder, &frameInfo, buffer, buffer_size);

			
			if (buffer) free(buffer);

			if (!noGapless)
			{
				if ( b->sampleId == 0) dur = 0;

				if (useAacLength || (b->timescale != ft->signal.rate )) {
					sample_count = frameInfo.samples;
				} else {
					sample_count = (unsigned int)(dur * frameInfo.channels);
					if (sample_count > frameInfo.samples)
						sample_count = frameInfo.samples;

					if (!useAacLength && !initial && ( b->sampleId < numSamples/2) && (sample_count != frameInfo.samples))
					{
						lsx_fail("MP4 seems to have incorrect frame duration, using values from AAC data.\n");
						useAacLength = 1;
						sample_count = frameInfo.samples;
					}
				}

				if (initial && (sample_count < b->framesize*frameInfo.channels) && (frameInfo.samples > sample_count))
					delay = frameInfo.samples - sample_count;
			} else {
				sample_count = frameInfo.samples;
			}

			if (frameInfo.error > 0)
			{
				lsx_warn("Warning: %s\n",
					NeAACDecGetErrorMessage(frameInfo.error));
			}

			if ((frameInfo.error == 0) && (frameInfo.samples > 0))
			{
				sample_buffer = (char*)sample_buffer + delay*2;
				for (i = 0; i< sample_count*2 ;i=i+2) {
					l = (((char*)sample_buffer)[i + 1]) << 24 | (0xffffff & (((char*)sample_buffer)[i] << 16));
					*(buf + (i/2)) = l;
				}
				(b->sampleId)++;
				return sample_count; //TODO Doubt on frameInfo.samples
			}
		}
	}

	else {
		do {
			sample_buffer = (sox_sample_t*)NeAACDecDecode(hDecoder, &frameInfo, b->buffer, b->bytes_into_buffer);
			/* update buffer indices */
			advance_buffer(b, frameInfo.bytesconsumed);

			if (frameInfo.error > 0)
			{
				/*faad_fprintf(stderr, "Error: %s\n",
					NeAACDecGetErrorMessage(frameInfo.error));*/
				//handle error
			}

			if ((frameInfo.error == 0) && (frameInfo.samples > 0))
			{
				for (i = 0; i< frameInfo.samples*2 ;i=i+2) {
					l = (((char*)sample_buffer)[i + 1]) << 24 | (0xffffff & (((char*)sample_buffer)[i] << 16));
					*(buf + (i/2)) = l;
				}
			
				fill_buffer(ft);
				return frameInfo.samples;
			}

			/* fill buffer */
			fill_buffer(ft);

			if (b->bytes_into_buffer == 0)
				sample_buffer = NULL; /* to make sure it stops now */

		} while (sample_buffer != NULL);
	}

    return t_sample_read;
}

static int stopread(sox_format_t * ft)
{
  priv_t *b =(priv_t*) ft->priv;

  NeAACDecClose(hDecoder);

    if (b->buffer)
        free(b->buffer);
	if (b->infile)
		mp4ff_close(b->infile);
	if (b->mp4cb)
		free(b->mp4cb);

  return SOX_SUCCESS;
}

static int sox_aacSeek(sox_format_t * ft, uint64_t offset)
{
   return SOX_SUCCESS;
}

LSX_FORMAT_HANDLER(aac)
{
	static char const * const names[] = {"m4a", "m4b", "m4p", "m4v", "m4r", "aac", "mp4", NULL};
  static unsigned const write_encodings[] = {
    SOX_ENCODING_AAC, 0, 0};
  static sox_format_handler_t const handler = {SOX_LIB_VERSION_CODE,
    "AAC lossy audio compression", names, 0,
    startread, sox_aacread, stopread,
    NULL, NULL, NULL,
    sox_aacSeek, write_encodings, adts_sample_rates, sizeof(priv_t)
  };
  return &handler;
}








