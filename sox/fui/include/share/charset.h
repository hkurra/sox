/* charset - Convenience library for converting strings between character sets
 * Copyright (C) 2002,2003,2004  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Parts are based on the charset.h (2001/12/04) from EasyTAG
 *  EasyTAG - Tag editor for MP3 and OGG files
 *  Copyright (C) 1999-2001  H蛆ard Kv虱en <havardk@xmms.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef FUI__SHARE__CHARSET_H
#define FUI__SHARE__CHARSET_H

/*
 * Charset info is a title,name pair.  The title is a human-readable
 * desecription of a charset.  The name is a shorthand desecription of a
 * charset.  Use the name when calling the functions below.
 */
typedef struct {
	char *title;
	char *name;
} FUI_Charset__Info;

/*
 * The list of all supported charsets
 */
extern const FUI_Charset__Info FUI_charset__info[];

/*
 * Functions for looking up the name/title in FUI_charset__info; both return
 * NULL if title/name does not exist.
 */
const char *FUI_charset__get_name_for_title(const char *title);
const char *FUI_charset__get_title_for_name(const char *name);

/*
 * Return the currently active locale's charset name.
 */
const char *FUI_charset__get_current();

/*
 * Tests if a given conversion is supported; returns 1 if supported, 0 if not
 * or no iconv.
 */
int FUI_charset__test_conversion(char *from_charset, char *to_charset);

/*
 * Convert a string from one charset to another.  The returned string is
 * malloc()ed and must be free()d by the caller.  Returns NULL if 'string' is
 * NULL or the conversion is not supported.  Returns an empty string if
 * 'string' ends in an invalid sequence.  If an invalid sequence appears in the
 * middle of 'string', the function attempts to convert everything after it.
 */
char *FUI_charset__convert_string(const char *string, const char *from_charset, const char *to_charset);

#endif
