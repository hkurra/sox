/* libFUI - FLAC User Interface library
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef FUI__FUI_Transducer_FIFO_h
#define FUI__FUI_Transducer_FIFO_h

#include <deque>
#include <map>
#include "Element.h"

namespace FUI {
	namespace Transducer {

		class FIFO {
		public:
			FIFO();
			virtual ~FIFO();

			inline unsigned size() const;

			Element *find(Element::PrimaryKey primary_key);
			Element *first_pending();

			void dequeue();
			void enqueue(Element *element);
		protected:
			typedef std::deque<Element*> Elements;
			typedef std::map<Element::PrimaryKey, Element*> ElementsIndex;

			Elements elements_;
			ElementsIndex elements_index_;
		};

		inline unsigned FIFO::size() const
		{ return (unsigned)elements_.size(); }

	}; // namepace Transducer
}; // namepace FUI

#endif
