/* libFUI - FLAC User Interface library
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef FUI__FUI_Transducer_Element_h
#define FUI__FUI_Transducer_Element_h

#include <string>
#include "FUI/Codec/Options.h"
#include "FUI/Codec/Type.h"
#include "FUI/Tag/Options.h"
#include "FUI/Tag/Type.h"

namespace FUI {
	namespace Transducer {

		class Element {
		public:
			typedef unsigned PrimaryKey;

			enum State {
				FINISHED,
				WORKING,
				PENDING
			};

			Element(
				std::string input_path,
				std::string output_path,
				Codec::Type input_format,
				Codec::Type output_format,
				const Codec::Options::Decode::Prototype *codec_decode_options,
				const Codec::Options::Encode::Prototype *codec_encode_options,
				const Tag::Options::Decode::Prototype *tag_decode_options,
				const Tag::Options::Encode::Prototype *tag_encode_options
			);
			virtual ~Element();

			inline PrimaryKey primary_key() const;
			inline State state() const;
			inline unsigned percent_complete() const;
			const char *state_as_string() const;
		public://@@@ should be protected? later?
			std::string input_path_;
			std::string output_path_;
			Codec::Type input_format_;
			Codec::Type output_format_;
			Codec::Options::Decode::Prototype *codec_decode_options_;
			Codec::Options::Encode::Prototype *codec_encode_options_;
			Tag::Options::Decode::Prototype *tag_decode_options_;
			Tag::Options::Encode::Prototype *tag_encode_options_;
		protected:
			PrimaryKey primary_key_;
			State state_;
			unsigned percent_complete_;
		private:
			static PrimaryKey next_primary_key_;
		public://@@@
			//@@@ protect these somehow?
			inline void set_state(State state) { state_ = state; }
			inline void set_percent_complete(unsigned percent_complete) { percent_complete_ = percent_complete; }
		};

		inline Element::PrimaryKey Element::primary_key() const
		{ return primary_key_; }

		inline Element::State Element::state() const
		{ return state_; }

		inline unsigned Element::percent_complete() const
		{ return percent_complete_; }

	}; // namepace Transducer
}; // namepace FUI

#endif
