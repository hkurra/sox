/* libFUI - FLAC User Interface library
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef FUI__FUI_Codec_Options_h
#define FUI__FUI_Codec_Options_h

#include <string>
#include <assert.h>
#include <inttypes.h>

namespace FUI {
	namespace Codec {
		namespace Options {

			namespace Decode {

				class Prototype {
				public:
					Prototype() { }
					virtual ~Prototype() { }

					// return "", meaning OK, else an error string
					virtual std::string verify() const = 0;
				};

				class FLAC: public Prototype {
				public:
					FLAC():
					Prototype(),
					verify_md5_(true)
					{ assert(verify().length() == 0); }

					std::string verify() const;

					bool verify_md5_;
				};

				class OggFLAC: public Prototype {
				public:
					OggFLAC():
					Prototype(),
					verify_md5_(true)
					{ assert(verify().length() == 0); }

					std::string verify() const;

					bool verify_md5_;
				};

				class OggVorbis: public Prototype {
				public:
					OggVorbis(): 
					Prototype()
					{ assert(verify().length() == 0); }

					std::string verify() const;
				};

				class MP3: public Prototype {
				public:
					MP3(): 
					Prototype()
					{ assert(verify().length() == 0); }

					std::string verify() const;
				};

				class RiffWave: public Prototype {
				public:
					RiffWave(): 
					Prototype()
					{ assert(verify().length() == 0); }

					std::string verify() const;
				};

				Prototype *clone(const Prototype *);
			};

			namespace Encode {

				class Prototype {
				public:
					Prototype() { }
					virtual ~Prototype() { }

					// return "", meaning OK, else an error string
					virtual std::string verify() const = 0;
				};

				class FLAC: public Prototype {
				public:
					FLAC():
					Prototype(),
					verify_(true),
					subset_(true),
					do_mid_side_(true),
					adaptive_mid_side_(false),
					blocksize_(4608),
					max_lpc_order_(8),
					qlp_coeff_precision_(0),
					do_qlp_coeff_prec_search_(false),
					do_escape_coding_(false),
					do_exhaustive_model_search_(false),
					min_residual_partition_order_(3),
					max_residual_partition_order_(3),
					rice_parameter_search_dist_(0)
					{ assert(verify().length() == 0); }

					FLAC(unsigned level);

					std::string verify() const;

					bool verify_;
					bool subset_;
					bool do_mid_side_;
					bool adaptive_mid_side_; // requires that do_mid_side is true also
					unsigned blocksize_;
					unsigned max_lpc_order_;
					unsigned qlp_coeff_precision_;
					bool do_qlp_coeff_prec_search_;
					bool do_escape_coding_; // deprecated in libFLAC
					bool do_exhaustive_model_search_;
					unsigned min_residual_partition_order_;
					unsigned max_residual_partition_order_;
					unsigned rice_parameter_search_dist_; // deprecated in libFLAC
				};

				class OggFLAC: public Prototype {
				public:
					OggFLAC():
					Prototype(),
					verify_(true),
					subset_(true),
					do_mid_side_(true),
					adaptive_mid_side_(false),
					blocksize_(4608),
					max_lpc_order_(8),
					qlp_coeff_precision_(0),
					do_qlp_coeff_prec_search_(false),
					do_escape_coding_(false),
					do_exhaustive_model_search_(false),
					min_residual_partition_order_(3),
					max_residual_partition_order_(3),
					rice_parameter_search_dist_(0),
					serial_number_(0)
					{ assert(verify().length() == 0); }

					OggFLAC(unsigned level);

					std::string verify() const;

					bool verify_;
					bool subset_;
					bool do_mid_side_;
					bool adaptive_mid_side_; // requires that do_mid_side is true also
					unsigned blocksize_;
					unsigned max_lpc_order_;
					unsigned qlp_coeff_precision_;
					bool do_qlp_coeff_prec_search_;
					bool do_escape_coding_; // deprecated in libFLAC
					bool do_exhaustive_model_search_;
					unsigned min_residual_partition_order_;
					unsigned max_residual_partition_order_;
					unsigned rice_parameter_search_dist_; // deprecated in libFLAC

					uint32_t serial_number_;
				};

				class OggVorbis: public Prototype {
				public:
					OggVorbis(): 
					Prototype(),
					serial_number_(0),
					quality_(5.0)
					{ assert(verify().length() == 0); }

					std::string verify() const;

					uint32_t serial_number_;
					double quality_;
				};

				class MP3: public Prototype {
				public:
					MP3(): 
					Prototype(),
					target_bps_(192)
					{ assert(verify().length() == 0); }

					std::string verify() const;

					unsigned target_bps_;
				};

				class RiffWave: public Prototype {
				public:
					RiffWave(): 
					Prototype()
					{ assert(verify().length() == 0); }

					std::string verify() const;
				};

				Prototype *clone(const Prototype *);

			};

		}; // namespace Options
	}; // namepace Codec
}; // namepace FUI

#endif
