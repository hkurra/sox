/* libFUI - FLAC User Interface library
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef FUI__FUI_Codec_Transducer_h
#define FUI__FUI_Codec_Transducer_h

#include <string>
#include "FUI/Tag/Options.h"
#include "FUI/ThreadContext.h"
#include "Options.h"
#include "Type.h"

namespace FUI {
	namespace Codec {
		namespace Transducer {

			struct Result {
				Result(): ok_(true), description_() { }
				Result(const char *x): ok_(false), description_(x) { }
				Result(const std::string &x): ok_(false), description_(x) { }
				// Synthetic copy constructor, destructor, operator=
#if 0
				//@@@ is this needed?
				enum Code {
				};
				Code code_;
#else
				//@@@ or is this all we need?
				bool ok_;
#endif
				std::string description_;
			};

			bool is_legal(Codec::Type input_type, Codec::Type output_type);

			Result transduce(
				Codec::Type input_type,
				std::string input_path,
				const Codec::Options::Decode::Prototype &codec_decode_options,
				const Tag::Options::Decode::Prototype &tag_decode_options,
				Codec::Type output_type,
				std::string output_path,
				const Codec::Options::Encode::Prototype &codec_encode_options,
				const Tag::Options::Encode::Prototype &tag_encode_options,
				FUI::ThreadContext *thread_context
			);

		}; // namespace Transducer
	}; // namepace Codec
}; // namepace FUI

#endif
