/* libFUI - FLAC User Interface library
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef FUI__FUI_Codec_CanonicalForm_h
#define FUI__FUI_Codec_CanonicalForm_h

namespace FUI {
	namespace Codec {

		class CanonicalForm {
		public:
			// Synthetic constructors, destructor, operator=
			unsigned sample_rate_;
			unsigned sample_resolution_;
			unsigned channels_;
		};

	}; // namepace Codec
}; // namepace FUI

#endif
