/* libFUI - FLAC User Interface library
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef FUI__FUI_Codec_Recognizer_h
#define FUI__FUI_Codec_Recognizer_h

#include <string>
#include "Type.h"

namespace FUI {
	namespace Codec {
		namespace Recognizer {

			//@@@ WATCHOUT: only use_extension == true is implemented yet
			// returns "" for success, else error string and 'format' is unchanged.
			std::string divine_format_from_file(const std::string &path, Codec::Type &format, bool use_extension = true);

		}; // namepace Recognizer
	}; // namepace Codec
}; // namepace FUI

#endif
