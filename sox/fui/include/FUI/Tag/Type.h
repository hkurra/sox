/* libFUI - FLAC User Interface library
 * Copyright (C) 2004  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef FUI__FUI_Tag_Type_h
#define FUI__FUI_Tag_Type_h

namespace FUI {
	namespace Tag {

		// WATCHOUT: `Type' is also used as an index for arrays
		// so make sure the values are contiguous and start at 0.
		enum Type {
			NULL_TAG = 0,
			FLAC,
			ID3,
			OGG_FLAC,
			OGG_VORBIS,
			TYPE_MAX
		};

		// The string version of the Type enum:
		extern const char * const TypeString[TYPE_MAX];

		// Human-readable value:
		extern const char * const TypeDescription[TYPE_MAX];

	} // namepace Tag
} // namepace FUI

#endif
