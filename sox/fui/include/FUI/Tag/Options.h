/* libFUI - FLAC User Interface library
 * Copyright (C) 2004  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef FUI__FUI_Tag_Options_h
#define FUI__FUI_Tag_Options_h

#include <string>
#include <assert.h>
#include <inttypes.h>

namespace FUI {
	namespace Tag {
		namespace Options {

			namespace Decode {

				class Prototype {
				public:
					inline Prototype() { }
					virtual ~Prototype() { }

					// return "", meaning OK, else an error string
					virtual std::string verify() const = 0;
				};

				class Null: public Prototype {
				public:
					inline Null():
					Prototype()
					{ assert(verify().length() == 0); }

					std::string verify() const;
				};

				// VorbisComment is used for FLAC, OGG_FLAC, and OGG_VORBIS tag types
				class VorbisComment: public Prototype {
				public:
					inline VorbisComment():
					Prototype()
					{ assert(verify().length() == 0); }

					std::string verify() const;
				};

				class ID3: public Prototype {
				public:
					inline ID3(): 
					Prototype(),
					v1_(true),
					v2_(true),
					v1_encoding_("ISO-8859-1")
					{ assert(verify().length() == 0); }

					std::string verify() const;

					bool v1_; // read ID3 v1 tag
					bool v2_; // read ID3 v2 tag

					std::string v1_encoding_; // charset used in ID3 v1 tag
				};

				Prototype *clone(const Prototype *);
			};

			namespace Encode {

				class Prototype {
				public:
					inline Prototype() { }
					virtual ~Prototype() { }

					// return "", meaning OK, else an error string
					virtual std::string verify() const = 0;
				};

				class Null: public Prototype {
				public:
					inline Null():
					Prototype()
					{ assert(verify().length() == 0); }

					std::string verify() const;
				};

				// VorbisComment is used for FLAC, OGG_FLAC, and OGG_VORBIS tag types
				class VorbisComment: public Prototype {
				public:
					inline VorbisComment():
					Prototype()
					{ assert(verify().length() == 0); }

					std::string verify() const;
				};

				class ID3: public Prototype {
				public:
					inline ID3():
					Prototype(),
					v1_(true),
					v2_(true),
					v1_encoding_("ISO-8859-1")
					{ assert(verify().length() == 0); }

					std::string verify() const;

					bool v1_; // write ID3 v1 tag
					bool v2_; // write ID3 v2 tag

					std::string v1_encoding_; // charset used for ID3 v1 tag
				};

				Prototype *clone(const Prototype *);

			};

		} // namespace Options
	} // namepace Tag
} // namepace FUI

#endif
