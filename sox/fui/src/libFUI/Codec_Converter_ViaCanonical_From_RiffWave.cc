/* libFUI - FLAC User Interface library
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <assert.h>
#include <string.h> // for memcpy()
#ifdef FUI__OLD_STRSTREAM
#include <strstream> // for ostrstream
#else
#include <sstream> // for ostringstream
#endif
#include "private/Codec/Converter/ViaCanonical/From/RiffWave.h"

#ifdef min
#undef min
#endif

namespace FUI {
	namespace Codec {
		namespace Converter {
			namespace ViaCanonical {
				namespace From {

					namespace local {

						//@@@ Pulled out of ass.
						//@@@ Should not be too big as temp buffer goes on stack.
						//@@@ Smaller numbers may actually be faster because of improved data locality.
						static const unsigned max_samples_per_call = 8192;


						static inline unsigned min(const unsigned a, const unsigned b)
						{
							return a < b? a : b;
						}

						static inline uint16_t parse_uint16(const unsigned char *s)
						{
							uint16_t x = 0;
							x = (unsigned)(*s++);
							x |= ((unsigned)(*s) << 8);
							return x;
						}

						static inline uint32_t parse_uint32(const unsigned char *s)
						{
							uint32_t x = 0;
							x = (unsigned)(*s++);
							x |= ((unsigned)(*s++) << 8);
							x |= ((unsigned)(*s++) << 16);
							x |= ((unsigned)(*s) << 24);
							return x;
						}

						static void format_samples(const unsigned char *pcm, unsigned samples, unsigned channels, unsigned bps, int32_t *data[])
						{
							if(bps == 8) {
								for(unsigned sample = 0; sample < samples; sample++)
									for(unsigned channel = 0; channel < channels; channel++)
										data[channel][sample] = (int32_t)(*pcm++) - 0x80;
							}
							else if(bps == 16) {
								for(unsigned sample = 0; sample < samples; sample++)
									for(unsigned channel = 0; channel < channels; channel++) {
										data[channel][sample] = (*pcm++);
										data[channel][sample] |= (((int32_t)((signed char)(*pcm++)))<<8);
									}
							}
							else if(bps == 24) {
								for(unsigned sample = 0; sample < samples; sample++)
									for(unsigned channel = 0; channel < channels; channel++) {
										data[channel][sample] = (*pcm++);
										data[channel][sample] |= ((*pcm++)<<8);
										data[channel][sample] |= (((int32_t)((signed char)(*pcm++)))<<16);
									}
							}
							else if(bps == 32) {
								for(unsigned sample = 0; sample < samples; sample++)
									for(unsigned channel = 0; channel < channels; channel++) {
										data[channel][sample] = (*pcm++);
										data[channel][sample] |= ((*pcm++)<<8);
										data[channel][sample] |= ((*pcm++)<<16);
										data[channel][sample] |= ((*pcm++)<<24);
									}
							}
							else {
								assert(0);
							}
						}

					}; // namespace local


					RiffWave::RiffWave():
					Interface(),
					status_(""),
					percent_complete_(0),
					file_(0),
					canonical_form_(),
					bytes_per_sample_(0),
					samples_to_decode_(0),
					samples_decoded_(0)
					{
					}

					RiffWave::~RiffWave()
					{
					}

					bool RiffWave::init(
						std::string path,
						const Codec::Options::Decode::Prototype &codec_decode_options,
						const Tag::Options::Decode::Prototype &tag_decode_options,
						::FLAC::Metadata::VorbisComment &tags,
						Codec::CanonicalForm &canonical_form,
						unsigned &max_samples_per_call
					)
					{
						assert(0 == file_);

						const Codec::Options::Decode::RiffWave &codec_options = dynamic_cast<const Codec::Options::Decode::RiffWave &>(codec_decode_options);
						const Tag::Options::Decode::Null &tag_options = dynamic_cast<const Tag::Options::Decode::Null &>(tag_decode_options);

						(void)codec_options;
						(void)tag_options;
						(void)tags;

						status_ = "";
						percent_complete_ = 0;
						bytes_per_sample_ = 0;
						samples_to_decode_ = samples_decoded_ = 0;

						if(0 == (file_ = fopen(path.c_str(), "rb")))
							return "error opening file";

						unsigned char header[12];

						if(
							fread(header, 1, sizeof(header), file_) < sizeof(header) ||
							0 != strncmp((const char *)header, "RIFF", 4) ||
							0 != strncmp((const char *)header+8, "WAVE", 4)
						) {
							status_ = "file is not RIFF WAVE";
							return false;
						}

						// search the subchunks for what we support:
						//@@@ 2G limits abound here
						long fmt_offset = -1;
						long data_offset = -1;
						assert(sizeof(header) >= 8);
						while(!feof(file_)) {
							const long here = ftell(file_);
							if(fread(header, 1, 8, file_) < 8) {
								if(!feof(file_)) {
									status_ = "error reading file";
									return false;
								}
							}
							else {
								if(0 == strncmp((const char *)header, "fmt ", 4)) {
									if(fmt_offset >= 0) {
										status_ = "file has more than one 'fmt' subchunk";
										return false;
									}
									fmt_offset = here;
								}
								else if(0 == strncmp((const char *)header, "data", 4)) {
									if(data_offset >= 0) {
										status_ = "file has more than one 'data' subchunk";
										return false;
									}
									data_offset = here;
								}
								if(fseek(file_, local::parse_uint32(header+4), SEEK_CUR) < 0) {
									status_ = "seek error";
									return false;
								}
							}
						}
						if(fmt_offset < 0) {
							status_ = "file has no 'fmt' subchunk";
							return false;
						}
						if(data_offset < 0) {
							status_ = "file has no 'data' subchunk";
							return false;
						}

						// parse the fmt subchunk
						if(fseek(file_, fmt_offset+4, SEEK_SET) < 0) {
							status_ = "seek error";
							return false;
						}
						uint16_t x16;
						uint32_t x32;
#define LOCAL__READ_UINT16 if(fread(header, 1, 2, file_) < 2) { status_ = "read error"; return false; } else { x16 = local::parse_uint16(header); }
#define LOCAL__READ_UINT32 if(fread(header, 1, 4, file_) < 4) { status_ = "read error"; return false; } else { x32 = local::parse_uint32(header); }
						LOCAL__READ_UINT32
						if(x32 < 16) {
							status_ = "illegal 'fmt' subchunk";
							return false;
						}
						LOCAL__READ_UINT16
						if(x16 != 1) {
#ifdef FUI__OLD_STRSTREAM
							std::ostrstream os;
#else
							std::ostringstream os;
#endif
							os << "unsupported compression type (" << x16 << ") in 'fmt' subchunk";
							status_ = os.str();
							return false;
						}
						LOCAL__READ_UINT16
						if(x16 == 0 || x16 > 8) {
#ifdef FUI__OLD_STRSTREAM
							std::ostrstream os;
#else
							std::ostringstream os;
#endif
							os << "unsupported number of channels (" << x16 << ") in 'fmt' subchunk";
							status_ = os.str();
							return false;
						}
						canonical_form.channels_ = x16;
						LOCAL__READ_UINT32
						canonical_form.sample_rate_ = x32;
						LOCAL__READ_UINT32 // avg bytes per second (ignored)
						LOCAL__READ_UINT16 // block align (ignored)
						LOCAL__READ_UINT16
						if(x16 != 8 && x16 != 16 && x16 != 24 && x16 != 32) {
#ifdef FUI__OLD_STRSTREAM
							std::ostrstream os;
#else
							std::ostringstream os;
#endif
							os << "unsupported bits-per-sample (" << x16 << ") in 'fmt' subchunk";
							status_ = os.str();
							return false;
						}
						canonical_form.sample_resolution_ = x16;
						bytes_per_sample_ = canonical_form.channels_ * ((canonical_form.sample_resolution_+7)/8);

						canonical_form_ = canonical_form;

						// now seek to 'data' subchunk and get size
						if(fseek(file_, data_offset+4, SEEK_SET) < 0) {
							status_ = "seek error";
							return false;
						}
						LOCAL__READ_UINT32
#undef LOCAL__READ_UINT16
#undef LOCAL__READ_UINT32
						if(x32 == 0) {
							status_ = "'data' subchunk has zero size";
							return false;
						}
						else if(x32 % bytes_per_sample_ != 0) {
							status_ = "'data' subchunk length implies partial samples";
							return false;
						}
						samples_to_decode_ = x32 / bytes_per_sample_;

						max_samples_per_call = local::min(samples_to_decode_, local::max_samples_per_call);

						return true;
					}

					bool RiffWave::supply_data(int32_t *data[], unsigned &samples)
					{
						assert(0 != file_);

						const unsigned samples_to_supply = local::min((samples_to_decode_-samples_decoded_), samples);

						if(samples_to_supply > 0) {
							const unsigned bytes_to_supply = samples_to_supply * bytes_per_sample_;
							unsigned char pcm[bytes_to_supply];

							if(fread(pcm, 1, bytes_to_supply, file_) < bytes_to_supply) {
								status_ = "read error or unexpected end of file";
								return false;
							}

							local::format_samples(pcm, samples_to_supply, canonical_form_.channels_, canonical_form_.sample_resolution_, data);
						}

						samples = samples_to_supply;
						samples_decoded_ += samples;
						if(0 != samples_to_decode_)
							percent_complete_ = local::min(99, 100 * samples_decoded_ / samples_to_decode_);
						else
							percent_complete_ = 0;

						return true;
					}

					bool RiffWave::finish()
					{
						if(0 != file_) {
							fclose(file_);
							file_ = 0;
						}
						percent_complete_ = 100;
						return true;
					}

					RiffWave RiffWave_;

				}; // namespace From
			}; // namespace ViaCanonical
		}; // namespace Converter
	}; // namespace Codec
}; // namespace FUI
