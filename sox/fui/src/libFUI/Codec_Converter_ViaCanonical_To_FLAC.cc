/* libFUI - FLAC User Interface library
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <assert.h>
#include <string.h> // for memcpy()
#include "FLAC++/metadata.h"
#include "private/Codec/Converter/ViaCanonical/To/FLAC.h"

namespace FUI {
	namespace Codec {
		namespace Converter {
			namespace ViaCanonical {
				namespace To {

					FLAC::FLAC():
					Interface(),
					path_(),
					status_(""),
					encoder_(),
					canonical_form_()
					{
						if(!encoder_.is_valid())
							status_ = "invalid encoder";
					}

					FLAC::~FLAC()
					{
					}

					bool FLAC::init(
						std::string path,
						const Codec::Options::Encode::Prototype &codec_encode_options,
						const Tag::Options::Encode::Prototype &tag_encode_options,
						::FLAC::Metadata::VorbisComment &tags,
						const Codec::CanonicalForm &canonical_form,
						unsigned &max_samples_per_call
					)
					{
						(void)max_samples_per_call;

						assert(encoder_.is_valid());

						const Codec::Options::Encode::FLAC &codec_options = dynamic_cast<const Codec::Options::Encode::FLAC &>(codec_encode_options);
						const Tag::Options::Encode::VorbisComment &tag_options = dynamic_cast<const Tag::Options::Encode::VorbisComment &>(tag_encode_options);

						status_ = "";
						canonical_form_ = canonical_form;

						::FLAC::Metadata::Prototype *metadata[1] = { &tags };
						const unsigned num_metadata = 1;

						if(!(
							encoder_.set_filename(path.c_str()) &&
							encoder_.set_verify(codec_options.verify_) &&
							encoder_.set_streamable_subset(codec_options.subset_) &&
							encoder_.set_channels(canonical_form_.channels_) && 
							encoder_.set_bits_per_sample(canonical_form_.sample_resolution_) && 
							encoder_.set_sample_rate(canonical_form_.sample_rate_) &&
							encoder_.set_do_mid_side_stereo(codec_options.do_mid_side_) &&
							encoder_.set_loose_mid_side_stereo(codec_options.adaptive_mid_side_) &&
							encoder_.set_blocksize(codec_options.blocksize_) &&
							encoder_.set_max_lpc_order(codec_options.max_lpc_order_) &&
							encoder_.set_qlp_coeff_precision(codec_options.qlp_coeff_precision_) &&
							encoder_.set_do_qlp_coeff_prec_search(codec_options.do_qlp_coeff_prec_search_) &&
							encoder_.set_do_escape_coding(codec_options.do_escape_coding_) &&
							encoder_.set_do_exhaustive_model_search(codec_options.do_exhaustive_model_search_) &&
							encoder_.set_min_residual_partition_order(codec_options.min_residual_partition_order_) &&
							encoder_.set_max_residual_partition_order(codec_options.max_residual_partition_order_) &&
							encoder_.set_rice_parameter_search_dist(codec_options.rice_parameter_search_dist_) &&
							encoder_.set_total_samples_estimate(0) &&
							encoder_.set_metadata(metadata, num_metadata)
						))
							assert(0);

						if((::FLAC__FileEncoderState)encoder_.init() != ::FLAC__FILE_ENCODER_OK) {
							status_ = encoder_.get_state().resolved_as_cstring(encoder_);
							return false;
						}

						return true;
					}

					bool FLAC::feed_data(int32_t *data[], unsigned &samples)
					{
						assert(encoder_.is_valid());

						if(!encoder_.process(data, samples)) {
							status_ = encoder_.get_state().resolved_as_cstring(encoder_);
							return false;
						}

						return true;
					}

					bool FLAC::finish()
					{
						assert(encoder_.is_valid());

						encoder_.finish();

						return true;
					}

					FLAC::Encoder::Encoder():
					::FLAC::Encoder::File()
					{
					}

					void FLAC::Encoder::progress_callback(FLAC__uint64, FLAC__uint64, unsigned, unsigned)
					{
					}

					FLAC FLAC_;

				}; // namespace To
			}; // namespace ViaCanonical
		}; // namespace Converter
	}; // namespace Codec
}; // namespace FUI
