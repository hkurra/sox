/* libFUI - FLAC User Interface library
 * Copyright (C) 2004  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "FUI/Tag/Options.h"

namespace FUI {
	namespace Tag {
		namespace Options {

			namespace Decode {

				std::string Null::verify() const
				{
					return "";
				}

				std::string VorbisComment::verify() const
				{
					return "";
				}

				std::string ID3::verify() const
				{
					return "";
				}

				Prototype *clone(const Prototype *obj)
				{
					Prototype *ret;
					const Null *null = dynamic_cast<const Null *>(obj);
					const VorbisComment *vorbiscomment = dynamic_cast<const VorbisComment *>(obj);
					const ID3 *id3 = dynamic_cast<const ID3 *>(obj);
					if(0 != null)
						ret = new Null(*null);
					else if(0 != vorbiscomment)
						ret = new VorbisComment(*vorbiscomment);
					else if(0 != id3)
						ret = new ID3(*id3);
					else
						assert(0);
					return ret;
				}

			}

			namespace Encode {

				std::string Null::verify() const
				{
					return "";
				}

				std::string VorbisComment::verify() const
				{
					return "";
				}

				std::string ID3::verify() const
				{
					return "";
				}

				Prototype *clone(const Prototype *obj)
				{
					Prototype *ret;
					const Null *null = dynamic_cast<const Null *>(obj);
					const VorbisComment *vorbiscomment = dynamic_cast<const VorbisComment *>(obj);
					const ID3 *id3 = dynamic_cast<const ID3 *>(obj);
					if(0 != null)
						ret = new Null(*null);
					else if(0 != vorbiscomment)
						ret = new VorbisComment(*vorbiscomment);
					else if(0 != id3)
						ret = new ID3(*id3);
					else
						assert(0);
					return ret;
				}

			}

		} // namespace Options
	} // namepace Tag
} // namepace FUI
