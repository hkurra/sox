/* libFUI - FLAC User Interface library
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "private/Codec/Transcoder/Internal/FLAC_to_FLAC.h"
#include "FLAC/metadata.h"
#include "FLAC++/decoder.h"
#include "FLAC++/encoder.h"
#include <stdio.h> //@@@for stderr
#include <vector>

#ifdef min
#undef min
#endif

namespace FUI {
	namespace Codec {
		namespace Transcoder {
			namespace Internal {

				namespace local_FLAC_to_FLAC {

					static inline unsigned min(const unsigned a, const unsigned b)
					{
						return a < b? a : b;
					}

					class Encoder : public ::FLAC::Encoder::File {
					public:
						Encoder(FUI::ThreadContext *thread_context);
						// Synthetic destructor.
						// No copy-constructor or operator= because of the ancestry.
					protected:
						// from ::FLAC::Encoder::File
						virtual void progress_callback(FLAC__uint64 bytes_written, FLAC__uint64 samples_written, unsigned frames_written, unsigned total_frames_estimate);

						FUI::ThreadContext *thread_context_;
						unsigned percent_complete_;
					};

					Encoder::Encoder(FUI::ThreadContext *thread_context):
					::FLAC::Encoder::File(),
					thread_context_(thread_context),
					percent_complete_(0)
					{ }

					void Encoder::progress_callback(FLAC__uint64, FLAC__uint64, unsigned frames_written, unsigned total_frames_estimate)
					{
						if(total_frames_estimate > 0) {
							const unsigned percent_complete = local_FLAC_to_FLAC::min(99, frames_written * 100 / total_frames_estimate);
							if(percent_complete > percent_complete_) {
								thread_context_->progress_report(percent_complete);
								percent_complete_ = percent_complete;
							}
						}
					}

					class Decoder : public ::FLAC::Decoder::File {
					public:
						Decoder(Encoder &encoder);
						~Decoder();
						// No copy-constructor or operator= because of the ancestry.

						inline std::string status() const;
					protected:
						// from ::FLAC::Decoder::File
						virtual ::FLAC__StreamDecoderWriteStatus write_callback(const ::FLAC__Frame *frame, const FLAC__int32 * const buffer[]);
						virtual void metadata_callback(const ::FLAC__StreamMetadata *metadata);
						virtual void error_callback(::FLAC__StreamDecoderErrorStatus status);

						bool error_occurred_;
						Encoder &encoder_;
						std::string status_;

						typedef std::vector< ::FLAC__StreamMetadata*> Metadata;
						Metadata metadata_;
					};

					Decoder::Decoder(Encoder &encoder):
					::FLAC::Decoder::File(),
					error_occurred_(false),
					encoder_(encoder),
					status_("")
					{
						assert(encoder_.is_valid());
					}

					Decoder::~Decoder()
					{
						for(Metadata::iterator it = metadata_.begin(); it != metadata_.end(); it++)
							::FLAC__metadata_object_delete(*it);
					}

					inline std::string Decoder::status() const
					{ return status_; }

					::FLAC__StreamDecoderWriteStatus Decoder::write_callback(const ::FLAC__Frame *frame, const FLAC__int32 * const buffer[])
					{
						if(error_occurred_)
							return ::FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;

						error_occurred_ = !encoder_.process(buffer, frame->header.blocksize);

						if(error_occurred_) {
							status_ = encoder_.get_state().resolved_as_cstring(encoder_);
							return ::FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
						}

						return ::FLAC__STREAM_DECODER_WRITE_STATUS_CONTINUE;
					}

					void Decoder::metadata_callback(const ::FLAC__StreamMetadata *metadata)
					{
						if(error_occurred_)
							return;

						::FLAC__StreamMetadata *copy = ::FLAC__metadata_object_clone(metadata);
						if(0 != copy) {
							metadata_.push_back(copy);
							if(metadata->type == ::FLAC__METADATA_TYPE_STREAMINFO) {
								if(!(
									encoder_.set_total_samples_estimate(metadata->data.stream_info.total_samples) &&
									encoder_.set_channels(metadata->data.stream_info.channels) && 
									encoder_.set_bits_per_sample(metadata->data.stream_info.bits_per_sample) && 
									encoder_.set_sample_rate(metadata->data.stream_info.sample_rate)
								)) {
									status_ = encoder_.get_state().resolved_as_cstring(encoder_);
									error_occurred_ = true;
								}
							}
							if(!error_occurred_ && metadata->is_last) {
								::FLAC__StreamMetadata *all[metadata_.size()];
								unsigned n = 0;
								for(Metadata::iterator it = metadata_.begin(); it != metadata_.end(); it++) {
									//@@@ when seektable options are added, we have to use that template instead of the original file's seektable
									if((*it)->type != ::FLAC__METADATA_TYPE_STREAMINFO)
										all[n++] = *it;
								}
								if(!encoder_.set_metadata(all, n) || (::FLAC__FileEncoderState)encoder_.init() != ::FLAC__FILE_ENCODER_OK) {
									status_ = encoder_.get_state().resolved_as_cstring(encoder_);
									error_occurred_ = true;
								}
							}
						}
						else {
							status_ = "memory allocation error";
							error_occurred_ = true;
						}
					}

					void Decoder::error_callback(::FLAC__StreamDecoderErrorStatus status)
					{
						(void)status;
						status_ = get_state().resolved_as_cstring(*this);
						error_occurred_ = true;
					}

				}; // namespace local_FLAC_to_FLAC

				std::string FLAC_to_FLAC::transduce(
					std::string input_path,
					const Codec::Options::Decode::Prototype &codec_decode_options,
					const Tag::Options::Decode::Prototype &tag_decode_options,
					std::string output_path,
					const Codec::Options::Encode::Prototype &codec_encode_options,
					const Tag::Options::Encode::Prototype &tag_encode_options,
					FUI::ThreadContext *thread_context
				)
				{
					fprintf(stderr, "@@@ Transcoder::Internal::FLAC_to_FLAC::transduce(%s, %s)\n", input_path.c_str(), output_path.c_str());

					thread_context->progress_report(0);

					local_FLAC_to_FLAC::Encoder encoder(thread_context);
					local_FLAC_to_FLAC::Decoder decoder(encoder);

					if(!decoder.is_valid())
						return "invalid decoder";

					if(!encoder.is_valid())
						return "invalid encoder";

					const Codec::Options::Decode::FLAC &flac_decode_options = dynamic_cast<const Codec::Options::Decode::FLAC &>(codec_decode_options);
					const Codec::Options::Encode::FLAC &flac_encode_options = dynamic_cast<const Codec::Options::Encode::FLAC &>(codec_encode_options);

					const Tag::Options::Decode::VorbisComment &vc_decode_options = dynamic_cast<const Tag::Options::Decode::VorbisComment &>(tag_decode_options);
					const Tag::Options::Encode::VorbisComment &vc_encode_options = dynamic_cast<const Tag::Options::Encode::VorbisComment &>(tag_encode_options);

					if(!(
						decoder.set_md5_checking(flac_decode_options.verify_md5_) && 
						decoder.set_filename(input_path.c_str()) && 
						decoder.set_metadata_respond_all()
					)) {
						assert(0);
						return "assertion failure";
					}

					if(!(
						encoder.set_filename(output_path.c_str()) &&
						encoder.set_verify(flac_encode_options.verify_) &&
						encoder.set_streamable_subset(flac_encode_options.subset_) &&
						encoder.set_do_mid_side_stereo(flac_encode_options.do_mid_side_) &&
						encoder.set_loose_mid_side_stereo(flac_encode_options.adaptive_mid_side_) &&
						encoder.set_blocksize(flac_encode_options.blocksize_) &&
						encoder.set_max_lpc_order(flac_encode_options.max_lpc_order_) &&
						encoder.set_qlp_coeff_precision(flac_encode_options.qlp_coeff_precision_) &&
						encoder.set_do_qlp_coeff_prec_search(flac_encode_options.do_qlp_coeff_prec_search_) &&
						encoder.set_do_escape_coding(flac_encode_options.do_escape_coding_) &&
						encoder.set_do_exhaustive_model_search(flac_encode_options.do_exhaustive_model_search_) &&
						encoder.set_min_residual_partition_order(flac_encode_options.min_residual_partition_order_) &&
						encoder.set_max_residual_partition_order(flac_encode_options.max_residual_partition_order_) &&
						encoder.set_rice_parameter_search_dist(flac_encode_options.rice_parameter_search_dist_)
					)) {
						assert(0);
						return "assertion failure";
					}

					if((::FLAC__FileDecoderState)decoder.init() != ::FLAC__FILE_DECODER_OK)
						return decoder.get_state().resolved_as_cstring(decoder);

					if(!decoder.process_until_end_of_file())
						return decoder.status();

					if(!decoder.finish())
						return "MD5 mismatch";

					encoder.finish();

					thread_context->progress_report(100);

					return "";
				}

				FLAC_to_FLAC FLAC_to_FLAC_;

			}; // namespace Internal
		}; // namespace Transcoder
	}; // namespace Codec
}; // namespace FUI
