/* libFUI - FLAC User Interface library
 * Copyright (C) 2004  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "private/Tag/Converter/ViaCanonical/From/Converters.h"
#include "private/Tag/Converter/ViaCanonical/From/FLAC.h"
#include "private/Tag/Converter/ViaCanonical/From/ID3.h"
#include "private/Tag/Converter/ViaCanonical/From/Null.h"

namespace FUI {
	namespace Tag {
		namespace Converter {
			namespace ViaCanonical {
				namespace From {

					Converters::Converters()
					{
						(*this)[Tag::FLAC] = &FLAC_;
						(*this)[Tag::NULL_TAG] = &Null_;
						(*this)[Tag::ID3] = &ID3_;
					}

					Converters converters_;

				} // namespace From
			} // namespace ViaCanonical
		} // namespace Converter
	} // namespace Tag
} // namespace FUI
