/* libFUI - FLAC User Interface library
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "private/Codec/Converter/Capability.h"
#include "private/Codec/Converter/ViaCanonical/From/Converters.h"
#include "private/Codec/Converter/ViaCanonical/To/Converters.h"

namespace FUI {
	namespace Codec {
		namespace Converter {

			CapabilityType find_common_capability(Codec::Type input_type, Codec::Type output_type)
			{
				if(
					ViaCanonical::From::converters_.find(input_type) != ViaCanonical::From::converters_.end() &&
					ViaCanonical::To::converters_.find(output_type) != ViaCanonical::To::converters_.end()
				)
					return INTERNAL_VIA_CANONICAL;
#if 0
				//@@@ TODO
				else if(
					ViaPipe::From::converters_.find(input_type) != ViaPipe::Find::converters_.end() &&
					ViaPipe::To::converters_.find(output_type) != ViaPipe::To::converters_.end()
				)
					return EXTERNAL_VIA_PIPE;
				else if(
					ViaWaveFile::From::converters_.find(input_type) != ViaWaveFile::From::converters_.end() &&
					ViaWaveFile::To::converters_.find(output_type) != ViaWaveFile::To::converters_.end()
				)
					return EXTERNAL_VIA_WAVE_FILE;
#endif
				else
					return CAPABILITY_TYPE_MAX;
			}

		}; // namespace Converter
	}; // namespace Codec
}; // namespace FUI
