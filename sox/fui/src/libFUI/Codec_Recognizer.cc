/* libFUI - FLAC User Interface library
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "FUI/Codec/Recognizer.h"
#include "FLAC/format.h"
#include "ogg/ogg.h"
#include "private/FILE.h"
#include <assert.h>
#include <stdio.h>

namespace FUI {
	namespace Codec {
		namespace Recognizer {

			namespace local {

				class OggStreamState {
				private:
					::ogg_stream_state obj_;
				public:
					inline OggStreamState() { }
					inline ~OggStreamState() { (void)ogg_stream_clear(&obj_); }
					inline operator const ::ogg_stream_state *() const { return &obj_; }
					inline operator ::ogg_stream_state *() { return &obj_; }
				};

				class OggSyncState {
				private:
					::ogg_sync_state obj_;
				public:
					inline OggSyncState() { }
					inline ~OggSyncState() { (void)ogg_sync_clear(&obj_); }
					inline operator const ::ogg_sync_state *() const { return &obj_; }
					inline operator ::ogg_sync_state *() { return &obj_; }
				};

				bool peek_ogg_page(const char *buffer, unsigned bytes, Codec::Type &format)
				{
					if(bytes >= 6 && 0 == strncmp(buffer, "vorbis", 6)) { //@@@@@ isn't there a packet type byte in front?
						format = Codec::OGG_VORBIS;
						return true;
					}
					// pre-official Ogg FLAC mapping:
					else if(bytes >= FLAC__STREAM_SYNC_LENGTH && 0 == strncmp(buffer, (const char *)::FLAC__STREAM_SYNC_STRING, FLAC__STREAM_SYNC_LENGTH)) {
						format = Codec::OGG_FLAC;
						return true;
					}
					// official Ogg FLAC 1.0+ mapping:
					else if(bytes >= 5 && 0 == strncmp(buffer, "\x7f""FLAC", 5)) {
						format = Codec::OGG_FLAC;
						return true;
					}
					else
						return false;
				}

				std::string peek_ogg(const std::string &path, Codec::Type &format)
				{
					local::OggStreamState stream_state;
					local::OggSyncState sync_state;

					if(::ogg_stream_init(stream_state, 0) != 0)
						return "can't initialize Ogg decoder";

					if(::ogg_sync_init(sync_state) != 0)
						return "@@@@";

					const unsigned bytes_to_read = 4096;
					unsigned bytes;
					char *buffer, *head;
					::ogg_page page;

					head = buffer = ::ogg_sync_buffer(sync_state, bytes_to_read);

					FUI::FILE f(path.c_str(), "rb");

					if(!f.is_open())
						return "can't open file";

					if(::ogg_sync_wrote(sync_state, fread(buffer, 1, bytes_to_read, f)) < 0)
						return "@@@@";

					bytes = 0;

					// All the Ogg-* formats we support have small first pages,
					// which simplifies the handling here:
					if(::ogg_sync_pageout(sync_state, &page) != 1)
						return "unsupported format";

					::ogg_stream_init(stream_state, ::ogg_page_serialno(&page));

					if(::ogg_stream_pagein(stream_state, &page) < 0)
						return "@@@";

					ogg_packet packet;

					while(::ogg_stream_packetout(stream_state, &packet) == 1) {
						memcpy(head, packet.packet, packet.bytes);
						bytes += packet.bytes;
						head += packet.bytes;
					}
					if(!local::peek_ogg_page(buffer, bytes, format))
						return "unsupported format";

					return "";
				}

			};

			std::string divine_format_from_file(const std::string &path, Codec::Type &format, bool use_extension)
			{
				assert(use_extension); //@@@ need to write the 'use_extension == false' case still;

				if(path.rfind(".flac") != std::string::npos)
					format = FUI::Codec::FLAC;
				else if(path.rfind(".mp3") != std::string::npos)
					format = FUI::Codec::MP3;
				else if(path.rfind(".ogg") != std::string::npos)
					return local::peek_ogg(path, format);
				else if(path.rfind(".wav") != std::string::npos)
					format = FUI::Codec::RIFF_WAVE;
				else
					return "unrecognized extension";

				return "";
			}

		}; // namespace Recognizer
	}; // namepace Codec
}; // namepace FUI
