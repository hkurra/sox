/* libFUI - FLAC User Interface library
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "FUI/Codec/Transducer.h"
#include "private/Codec/Converter/Capability.h"
#include "private/Codec/Converter/ViaCanonical/From/Converters.h"
#include "private/Codec/Converter/ViaCanonical/To/Converters.h"
#include "private/Codec/Transcoder/External/Transcoders.h"
#include "private/Codec/Transcoder/Internal/Transcoders.h"
#include <stdio.h> //@@@for stderr

namespace FUI {
	namespace Codec {
		namespace Transducer {

			namespace local {

				static Result transduce_via_canonical_(
					Type input_type,
					std::string input_path,
					const Codec::Options::Decode::Prototype &codec_decode_options,
					const Tag::Options::Decode::Prototype &tag_decode_options,
					Type output_type,
					std::string output_path,
					const Codec::Options::Encode::Prototype &codec_encode_options,
					const Tag::Options::Encode::Prototype &tag_encode_options,
					FUI::ThreadContext *thread_context
				)
				{
					assert(Converter::ViaCanonical::From::converters_.find(input_type) != Converter::ViaCanonical::From::converters_.end());
					assert(Converter::ViaCanonical::To::converters_.find(output_type) != Converter::ViaCanonical::To::converters_.end());

					Converter::ViaCanonical::From::Interface *from = Converter::ViaCanonical::From::converters_[input_type];
					Converter::ViaCanonical::To::Interface *to = Converter::ViaCanonical::To::converters_[output_type];

					assert(0 != from);
					assert(0 != to);

					if(from->status().length() != 0)
						return Result("@@@ 'from' instance: [" + from->status() + "]");
					if(to->status().length() != 0)
						return Result("@@@ 'to' instance: [" + to->status() + "]");

					class ConverterJanitor {
					private:
						Converter::ViaCanonical::From::Interface *from_;
						Converter::ViaCanonical::To::Interface *to_;
						FUI::ThreadContext *thread_context_;
					public:
						ConverterJanitor(Converter::ViaCanonical::From::Interface *from, Converter::ViaCanonical::To::Interface *to, FUI::ThreadContext *thread_context)
						: from_(from), to_(to), thread_context_(thread_context)
							{ assert(0 != from); assert(0 != to); assert(0 != thread_context); }
						~ConverterJanitor()
							{ (void)from_->finish(); (void)to_->finish(); thread_context_->progress_report(from_->percent_complete()); } //@@@@ need to check the values from finish()

					} janitor0(from, to, thread_context);

					Codec::CanonicalForm canonical_form;
					::FLAC::Metadata::VorbisComment tags;
					unsigned max_samples_per_call;

					if(!from->init(input_path, codec_decode_options, tag_decode_options, tags, canonical_form, max_samples_per_call))
						return Result("@@@ from->init() [" + from->status() + "]");

					int32_t *signal[canonical_form.channels_];

					for(unsigned i = 0; i < canonical_form.channels_; i++)
						signal[i] = new int32_t[max_samples_per_call];

					class CanonicalDataJanitor {
					public:
						int32_t **signal_;
						unsigned channels_;
						inline CanonicalDataJanitor(int32_t **signal, unsigned channels):
							signal_(signal), channels_(channels)
							{ assert(0 != signal); }
						inline ~CanonicalDataJanitor()
							{ for(unsigned i = 0; i < channels_; i++) delete [] signal_[i]; }
					} janitor1(signal, canonical_form.channels_);

					if(!to->init(output_path, codec_encode_options, tag_encode_options, tags, canonical_form, max_samples_per_call))
						return Result("@@@ to->init() [" + to->status() + "]");

					unsigned samples;
					unsigned last_percent_complete = 999;
					bool aborted;
					while(!(aborted = thread_context->check_for_abort())) {
						samples = max_samples_per_call;

						if(!from->supply_data(signal, samples))
							return Result("@@@ from->supply_data() [" + from->status() + "]");

						if(samples == 0)
							break;
						else {
							if(!to->feed_data(signal, samples))
								return Result("@@@ to->feed_data() [" + to->status() + "]");
						}

						unsigned percent_complete = from->percent_complete();
						if(percent_complete != last_percent_complete) {
							thread_context->progress_report(percent_complete);
							last_percent_complete = percent_complete;
						}
					}

					return aborted? Result("@@@ aborted") : Result();
				}

			};

			bool is_legal(Type input_type, Type output_type)
			{
				Transcoder::Pair pair(input_type, output_type);
				if(Transcoder::Internal::transcoders_.find(pair) != Transcoder::Internal::transcoders_.end())
					return true;
				else if(Transcoder::External::transcoders_.find(pair) != Transcoder::External::transcoders_.end())
					return true;
				else
					return Converter::find_common_capability(input_type, output_type) != Converter::CAPABILITY_TYPE_MAX;
			}

			Result transduce(
				Type input_type,
				std::string input_path,
				const Codec::Options::Decode::Prototype &codec_decode_options,
				const Tag::Options::Decode::Prototype &tag_decode_options,
				Type output_type,
				std::string output_path,
				const Codec::Options::Encode::Prototype &codec_encode_options,
				const Tag::Options::Encode::Prototype &tag_encode_options,
				FUI::ThreadContext *thread_context
			)
			{
				// verify is_legal(input_type, output_type)
				// verify input_path exists
				// verify input_path is readable
				// verify directory of output_path exists
				// verify directory of output_path is writable

				Transcoder::Pair pair(input_type, output_type);

				if(Transcoder::Internal::transcoders_.find(pair) != Transcoder::Internal::transcoders_.end())
					return Result(Transcoder::Internal::transcoders_[pair]->transduce(input_path, codec_decode_options, tag_decode_options, output_path, codec_encode_options, tag_encode_options, thread_context));
				else if(Transcoder::External::transcoders_.find(pair) != Transcoder::External::transcoders_.end())
					return Result(Transcoder::External::transcoders_[pair]->transduce(input_path, codec_decode_options, tag_decode_options, output_path, codec_encode_options, tag_encode_options, thread_context));
				else {
					const Converter::CapabilityType common_capability = Converter::find_common_capability(input_type, output_type);
					switch(common_capability) {
						case Converter::INTERNAL_VIA_CANONICAL:
							return local::transduce_via_canonical_(input_type, input_path, codec_decode_options, tag_decode_options, output_type, output_path, codec_encode_options, tag_encode_options, thread_context);
						case Converter::EXTERNAL_VIA_PIPE:
							fprintf(stderr, "@@@ TODO: transduce_via_pipe_(%s, %s)\n", input_path.c_str(), output_path.c_str());
							break;
						case Converter::EXTERNAL_VIA_WAVE_FILE:
							fprintf(stderr, "@@@ TODO: transduce_via_wave_file_(%s, %s)\n", input_path.c_str(), output_path.c_str());
							break;
						default:
							assert(0);
					}
				}
				return Result("@@@ unimplemented");
			}

		}; // namespace Transducer
	}; // namespace Codec
}; // namespace FUI
