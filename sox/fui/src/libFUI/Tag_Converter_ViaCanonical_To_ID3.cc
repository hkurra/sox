/* libFUI - FLAC User Interface library
 * Copyright (C) 2004  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h> // for strtoul()
#include <string.h>
#include "share/charset.h"
#include "private/FILE.h"
#include "private/Tag/Converter/ViaCanonical/To/ID3.h"

#ifdef min
#undef min
#endif

namespace FUI {
	namespace Tag {
		namespace Converter {
			namespace ViaCanonical {
				namespace To {

					namespace local {

						static inline unsigned min(const unsigned a, const unsigned b)
						{
							return a < b? a : b;
						}

						//@@@ this is duplicated in Tag_Converter_ViaCanonical_From_ID3.cc, what to do?
						static const char * const genre_table_[] =
						{
							"Blues",		/* 0 */
							"Classic Rock",
							"Country",
							"Dance",
							"Disco",
							"Funk",			/* 5 */
							"Grunge",
							"Hip-Hop",
							"Jazz",
							"Metal",
							"New Age",		/* 10 */		
							"Oldies",
							"Other",
							"Pop",
							"R&B",
							"Rap",			/* 15 */
							"Reggae",
							"Rock",
							"Techno",
							"Industrial",
							"Alternative", 		/* 20 */
							"Ska",
							"Death Metal",
							"Pranks",
							"Soundtrack",
							"Euro-Techno", 		/* 25 */
							"Ambient",
							"Trip-Hop",
							"Vocal",
							"Jazz+Funk",
							"Fusion",		/* 30 */
							"Trance",
							"Classical",
							"Instrumental",
							"Acid",
							"House",		/* 35 */
							"Game",
							"Sound Clip",
							"Gospel",
							"Noise",
							"Altern Rock", 		/* 40 */
							"Bass",
							"Soul",
							"Punk",
							"Space",
							"Meditative",		/* 45 */
							"Instrumental Pop",
							"Instrumental Rock",
							"Ethnic",
							"Gothic",
							"Darkwave",		/* 50 */
							"Techno-Industrial",
							"Electronic",
							"Pop-Folk",
							"Eurodance",
							"Dream",		/* 55 */
							"Southern Rock",
							"Comedy",
							"Cult",
							"Gangsta",
							"Top 40",		/* 60 */
							"Christian Rap",
							"Pop/Funk",
							"Jungle",
							"Native American",
							"Cabaret",		/* 65 */
							"New Wave",
							"Psychadelic",
							"Rave",
							"Showtunes",
							"Trailer",		/* 70 */
							"Lo-Fi",
							"Tribal",
							"Acid Punk",
							"Acid Jazz",
							"Polka",		/* 75 */
							"Retro",
							"Musical",
							"Rock & Roll",
							"Hard Rock",
							"Folk",			/* 80 */
							"Folk/Rock",
							"National Folk",
							"Fast Fusion",
							"Swing",
							"Bebob",		/* 85 */
							"Latin",
							"Revival",
							"Celtic",
							"Bluegrass",
							"Avantgarde",		/* 90 */
							"Gothic Rock",
							"Progressive Rock",
							"Psychedelic Rock",
							"Symphonic Rock",
							"Slow Rock",		/* 95 */
							"Big Band",
							"Chorus",
							"Easy Listening",
							"Acoustic",
							"Humour",		/* 100 */
							"Speech",
							"Chanson",
							"Opera",
							"Chamber Music",
							"Sonata",		/* 105 */
							"Symphony",
							"Booty Bass",
							"Primus",
							"Porn Groove",
							"Satire",		/* 110 */
							"Slow Jam",
							"Club",
							"Tango",
							"Samba",
							"Folklore",		/* 115 */
							"Ballad",
							"Power Ballad",
							"Rhythmic Soul",
							"Freestyle",
							"Duet",			/* 120 */
							"Punk Rock",
							"Drum Solo",
							"A Capella",
							"Euro-House",
							"Dance Hall",		/* 125 */
							"Goa",
							"Drum & Bass",
							"Club-House",
							"Hardcore",
							"Terror",		/* 130 */
							"Indie",
							"BritPop",
							"Negerpunk",
							"Polsk Punk",
							"Beat",			/* 135 */
							"Christian Gangsta Rap",
							"Heavy Metal",
							"Black Metal",
							"Crossover",
							"Contemporary Christian",/* 140 */
							"Christian Rock",
							"Merengue",
							"Salsa",
							"Thrash Metal",
							"Anime",		/* 145 */
							"JPop",
							"Synthpop"
						};

						// convert a Vorbis comment field to an ID3 v1 field
						static void add(const ::FLAC::Metadata::VorbisComment &tags, const char *name, char *value, unsigned width, const std::string &charset)
						{
							//@@@ need to add some missing functions to libFLAC++, for now just use libFLAC routines
							const int i = ::FLAC__metadata_object_vorbiscomment_find_entry_from(tags, /*offset=*/0, name);
							if(i >= 0) {
								::FLAC::Metadata::VorbisComment::Entry entry = tags.get_comment((unsigned)i);
								char *encoded = ::FUI_charset__convert_string(entry.get_field_value(), "UTF-8", charset.c_str());
								if(encoded) {
									memcpy(value, encoded, local::min(width, strlen(encoded)));
									free(encoded);
								}
							}
						}

					} // namespace local

					ID3::ID3():
					Interface()
					{
					}

					ID3::~ID3()
					{
					}

					std::string ID3::write(std::string path, const Tag::Options::Encode::Prototype &encode_options, const ::FLAC::Metadata::VorbisComment &tags)
					{
						const Tag::Options::Encode::ID3 &options = dynamic_cast<const Tag::Options::Encode::ID3 &>(encode_options);

						//@@@ if we ever support permission cloning of the output, we
						//@@@ will need to add code here to chmod +w the file first if
						//@@@ necessary and then put it back

						FUI::FILE f(path, "w+b");

						if(!f.is_open())
							return "error opening file"; //@@@ use ostringstream: "..."+path+"for appending'

						//
						// ID3 v1
						//
						if(options.v1_) {
							// check for existing tag, overwrite or appended depending on result
							{
								char buffer[3];
								long pos = 0;
								if(fseek(f, -128, SEEK_END) >= 0 && fread(buffer, 1, 3, f) == 3 && 0 == strncmp(buffer, "TAG", 3))
									pos = -128L;
								if(fseek(f, pos, SEEK_END) < 0)
									return "error seeking file"; //@@@ use ostringstream
							}

							char buffer[128];

							local::add(tags, "TITLE", buffer+3, 30, options.v1_encoding_);
							local::add(tags, "ARTIST", buffer+33, 30, options.v1_encoding_);
							local::add(tags, "ALBUM", buffer+63, 30, options.v1_encoding_);
							local::add(tags, "DATE", buffer+93, 4, options.v1_encoding_);
							local::add(tags, "COMMENT", buffer+97, 28, options.v1_encoding_); // always do ID3 v1.1

							int i;
							if(0 <= (i = ::FLAC__metadata_object_vorbiscomment_find_entry_from(tags, /*offset=*/0, "TRACKNUMBER")))
								buffer[126] = (char)((unsigned char)strtoul(tags.get_comment((unsigned)i).get_field_value(), 0, 10));

							if(0 <= (i = ::FLAC__metadata_object_vorbiscomment_find_entry_from(tags, /*offset=*/0, "GENRE"))) {
								const char *value = tags.get_comment((unsigned)i).get_field_value();
								buffer[127] = '\xff'; //@@@@@@ what is the value for unknown?
								for(i = (sizeof(local::genre_table_)/sizeof(local::genre_table_[0]))-1; i >= 0; i--) {
									if(0 == strcasecmp(value, local::genre_table_[i])) {
										buffer[127] = (char)i;
										break;
									}
								}
							}

							if(fwrite(buffer, 1, 128, f) != 128)
								return "error writing to file"; //@@@ use ostringstream
						}

						//
						// ID3 v2
						//
						if(options.v2_) {
							//@@@@ TODO, have to actually prepend the file
							return "@@@ ID3v2 writing is unimplemented @@@";
						}

						return "";
					}

					std::string ID3::write(std::string path, const Tag::Options::Encode::Prototype &encode_options, unsigned char *&v1, unsigned char *&v2)
					{
						const Tag::Options::Encode::ID3 &options = dynamic_cast<const Tag::Options::Encode::ID3 &>(encode_options);

						//@@@ if we ever support permission cloning of the output, we
						//@@@ will need to add code here to chmod +w the file first if
						//@@@ necessary and then put it back

						FUI::FILE f(path, "w+b");

						if(!f.is_open())
							return "error opening file"; //@@@ use ostringstream: "..."+path+"for appending'

						//
						// ID3 v1
						//
						if(v1 && options.v1_) {
							// check for existing tag, overwrite or appended depending on result
							{
								char buffer[3];
								long pos = 0;
								if(fseek(f, -128, SEEK_END) >= 0 && fread(buffer, 1, 3, f) == 3 && 0 == strncmp(buffer, "TAG", 3))
									pos = -128L;
								if(fseek(f, pos, SEEK_END) < 0)
									return "error seeking file"; //@@@ use ostringstream
							}

							if(fwrite(v1, 1, 128, f) != 128)
								return "error writing to file"; //@@@ use ostringstream
						}

						//
						// ID3 v2
						//
						if(v2 && options.v2_) {
							//@@@@ TODO, have to actually prepend the file
							return "@@@ ID3v2 writing is unimplemented @@@";
						}

						return "";
					}

					ID3 ID3_;

				} // namespace To
			} // namespace ViaCanonical
		} // namespace Converter
	} // namespace Tag
} // namespace FUI
