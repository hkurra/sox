/* libFUI - FLAC User Interface library
 * Copyright (C) 2004  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "private/FILE.h"
#include "private/Tag/Transcoder/Internal/ID3_to_ID3.h"
#include <stdio.h>

namespace FUI {
	namespace Tag {
		namespace Transcoder {
			namespace Internal {

				std::string ID3_to_ID3::transduce(
					std::string input_path,
					const Tag::Options::Decode::Prototype &decode_options,
					std::string output_path,
					const Tag::Options::Encode::Prototype &encode_options,
					FUI::ThreadContext *thread_context
				)
				{
					fprintf(stderr, "@@@ Transcoder::Internal::ID3_to_ID3::transduce(%s, %s)\n", input_path.c_str(), output_path.c_str());

					thread_context->progress_report(0);

					const Tag::Options::Decode::ID3 &id3_decode_options = dynamic_cast<const Tag::Options::Decode::ID3 &>(decode_options);
					const Tag::Options::Encode::ID3 &id3_encode_options = dynamic_cast<const Tag::Options::Encode::ID3 &>(encode_options);

					//
					// ID3 v1
					//
					if(id3_decode_options.v1_ && id3_encode_options.v1_) {
						FUI::FILE f(input_path, "rb");

						if(!f.is_open())
							return "error opening file for reading"; //@@@ more detail with ostringstream

						unsigned char buffer[128];

						if(fseek(f, -128, SEEK_END) >= 0 && fread(buffer, 1, 128, f) == 128 && 0 == strncmp((const char*)buffer, "TAG", 3)) {
							f.close();

							//@@@ if we ever support permission cloning of the output, we
							//@@@ will need to add code here to chmod +w the file first if
							//@@@ necessary and then put it back

							f.open(output_path, "w+b");

							if(!f.is_open())
								return "error opening file for appending"; //@@@ more detail with ostringstream

							if(fseek(f, 0L, SEEK_END) < 0)
								return "error seeking file"; //@@@ more detail with ostringstream

							if(fwrite(buffer, 1, 128, f) != 128)
								return "error writing file"; //@@@ more detail with ostringstream
						}
					}

					//
					// ID3 v2
					//
					if(id3_decode_options.v2_ && id3_encode_options.v2_) {
						return "@@@@ TODO: ID3v2 writing is unsupported @@@@";
					}

					thread_context->progress_report(100);

					return "";
				}

				ID3_to_ID3 ID3_to_ID3_;

			} // namespace Internal
		} // namespace Transcoder
	} // namespace Tag
} // namespace FUI
