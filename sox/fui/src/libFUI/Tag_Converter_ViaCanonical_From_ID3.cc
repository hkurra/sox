/* libFUI - FLAC User Interface library
 * Copyright (C) 2004  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <assert.h>
#include <string.h>
#include "share/charset.h"
#include "private/FILE.h"
#include "private/Tag/Converter/ViaCanonical/From/ID3.h"

namespace FUI {
	namespace Tag {
		namespace Converter {
			namespace ViaCanonical {
				namespace From {

					namespace local {

						static const char * const genre_table_[] =
						{
							"Blues",		/* 0 */
							"Classic Rock",
							"Country",
							"Dance",
							"Disco",
							"Funk",			/* 5 */
							"Grunge",
							"Hip-Hop",
							"Jazz",
							"Metal",
							"New Age",		/* 10 */		
							"Oldies",
							"Other",
							"Pop",
							"R&B",
							"Rap",			/* 15 */
							"Reggae",
							"Rock",
							"Techno",
							"Industrial",
							"Alternative", 		/* 20 */
							"Ska",
							"Death Metal",
							"Pranks",
							"Soundtrack",
							"Euro-Techno", 		/* 25 */
							"Ambient",
							"Trip-Hop",
							"Vocal",
							"Jazz+Funk",
							"Fusion",		/* 30 */
							"Trance",
							"Classical",
							"Instrumental",
							"Acid",
							"House",		/* 35 */
							"Game",
							"Sound Clip",
							"Gospel",
							"Noise",
							"Altern Rock", 		/* 40 */
							"Bass",
							"Soul",
							"Punk",
							"Space",
							"Meditative",		/* 45 */
							"Instrumental Pop",
							"Instrumental Rock",
							"Ethnic",
							"Gothic",
							"Darkwave",		/* 50 */
							"Techno-Industrial",
							"Electronic",
							"Pop-Folk",
							"Eurodance",
							"Dream",		/* 55 */
							"Southern Rock",
							"Comedy",
							"Cult",
							"Gangsta",
							"Top 40",		/* 60 */
							"Christian Rap",
							"Pop/Funk",
							"Jungle",
							"Native American",
							"Cabaret",		/* 65 */
							"New Wave",
							"Psychadelic",
							"Rave",
							"Showtunes",
							"Trailer",		/* 70 */
							"Lo-Fi",
							"Tribal",
							"Acid Punk",
							"Acid Jazz",
							"Polka",		/* 75 */
							"Retro",
							"Musical",
							"Rock & Roll",
							"Hard Rock",
							"Folk",			/* 80 */
							"Folk/Rock",
							"National Folk",
							"Fast Fusion",
							"Swing",
							"Bebob",		/* 85 */
							"Latin",
							"Revival",
							"Celtic",
							"Bluegrass",
							"Avantgarde",		/* 90 */
							"Gothic Rock",
							"Progressive Rock",
							"Psychedelic Rock",
							"Symphonic Rock",
							"Slow Rock",		/* 95 */
							"Big Band",
							"Chorus",
							"Easy Listening",
							"Acoustic",
							"Humour",		/* 100 */
							"Speech",
							"Chanson",
							"Opera",
							"Chamber Music",
							"Sonata",		/* 105 */
							"Symphony",
							"Booty Bass",
							"Primus",
							"Porn Groove",
							"Satire",		/* 110 */
							"Slow Jam",
							"Club",
							"Tango",
							"Samba",
							"Folklore",		/* 115 */
							"Ballad",
							"Power Ballad",
							"Rhythmic Soul",
							"Freestyle",
							"Duet",			/* 120 */
							"Punk Rock",
							"Drum Solo",
							"A Capella",
							"Euro-House",
							"Dance Hall",		/* 125 */
							"Goa",
							"Drum & Bass",
							"Club-House",
							"Hardcore",
							"Terror",		/* 130 */
							"Indie",
							"BritPop",
							"Negerpunk",
							"Polsk Punk",
							"Beat",			/* 135 */
							"Christian Gangsta Rap",
							"Heavy Metal",
							"Black Metal",
							"Crossover",
							"Contemporary Christian",/* 140 */
							"Christian Rock",
							"Merengue",
							"Salsa",
							"Thrash Metal",
							"Anime",		/* 145 */
							"JPop",
							"Synthpop"
						};

						// convert an ID3 v1 field (which has a fixed width and is NUL-padded) to a Vorbis comment entry and add it to the tags
						static void add(::FLAC::Metadata::VorbisComment &tags, const char *name, const char *value, unsigned width, const std::string &charset)
						{
							char encoded[width+1];
							memcpy(encoded, value, width);
							encoded[width] = '\0';

							char *utf8 = ::FUI_charset__convert_string(encoded, charset.c_str(), "UTF-8");
							if(utf8) {
								tags.append_comment(::FLAC::Metadata::VorbisComment::Entry(name, utf8));
								free(utf8);
							}
						}

					} // namespace local

					ID3::ID3():
					Interface()
					{
					}

					ID3::~ID3()
					{
					}

					std::string ID3::read(std::string path, const Tag::Options::Decode::Prototype &decode_options, ::FLAC::Metadata::VorbisComment &tags)
					{
						const Tag::Options::Decode::ID3 &options = dynamic_cast<const Tag::Options::Decode::ID3 &>(decode_options);

						FUI::FILE f(path, "rb");

						if(!f.is_open())
							return "error opening file"; //@@@ more descriptive with ostringstream

						//
						// ID3 v2
						//
						if(options.v2_) {
							unsigned char buffer[10];

							if(fread(buffer, 1, 10, f) == 10 && 0 == strncmp((const char*)buffer, "ID3", 3)) {
								const unsigned length =
									(unsigned)(buffer[6] & 0x7f) << 21 |
									(unsigned)(buffer[7] & 0x7f) << 14 |
									(unsigned)(buffer[8] & 0x7f) << 7 |
									(unsigned)(buffer[9] & 0x7f);

								//@@@@@@ read tag and parse it
							}
						}

						//
						// ID3 v1
						//
						if(options.v1_) {
							char buffer[128];

							if(fseek(f, -128, SEEK_END) >= 0 && fread(buffer, 1, 128, f) == 128 && 0 == strncmp(buffer, "TAG", 3)) {
								local::add(tags, "TITLE", buffer+3, 30, options.v1_encoding_);
								local::add(tags, "ARTIST", buffer+33, 30, options.v1_encoding_);
								local::add(tags, "ALBUM", buffer+63, 30, options.v1_encoding_);
								local::add(tags, "DATE", buffer+93, 4, options.v1_encoding_);
								local::add(tags, "COMMENT", buffer+97, 30, options.v1_encoding_);
								if(buffer[126]) {
									snprintf(buffer, 4, "%u", (unsigned)buffer[126]);
									tags.append_comment(::FLAC::Metadata::VorbisComment::Entry("TRACKNUMBER", buffer));
								}
								if((unsigned)buffer[127] < (sizeof(local::genre_table_)/sizeof(local::genre_table_[0])))
									local::add(tags, "GENRE", local::genre_table_[(unsigned)buffer[127]], strlen(local::genre_table_[(unsigned)buffer[127]]), "UTF-8");
								else
									local::add(tags, "GENRE", "Unknown", strlen("Unknown"), "UTF-8");
							}
						}

						return "";
					}

					std::string ID3::read(std::string path, const Tag::Options::Decode::Prototype &decode_options, unsigned char *&v1, unsigned char *&v2)
					{
						v1 = v2 = 0;

						const Tag::Options::Decode::ID3 &options = dynamic_cast<const Tag::Options::Decode::ID3 &>(decode_options);

						FUI::FILE f(path, "rb");

						if(!f.is_open())
							return "error opening file"; //@@@ more descriptive with ostringstream

						//
						// ID3 v2
						//
						if(options.v2_) {
							unsigned char buffer[10];

							if(fread(buffer, 1, 10, f) == 10 && 0 == strncmp((const char*)buffer, "ID3", 3)) {
								const unsigned length =
									(unsigned)(buffer[6] & 0x7f) << 21 |
									(unsigned)(buffer[7] & 0x7f) << 14 |
									(unsigned)(buffer[8] & 0x7f) << 7 |
									(unsigned)(buffer[9] & 0x7f);

								v2 = new unsigned char[10 + length];
								memcpy(v2, buffer, 10);
								if(fread(v2+10, 1, length, f) != length) {
									delete [] v2;
									v2 = 0;
								}
							}
						}

						//
						// ID3 v1
						//
						if(options.v1_) {
							v1 = new unsigned char[128];

							if(fseek(f, -128, SEEK_END) < 0 || fread(v1, 1, 128, f) != 128 || strncmp((const char*)v1, "TAG", 3)) {
								delete [] v1;
								v1 = 0;
							}
						}

						return "";
					}

					ID3 ID3_;

				} // namespace From
			} // namespace ViaCanonical
		} // namespace Converter
	} // namespace Tag
} // namespace FUI
