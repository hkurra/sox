/* libFUI - FLAC User Interface library
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "FUI/Transducer/Element.h"

namespace FUI {
	namespace Transducer {

		Element::PrimaryKey Element::next_primary_key_ = 0;

		Element::Element(
			std::string input_path,
			std::string output_path,
			Codec::Type input_format,
			Codec::Type output_format,
			const Codec::Options::Decode::Prototype *codec_decode_options,
			const Codec::Options::Encode::Prototype *codec_encode_options,
			const Tag::Options::Decode::Prototype *tag_decode_options,
			const Tag::Options::Encode::Prototype *tag_encode_options
		):
		input_path_(input_path),
		output_path_(output_path),
		input_format_(input_format),
		output_format_(output_format),
		codec_decode_options_(Codec::Options::Decode::clone(codec_decode_options)),
		codec_encode_options_(Codec::Options::Encode::clone(codec_encode_options)),
		tag_decode_options_(Tag::Options::Decode::clone(tag_decode_options)),
		tag_encode_options_(Tag::Options::Encode::clone(tag_encode_options)),
		primary_key_(next_primary_key_++),
		state_(PENDING),
		percent_complete_(0)
		{
		}

		Element::~Element()
		{
			delete(codec_decode_options_);
			delete(codec_encode_options_);
			delete(tag_decode_options_);
			delete(tag_encode_options_);
		}

		const char *Element::state_as_string() const
		{
			switch(state()) {
				case FINISHED:
					return "FINISHED";
				case PENDING:
					return "PENDING";
				case WORKING:
					return "WORKING";
				default:
					assert(0);
					return "@@@ state error @@@";
			}
		}

	}; // namespace Transducer
}; // namespace FUI
