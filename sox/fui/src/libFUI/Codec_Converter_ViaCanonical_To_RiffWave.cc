/* libFUI - FLAC User Interface library
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <assert.h>
#include <stdio.h>
#include <string.h> // for memcpy()
#ifdef FUI__OLD_STRSTREAM
#include <strstream> // for ostrstream
#else
#include <sstream> // for ostringstream
#endif
#include "private/Codec/Converter/ViaCanonical/To/RiffWave.h"

namespace FUI {
	namespace Codec {
		namespace Converter {
			namespace ViaCanonical {
				namespace To {

					namespace local {

						static inline bool write_uint16(::FILE *file, uint16_t val)
						{
							unsigned char buf[2];
							buf[0] = val; val >>= 8;
							buf[1] = val;
							return fwrite(buf, 1, 2, file) == 2;
						}

						static inline bool write_uint32(::FILE *file, uint32_t val)
						{
							unsigned char buf[4];
							buf[0] = val; val >>= 8;
							buf[1] = val; val >>= 8;
							buf[2] = val; val >>= 8;
							buf[3] = val;
							return fwrite(buf, 1, 4, file) == 4;
						}

						static void format_output(int32_t *data[], unsigned samples, unsigned channels, unsigned bps, unsigned char *pcm)
						{
							if(bps == 8) {
								for(unsigned sample = 0; sample < samples; sample++)
									for(unsigned channel = 0; channel < channels; channel++)
										*(pcm++) = data[channel][sample] + 128;
							}
							else if(bps == 16) {
								for(unsigned sample = 0; sample < samples; sample++)
									for(unsigned channel = 0; channel < channels; channel++) {
										int32_t s = data[channel][sample];
										*(pcm++) = s; s >>= 8;
										*(pcm++) = s;
									}
							}
							else if(bps == 24) {
								for(unsigned sample = 0; sample < samples; sample++)
									for(unsigned channel = 0; channel < channels; channel++) {
										int32_t s = data[channel][sample];
										*(pcm++) = s; s >>= 8;
										*(pcm++) = s; s >>= 8;
										*(pcm++) = s;
									}
							}
							else if(bps == 32) {
								for(unsigned sample = 0; sample < samples; sample++)
									for(unsigned channel = 0; channel < channels; channel++) {
										int32_t s = data[channel][sample];
										*(pcm++) = s; s >>= 8;
										*(pcm++) = s; s >>= 8;
										*(pcm++) = s; s >>= 8;
										*(pcm++) = s;
									}
							}
						}

					};

					RiffWave::RiffWave():
					Interface(),
					status_(""),
					file_(0),
					canonical_form_(),
					samples_encoded_(0)
					{
					}

					RiffWave::~RiffWave()
					{
					}

					bool RiffWave::init(
						std::string path,
						const Codec::Options::Encode::Prototype &codec_encode_options,
						const Tag::Options::Encode::Prototype &tag_encode_options,
						::FLAC::Metadata::VorbisComment &tags,
						const Codec::CanonicalForm &canonical_form,
						unsigned &max_samples_per_call
					)
					{
						(void)max_samples_per_call;

						assert(0 == file_);

						const Codec::Options::Encode::RiffWave &codec_options = dynamic_cast<const Codec::Options::Encode::RiffWave &>(codec_encode_options);
						const Tag::Options::Encode::Null &tag_options = dynamic_cast<const Tag::Options::Encode::Null &>(tag_encode_options);

						(void)codec_options;
						(void)tag_options;
						(void)tags;

						status_ = "";
						samples_encoded_ = 0;

						if(canonical_form.sample_resolution_ != 8 && canonical_form.sample_resolution_ != 16 && canonical_form.sample_resolution_ != 24 && canonical_form.sample_resolution_ != 32) {
#ifdef FUI__OLD_STRSTREAM
							std::ostrstream os;
#else
							std::ostringstream os;
#endif
							os << "unsupported bits-per-sample (" << canonical_form.sample_resolution_ << ")";
							status_ = os.str();
							return false;
						}
						if(canonical_form.channels_ == 0 || canonical_form.channels_ > 8) {
#ifdef FUI__OLD_STRSTREAM
							std::ostrstream os;
#else
							std::ostringstream os;
#endif
							os << "unsupported number of channels (" << canonical_form.channels_ << ")";
							status_ = os.str();
							return false;
						}

						canonical_form_ = canonical_form;

						if(0 == (file_ = fopen(path.c_str(), "w+b"))) {
							status_ = "error opening file for writing";
							return false;
						}

						// Write the RIFF header, 'fmt' subchunk, and 'data' header.
						// We'll go back and fill in the chunk sizes later.
						if(fwrite("RIFF\000\000\000\000WAVEfmt \020\000\000\000\001\000", 1, 22, file_) < 22) {
							status_ = "write error";
							return false;
						}
						const unsigned bytes_per_sample = (canonical_form_.sample_resolution_+7) / 8;
						const unsigned bytes_per_wide_sample = canonical_form_.channels_ * bytes_per_sample;
#define LOCAL__WRITE_UINT16(x) if(!local::write_uint16(file_, (x))) { status_ = "write error"; return false; }
#define LOCAL__WRITE_UINT32(x) if(!local::write_uint32(file_, (x))) { status_ = "write error"; return false; }
						LOCAL__WRITE_UINT16(canonical_form_.channels_)
						LOCAL__WRITE_UINT32(canonical_form_.sample_rate_)
						LOCAL__WRITE_UINT32(canonical_form_.sample_rate_ * bytes_per_wide_sample)
						LOCAL__WRITE_UINT16(bytes_per_wide_sample)
						LOCAL__WRITE_UINT16(canonical_form_.sample_resolution_)
#undef LOCAL__WRITE_UINT16
#undef LOCAL__WRITE_UINT32
						if(fwrite("data\000\000\000\000", 1, 8, file_) < 8) {
							status_ = "write error";
							return false;
						}

						return true;
					}

					bool RiffWave::feed_data(int32_t *data[], unsigned &samples)
					{
						assert(0 != file_);

						if(samples > 0) {
							const unsigned bytes_per_sample = (canonical_form_.sample_resolution_+7) / 8;
							const unsigned bytes_per_wide_sample = canonical_form_.channels_ * bytes_per_sample;
							const unsigned bytes_to_encode = bytes_per_wide_sample * samples;
							unsigned char pcm[bytes_to_encode];

							local::format_output(data, samples, canonical_form_.channels_, canonical_form_.sample_resolution_, pcm);

							if(fwrite(pcm, 1, bytes_to_encode, file_) < bytes_to_encode) {
								status_ = "write error";
								return false;
							}
						}

						samples_encoded_ += samples;
						return true;
					}

					bool RiffWave::finish()
					{
						if(0 != file_) {
							const unsigned bytes_per_sample = (canonical_form_.sample_resolution_+7) / 8;
							const unsigned bytes_per_wide_sample = canonical_form_.channels_ * bytes_per_sample;
							const unsigned data_size = samples_encoded_ * bytes_per_wide_sample;
							if(fseek(file_, 4, SEEK_SET) < 0) {
								status_ = "seek error";
								return false;
							}
							if(!local::write_uint32(file_, data_size + 36)) {
								status_ = "write error";
								return false;
							}
							if(fseek(file_, 40, SEEK_SET) < 0) {
								status_ = "seek error";
								return false;
							}
							if(!local::write_uint32(file_, data_size)) {
								status_ = "write error";
								return false;
							}
							fclose(file_);
							file_ = 0;
						}
						return true;
					}

					RiffWave RiffWave_;

				}; // namespace To
			}; // namespace ViaCanonical
		}; // namespace Converter
	}; // namespace Codec
}; // namespace FUI
