/* libFUI - FLAC User Interface library
 * Copyright (C) 2004  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h> // for strtoul()
#include <string.h>
#include "private/Tag/Converter/ViaCanonical/To/FLAC.h"

namespace FUI {
	namespace Tag {
		namespace Converter {
			namespace ViaCanonical {
				namespace To {

					FLAC::FLAC():
					Interface()
					{
					}

					FLAC::~FLAC()
					{
					}

					std::string FLAC::write(std::string path, const Tag::Options::Encode::Prototype &encode_options, const ::FLAC::Metadata::VorbisComment &tags)
					{
						const Tag::Options::Encode::VorbisComment &options = dynamic_cast<const Tag::Options::Encode::VorbisComment &>(encode_options);

						//@@@ if we ever support permission cloning of the output, we
						//@@@ will need to add code here to chmod +w the file first if
						//@@@ necessary and then put it back

						//@@@ TODO: this whole thing should go into libFLAC++ as set_tags()..., use new set_tags() everywhere in flac also

						::FLAC::Metadata::Chain chain;
						if(!chain.is_valid())
							return "memory allocation error";

						if(!chain.read(path.c_str()))
							return "error reading metadata"; //@@@ more descriptive, use ostringstream

						::FLAC::Metadata::Iterator iterator;
						if(!iterator.is_valid())
							return "memory allocation error";

						iterator.init(chain);

						FLAC__bool got_vorbis_comments = false;
						do {
							if(iterator.get_block_type() == ::FLAC__METADATA_TYPE_VORBIS_COMMENT)
								got_vorbis_comments = true;
						} while(!got_vorbis_comments && iterator.next());

						::FLAC::Metadata::Prototype *block = ::FLAC::Metadata::clone(&tags);
						if(0 == block)
							return "memory allocation error";

						FLAC__bool ok;
						if(got_vorbis_comments)
							ok = iterator.set_block(block);
						else
							ok = iterator.insert_block_after(block);

						if(ok) {
							chain.sort_padding();
							ok = chain.write(/*use_padding=*/true, /*preserve_file_stats=*/true);
						}

						return ok? "" : "@@@ writing block @@@";
					}

					FLAC FLAC_;

				} // namespace To
			} // namespace ViaCanonical
		} // namespace Converter
	} // namespace Tag
} // namespace FUI
