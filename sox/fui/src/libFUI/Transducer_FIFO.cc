/* libFUI - FLAC User Interface library
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "FUI/Transducer/FIFO.h"

namespace FUI {
	namespace Transducer {

		FIFO::FIFO()
		{
		}

		FIFO::~FIFO()
		{
			for(unsigned i = 0; i < elements_.size(); i++)
				delete elements_[i];
		}

		Element *FIFO::find(Element::PrimaryKey primary_key)
		{
			return elements_index_.find(primary_key) == elements_index_.end()? 0 : elements_index_[primary_key];
		}

		Element *FIFO::first_pending()
		{
			for(Elements::iterator it = elements_.begin(); it != elements_.end(); it++)
				if((*it)->state() == Element::PENDING)
					return *it;

			return 0;
		}

		void FIFO::dequeue()
		{
			assert(!elements_.empty());
			Element *front_element = elements_[0];
			const Element::PrimaryKey front_key = front_element->primary_key();
			ElementsIndex::iterator it = elements_index_.find(front_key);
			assert(it != elements_index_.end());
			elements_.pop_front();
			elements_index_.erase(it);
			delete front_element;
		}

		void FIFO::enqueue(Element *element)
		{
			elements_.push_back(element);
			elements_index_[element->primary_key()] = element;
		}

	}; // namespace Transducer
}; // namespace FUI
