/* libFUI - FLAC User Interface library
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "FUI/Codec/Type.h"

namespace FUI {
	namespace Codec {

		const char * const TypeString[TYPE_MAX] = {
			"FLAC",
			"MP3",
			"OGG_FLAC",
			"OGG_VORBIS",
			"RIFF_WAVE"
		};

		const char * const TypeDescription[TYPE_MAX] = {
			"FLAC",
			"MP3",
			"Ogg FLAC",
			"Ogg Vorbis",
			"RIFF WAVE"
		};

		const char * const TypeExtension[TYPE_MAX] = {
			".flac",
			".mp3",
			".ogg",
			".ogg",
			".wav"
		};

	}; // namepace Codec
}; // namepace FUI
