/* libFUI - FLAC User Interface library
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <assert.h>
#include <stdio.h>
#include <string.h> // for memcpy()
#include "FLAC++/metadata.h"
#include "private/Codec/Converter/ViaCanonical/From/FLAC.h"
#include "private/Tag/Converter/ViaCanonical/From/FLAC.h"

#ifdef min
#undef min
#endif

namespace FUI {
	namespace Codec {
		namespace Converter {
			namespace ViaCanonical {
				namespace From {

					namespace local {

						static inline unsigned min(const unsigned a, const unsigned b)
						{
							return a < b? a : b;
						}

					}

					FLAC::FLAC():
					Interface(),
					path_(),
					status_(""),
					percent_complete_(0),
					samples_to_decode_(0),
					samples_decoded_(0),
					decoder_(decoder_error_occurred_),
					decoder_error_occurred_(false)
					{
						if(!decoder_.is_valid())
							status_ = "invalid decoder";
					}

					FLAC::~FLAC()
					{
					}

					bool FLAC::init(
						std::string path,
						const Codec::Options::Decode::Prototype &codec_decode_options,
						const Tag::Options::Decode::Prototype &tag_decode_options,
						::FLAC::Metadata::VorbisComment &tags,
						Codec::CanonicalForm &canonical_form,
						unsigned &max_samples_per_call
					)
					{
						assert(decoder_.is_valid());

						const Codec::Options::Decode::FLAC &codec_options = dynamic_cast<const Codec::Options::Decode::FLAC &>(codec_decode_options);
						const Tag::Options::Decode::VorbisComment &tag_options = dynamic_cast<const Tag::Options::Decode::VorbisComment &>(tag_decode_options);

						path_ = path;
						status_ = "";
						percent_complete_ = 0;
						samples_to_decode_ = samples_decoded_ = 0;

						if(!decoder_.set_md5_checking(codec_options.verify_md5_))
							assert(0);

						if(!decoder_.set_filename(path_.c_str()))
							assert(0);

						if(!decoder_.set_metadata_ignore_all())
							assert(0);

						if((::FLAC__FileDecoderState)decoder_.init() != ::FLAC__FILE_DECODER_OK) {
							status_ = decoder_.get_state().resolved_as_cstring(decoder_);
							return false;
						}

						if(!decoder_.process_until_end_of_metadata()) {
							status_ = decoder_.get_state().resolved_as_cstring(decoder_);
							return false;
						}

						::FLAC::Metadata::StreamInfo streaminfo;
						if(!::FLAC::Metadata::get_streaminfo(path_.c_str(), streaminfo)) {
							status_ = "invalid FLAC file";
							return false;
						}

						samples_to_decode_ = streaminfo.get_total_samples();
						canonical_form.sample_rate_ = streaminfo.get_sample_rate();
						canonical_form.sample_resolution_ = streaminfo.get_bits_per_sample();
						canonical_form.channels_ = streaminfo.get_channels();
						max_samples_per_call = streaminfo.get_max_blocksize();
						if(max_samples_per_call == 0)
							max_samples_per_call = FLAC__MAX_BLOCK_SIZE;

						decoder_.set_target_canonical_form(canonical_form);

						(void) Tag::Converter::ViaCanonical::From::FLAC_.read(path, tag_options, tags);

						return true;
					}

					bool FLAC::supply_data(int32_t *data[], unsigned &samples)
					{
						assert(decoder_.is_valid());
						assert(sizeof(int32_t) == sizeof(FLAC__int32));

						unsigned decoded_samples = 0;

						decoder_error_occurred_ = false;

						if(!decoder_.process_single(data, &decoded_samples, samples)) {
							status_ = decoder_.get_state().resolved_as_cstring(decoder_);
							return false;
						}

						if(decoder_.get_state() == ::FLAC__FILE_DECODER_END_OF_FILE)
							decoded_samples = 0;

						samples = decoded_samples;

						if(decoder_error_occurred_)
							status_ = decoder_.get_state().resolved_as_cstring(decoder_);

						samples_decoded_ += samples;
						if(0 != samples_to_decode_)
							percent_complete_ = local::min(99, 100 * samples_decoded_ / samples_to_decode_);
						else
							percent_complete_ = 0;

						return !decoder_error_occurred_;
					}

					bool FLAC::finish()
					{
						assert(decoder_.is_valid());

						if(!decoder_.finish()) {
							status_ = "MD5 mismatch";
							return false;
						}

						percent_complete_ = 100;

						return true;
					}

					FLAC::Decoder::Decoder(bool &error_occurred):
					::FLAC::Decoder::File(),
					error_occurred_(error_occurred),
					target_canonical_form_(),
					target_data_(0),
					target_samples_(0),
					max_samples_per_call_(0)
					{
					}

					bool FLAC::Decoder::process_single(int32_t *data[], unsigned *decoded_samples, unsigned max_samples_per_call)
					{
						target_data_ = data;
						target_samples_ = decoded_samples;
						max_samples_per_call_ = max_samples_per_call;
						return ::FLAC::Decoder::File::process_single();
					}

					::FLAC__StreamDecoderWriteStatus FLAC::Decoder::write_callback(const ::FLAC__Frame *frame, const FLAC__int32 * const buffer[])
					{
						assert(0 != target_data_);
						assert(0 != target_samples_);

						if(error_occurred_)
							return ::FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;

						//
						// Check that the audio format has not changed mid-stream:
						//
						if(frame->header.channels != target_canonical_form_.channels_) {
							error_occurred_ = true; //@@@result pass up to parent->status_
							return ::FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
						}
						if(frame->header.bits_per_sample != target_canonical_form_.sample_resolution_) {
							error_occurred_ = true; //@@@result pass up to parent->status_
							return ::FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
						}
						if(frame->header.sample_rate != target_canonical_form_.sample_rate_) {
							error_occurred_ = true; //@@@result pass up to parent->status_
							return ::FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
						}

						//
						// Check that we have space enough to store the decoded data:
						//
						if(frame->header.blocksize > max_samples_per_call_) {
							error_occurred_ = true; //@@@result pass up to parent->status_
							return ::FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
						}

						*target_samples_ = frame->header.blocksize;

						for(unsigned channel = 0; channel < frame->header.channels; channel++) {
							assert(0 != target_data_[channel]);
							memcpy(target_data_[channel], buffer[channel], sizeof(FLAC__int32) * *target_samples_);
						}

						return ::FLAC__STREAM_DECODER_WRITE_STATUS_CONTINUE;
					}

					void FLAC::Decoder::metadata_callback(const ::FLAC__StreamMetadata *)
					{
						// We should never get a metadata callback because
						// we decoder_.set_metadata_ignore_all(); if we do
						// there's a bug in the decoder which we want to
						// know about.
						assert(0);
					}

					void FLAC::Decoder::error_callback(::FLAC__StreamDecoderErrorStatus status)
					{
						(void)status;//@@@result pass up to parent->status_ FLAC__StreamDecoderErrorStatusString[status];
						error_occurred_ = true;
					}

					FLAC FLAC_;

				}; // namespace From
			}; // namespace ViaCanonical
		}; // namespace Converter
	}; // namespace Codec
}; // namespace FUI
