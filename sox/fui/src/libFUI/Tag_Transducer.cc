/* libFUI - FLAC User Interface library
 * Copyright (C) 2004  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "FUI/Tag/Transducer.h"
#include "private/Tag/Converter/ViaCanonical/From/Converters.h"
#include "private/Tag/Converter/ViaCanonical/To/Converters.h"
#include "private/Tag/Transcoder/Internal/Transcoders.h"
#include <stdio.h> //@@@for stderr

namespace FUI {
	namespace Tag {
		namespace Transducer {

			namespace local {

				static Result transduce_via_canonical_(
					Type input_type,
					std::string input_path,
					const Tag::Options::Decode::Prototype &decode_options,
					Type output_type,
					std::string output_path,
					const Tag::Options::Encode::Prototype &encode_options,
					FUI::ThreadContext *thread_context
				)
				{
					assert(Converter::ViaCanonical::From::converters_.find(input_type) != Converter::ViaCanonical::From::converters_.end());
					assert(Converter::ViaCanonical::To::converters_.find(output_type) != Converter::ViaCanonical::To::converters_.end());

					Converter::ViaCanonical::From::Interface *from = Converter::ViaCanonical::From::converters_[input_type];
					Converter::ViaCanonical::To::Interface *to = Converter::ViaCanonical::To::converters_[output_type];

					assert(0 != from);
					assert(0 != to);

					::FLAC::Metadata::VorbisComment tags;

					bool aborted;
					if(!(aborted = thread_context->check_for_abort())) {
						std::string result;

						result = from->read(input_path, decode_options, tags);
						if(result.length())
							return Result("@@@ from->read() [" + result + "]");

						if(tags.get_num_comments()) {
							result = to->write(output_path, encode_options, tags);
							if(result.length())
								return Result("@@@ to->write() [" + result + "]");
						}
					}

					return aborted? Result("@@@ aborted") : Result();
				}

			};

			bool is_legal(Type input_type, Type output_type)
			{
				Transcoder::Pair pair(input_type, output_type);
				if(Transcoder::Internal::transcoders_.find(pair) != Transcoder::Internal::transcoders_.end())
					return true;
				else
					return
						Converter::ViaCanonical::From::converters_.find(input_type) != Converter::ViaCanonical::From::converters_.end() &&
						Converter::ViaCanonical::To::converters_.find(output_type) != Converter::ViaCanonical::To::converters_.end()
					;
			}

			Result transduce(
				Type input_type,
				std::string input_path,
				const Tag::Options::Decode::Prototype &decode_options,
				Type output_type,
				std::string output_path,
				const Tag::Options::Encode::Prototype &encode_options,
				//@@@ do we need the thread_context at all?  it's only useful when copying tags that rewrite the file and take a long time
				FUI::ThreadContext *thread_context
			)
			{
				// verify is_legal(input_type, output_type)
				// verify input_path exists
				// verify input_path is readable
				// verify output_path exists
				// verify output_path is readable

				Transcoder::Pair pair(input_type, output_type);

				if(Transcoder::Internal::transcoders_.find(pair) != Transcoder::Internal::transcoders_.end())
					return Result(Transcoder::Internal::transcoders_[pair]->transduce(input_path, decode_options, output_path, encode_options, thread_context));
				else
					return local::transduce_via_canonical_(input_type, input_path, decode_options, output_type, output_path, encode_options, thread_context);
			}

		} // namespace Transducer
	} // namespace Tag
} // namespace FUI
