/* libFUI - FLAC User Interface library
 * Copyright (C) 2004  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef FUI__private_Tag_Converter_ViaCanonical_To_ID3_h
#define FUI__private_Tag_Converter_ViaCanonical_To_ID3_h

#include "Interface.h"

namespace FUI {
	namespace Tag {
		namespace Converter {
			namespace ViaCanonical {
				namespace To {

					class ID3 : public Interface {
					public:
						ID3();
						virtual ~ID3();

						// from Interface
						virtual std::string write(std::string path, const Tag::Options::Encode::Prototype &encode_options, const ::FLAC::Metadata::VorbisComment &tag);

						// writes the raw byte buffers.  v1/v2 will be not be written if NULL or options do no request it.
						std::string write(std::string path, const Tag::Options::Encode::Prototype &encode_options, unsigned char *&v1, unsigned char *&v2);
					};

					extern ID3 ID3_;

				}; // namespace To
			}; // namespace ViaCanonical
		}; // namespace Converter
	}; // namespace Tag
}; // namespace FUI

#endif
