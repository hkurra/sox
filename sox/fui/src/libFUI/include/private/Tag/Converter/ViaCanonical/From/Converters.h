/* libFUI - FLAC User Interface library
 * Copyright (C) 2004  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef FUI__private_Tag_Converter_ViaCanonical_From_Converters_h
#define FUI__private_Tag_Converter_ViaCanonical_From_Converters_h

#include <map>
#include "FUI/Tag/Type.h"
#include "Interface.h"

namespace FUI {
	namespace Tag {
		namespace Converter {
			namespace ViaCanonical {
				namespace From {

					class Converters: public std::map<Tag::Type, Interface*> {
					public:
						Converters();
					};

					extern Converters converters_;

				}; // namespace From
			}; // namespace ViaCanonical
		}; // namespace Converter
	}; // namespace Tag
}; // namespace FUI

#endif
