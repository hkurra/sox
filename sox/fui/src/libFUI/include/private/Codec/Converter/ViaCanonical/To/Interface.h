/* libFUI - FLAC User Interface library
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef FUI__private_Codec_Converter_ViaCanonical_To_Interface_h
#define FUI__private_Codec_Converter_ViaCanonical_To_Interface_h

#include <inttypes.h>
#include <string>
#include "FLAC++/metadata.h"
#include "FUI/Codec/CanonicalForm.h"
#include "FUI/Codec/Options.h"
#include "FUI/Tag/Options.h"

namespace FUI {
	namespace Codec {
		namespace Converter {
			namespace ViaCanonical {
				namespace To {

					class Interface {
					public:
						virtual std::string status() const = 0;
						virtual bool init(
							std::string path, //in
							const Codec::Options::Encode::Prototype &codec_encode_options, //in
							const Tag::Options::Encode::Prototype &tag_encode_options, //in
							::FLAC::Metadata::VorbisComment &tags, //in
							const Codec::CanonicalForm &canonical_form, //out
							unsigned &max_samples_per_call //out
						) = 0;
						virtual bool feed_data(int32_t *data[], unsigned &samples) = 0;
						virtual bool finish() = 0;
					};

				}; // namespace To
			}; // namespace ViaCanonical
		}; // namespace Converter
	}; // namespace Codec
}; // namespace FUI

#endif
