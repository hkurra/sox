/* libFUI - FLAC User Interface library
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef FUI__private_Codec_Converter_ViaCanonical_To_FLAC_h
#define FUI__private_Codec_Converter_ViaCanonical_To_FLAC_h

#include "FLAC++/encoder.h"
#include "Interface.h"

namespace FUI {
	namespace Codec {
		namespace Converter {
			namespace ViaCanonical {
				namespace To {

					class FLAC : public Interface {
					public:
						FLAC();
						virtual ~FLAC();

						// from Interface
						virtual inline std::string status() const;
						virtual bool init(
							std::string path,
							const Codec::Options::Encode::Prototype &codec_encode_options,
							const Tag::Options::Encode::Prototype &tag_encode_options,
							::FLAC::Metadata::VorbisComment &tags,
							const Codec::CanonicalForm &canonical_form,
							unsigned &max_samples_per_call
						);
						virtual bool feed_data(int32_t *data[], unsigned &samples);
						virtual bool finish();
					protected:
						class Encoder : public ::FLAC::Encoder::File {
						public:
							Encoder();
							// Synthetic destructor.
							// No copy-constructor or operator= because of the ancestry.
						protected:
							// from ::FLAC::Encoder::File
							virtual void progress_callback(FLAC__uint64 bytes_written, FLAC__uint64 samples_written, unsigned frames_written, unsigned total_frames_estimate);
						};

						std::string path_;
						std::string status_;
						Encoder encoder_;
						Codec::CanonicalForm canonical_form_;
					};

					inline std::string FLAC::status() const
					{ return status_; }

					extern FLAC FLAC_;

				}; // namespace To
			}; // namespace ViaCanonical
		}; // namespace Converter
	}; // namespace Codec
}; // namespace FUI

#endif
