/* libFUI - FLAC User Interface library
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef FUI__private_Codec_Converter_ViaCanonical_From_RiffWave_h
#define FUI__private_Codec_Converter_ViaCanonical_From_RiffWave_h

#include <stdio.h> // for 'FILE'
#include "Interface.h"

namespace FUI {
	namespace Codec {
		namespace Converter {
			namespace ViaCanonical {
				namespace From {

					class RiffWave : public Interface {
					public:
						RiffWave();
						virtual ~RiffWave();

						// from Interface
						virtual inline std::string status() const;
						virtual bool init(
							std::string path,
							const Codec::Options::Decode::Prototype &codec_decode_options,
							const Tag::Options::Decode::Prototype &tag_decode_options,
							::FLAC::Metadata::VorbisComment &tags,
							Codec::CanonicalForm &canonical_form,
							unsigned &max_samples_per_call
						);
						virtual bool supply_data(int32_t *data[], unsigned &samples);
						virtual bool finish();
						virtual inline unsigned percent_complete() const;
					protected:
						std::string status_;
						unsigned percent_complete_;
						::FILE *file_;
						Codec::CanonicalForm canonical_form_;
						unsigned bytes_per_sample_;
						unsigned samples_to_decode_;
						unsigned samples_decoded_;
					};

					inline std::string RiffWave::status() const
					{ return status_; }

					inline unsigned RiffWave::percent_complete() const
					{ return percent_complete_; }

					extern RiffWave RiffWave_;

				}; // namespace From
			}; // namespace ViaCanonical
		}; // namespace Converter
	}; // namespace Codec
}; // namespace FUI

#endif
