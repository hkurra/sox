/* libFUI - FLAC User Interface library
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef FUI__private_Codec_Converter_ViaCanonical_From_FLAC_h
#define FUI__private_Codec_Converter_ViaCanonical_From_FLAC_h

#include "FLAC++/decoder.h"
#include "Interface.h"

namespace FUI {
	namespace Codec {
		namespace Converter {
			namespace ViaCanonical {
				namespace From {

					class FLAC : public Interface {
					public:
						FLAC();
						virtual ~FLAC();

						// from Interface
						virtual inline std::string status() const;
						virtual bool init(
							std::string path,
							const Codec::Options::Decode::Prototype &codec_decode_options,
							const Tag::Options::Decode::Prototype &tag_decode_options,
							::FLAC::Metadata::VorbisComment &tags,
							Codec::CanonicalForm &canonical_form,
							unsigned &max_samples_per_call
						);
						virtual bool supply_data(int32_t *data[], unsigned &samples);
						virtual bool finish();
						virtual inline unsigned percent_complete() const;
					protected:
						class Decoder : public ::FLAC::Decoder::File {
						public:
							Decoder(bool &error_occurred);
							// Synthetic destructor.
							// No copy-constructor or operator= because of the ancestry.

							inline void set_target_canonical_form(Codec::CanonicalForm);
							bool process_single(int32_t *data[], unsigned *decoded_samples, unsigned max_samples_per_call);
						protected:
							// from ::FLAC::Decoder::File
							virtual ::FLAC__StreamDecoderWriteStatus write_callback(const ::FLAC__Frame *frame, const FLAC__int32 * const buffer[]);
							virtual void metadata_callback(const ::FLAC__StreamMetadata *metadata);
							virtual void error_callback(::FLAC__StreamDecoderErrorStatus status);

							bool &error_occurred_;
							Codec::CanonicalForm target_canonical_form_;
							int32_t **target_data_;
							unsigned *target_samples_;
							unsigned max_samples_per_call_;
						};

						std::string path_;
						std::string status_;
						unsigned percent_complete_;
						::FLAC__uint64 samples_to_decode_;
						::FLAC__uint64 samples_decoded_;
						Decoder decoder_;
						bool decoder_error_occurred_;
					};

					inline std::string FLAC::status() const
					{ return status_; }

					inline unsigned FLAC::percent_complete() const
					{ return percent_complete_; }

					inline void FLAC::Decoder::set_target_canonical_form(Codec::CanonicalForm x)
					{ target_canonical_form_ = x; }

					extern FLAC FLAC_;

				}; // namespace From
			}; // namespace ViaCanonical
		}; // namespace Converter
	}; // namespace Codec
}; // namespace FUI

#endif
