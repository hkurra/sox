/* libFUI - FLAC User Interface library
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef FUI__private_Codec_Transcoder_Internal_FLAC_to_FLAC_h
#define FUI__private_Codec_Transcoder_Internal_FLAC_to_FLAC_h

#include "Interface.h"

namespace FUI {
	namespace Codec {
		namespace Transcoder {
			namespace Internal {

				class FLAC_to_FLAC : public Interface {
				public:
					virtual std::string transduce(
						std::string input_path,
						const Codec::Options::Decode::Prototype &codec_decode_options,
						const Tag::Options::Decode::Prototype &tag_decode_options,
						std::string output_path,
						const Codec::Options::Encode::Prototype &codec_encode_options,
						const Tag::Options::Encode::Prototype &tag_encode_options,
						FUI::ThreadContext *thread_context
					);
				};

				extern FLAC_to_FLAC FLAC_to_FLAC_;

			}; // namespace Internal
		}; // namespace Transcoder
	}; // namespace Codec
}; // namespace FUI

#endif
