/* libFUI - FLAC User Interface library
 * Copyright (C) 2004  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef FUI__private_FILE_h
#define FUI__private_FILE_h

#include <stdio.h>
#include <string>

namespace FUI {

	class FILE {
	private:
		::FILE *f;
	public:
		inline FILE(const char *path, const char *mode): f(fopen(path, mode)) { }
		inline FILE(const std::string &path, const char *mode): f(fopen(path.c_str(), mode)) { }
		inline ~FILE() { if(0 != f) fclose(f); }
		inline bool is_open() const { return 0 != f; }
		inline operator const ::FILE *() const { return f; }
		inline operator ::FILE *() { return f; }
		inline void close() { if(is_open()) fclose(f); }
		inline void open(const char *path, const char *mode) { close(); f = fopen(path, mode); }
		inline void open(const std::string &path, const char *mode) { close(); f = fopen(path.c_str(), mode); }
	};

} // namespace FUI

#endif
