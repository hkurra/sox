/* libFUI - FLAC User Interface library
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "private/Codec/Transcoder/External/FLAC_to_Ogg_FLAC.h"
#include<stdio.h> //@@@for stderr

namespace FUI {
	namespace Codec {
		namespace Transcoder {
			namespace External {

				std::string FLAC_to_Ogg_FLAC::transduce(
					std::string input_path,
					const Codec::Options::Decode::Prototype &codec_decode_options,
					const Tag::Options::Decode::Prototype &tag_decode_options,
					std::string output_path,
					const Codec::Options::Encode::Prototype &codec_encode_options,
					const Tag::Options::Encode::Prototype &tag_encode_options,
					FUI::ThreadContext *thread_context
				)
				{
					(void)codec_decode_options, (void)tag_decode_options, (void)codec_encode_options, (void)tag_encode_options, (void)thread_context; //@@@
					fprintf(stderr, "@@@ Transcoder::External::FLAC_to_Ogg_FLAC::transduce(%s, %s)\n", input_path.c_str(), output_path.c_str());
					return "@@@ unimplemented @@@";
				}

				FLAC_to_Ogg_FLAC FLAC_to_Ogg_FLAC_;

			}; // namespace External
		}; // namespace Transcoder
	}; // namespace Codec
}; // namespace FUI
