/* libFUI - FLAC User Interface library
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "private/Codec/Transcoder/Internal/Transcoders.h"
#include "private/Codec/Transcoder/Internal/FLAC_to_FLAC.h"
#include "private/Codec/Transcoder/Internal/FLAC_to_Ogg_FLAC.h"

namespace FUI {
	namespace Codec {
		namespace Transcoder {
			namespace Internal {

				Transcoders::Transcoders()
				{
					// The equivalent `insert()' is a little clumsy, e.g.
					//   insert(
					//     std::pair<Pair, Interface>(
					//         Pair(FLAC, OGG_FLAC),
					//         FLAC_to_Ogg_FLAC_
					//     )
					//   );
					// so we use operator[]():
					(*this)[Pair(FLAC, FLAC)] = &FLAC_to_FLAC_;
					(*this)[Pair(FLAC, OGG_FLAC)] = &FLAC_to_Ogg_FLAC_;
				}

				Transcoders transcoders_;

			}; // namespace Internal
		}; // namespace Transcoder
	}; // namespace Codec
}; // namespace FUI
