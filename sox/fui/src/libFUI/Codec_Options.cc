/* libFUI - FLAC User Interface library
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "FUI/Codec/Options.h"

namespace FUI {
	namespace Codec {
		namespace Options {

			namespace Decode {

				std::string FLAC::verify() const
				{
					return "";
				}

				std::string OggFLAC::verify() const
				{
					return "";
				}

				std::string OggVorbis::verify() const
				{
					return "";
				}

				std::string MP3::verify() const
				{
					return "";
				}

				std::string RiffWave::verify() const
				{
					return "";
				}

				Prototype *clone(const Prototype *obj)
				{
					Prototype *ret;
					const FLAC *flac = dynamic_cast<const FLAC *>(obj);
					const MP3 *mp3 = dynamic_cast<const MP3 *>(obj);
					const OggFLAC *ogg_flac = dynamic_cast<const OggFLAC *>(obj);
					const OggVorbis *ogg_vorbis = dynamic_cast<const OggVorbis *>(obj);
					const RiffWave *wave = dynamic_cast<const RiffWave *>(obj);
					if(0 != flac)
						ret = new FLAC(*flac);
					else if(0 != mp3)
						ret = new MP3(*mp3);
					else if(0 != ogg_flac)
						ret = new OggFLAC(*ogg_flac);
					else if(0 != ogg_vorbis)
						ret = new OggVorbis(*ogg_vorbis);
					else if(0 != wave)
						ret = new RiffWave(*wave);
					else
						assert(0);
					return ret;
				}

			};

			namespace Encode {

				FLAC::FLAC(unsigned level):
				Prototype()
				{
					verify_ = true;
					subset_ = true;

					switch(level) {
						case 0:
							do_mid_side_ = false;
							adaptive_mid_side_ = false;
							blocksize_ = 1152;
							max_lpc_order_ = 0;
							qlp_coeff_precision_ = 0;
							do_qlp_coeff_prec_search_ = false;
							do_escape_coding_ = false;
							do_exhaustive_model_search_ = false;
							min_residual_partition_order_ = 2;
							max_residual_partition_order_ = 2;
							rice_parameter_search_dist_ = 0;
							break;
						case 1:
							do_mid_side_ = true;
							adaptive_mid_side_ = true;
							blocksize_ = 1152;
							max_lpc_order_ = 0;
							qlp_coeff_precision_ = 0;
							do_qlp_coeff_prec_search_ = false;
							do_escape_coding_ = false;
							do_exhaustive_model_search_ = false;
							min_residual_partition_order_ = 2;
							max_residual_partition_order_ = 2;
							rice_parameter_search_dist_ = 0;
							break;
						case 2:
							do_mid_side_ = true;
							adaptive_mid_side_ = false;
							blocksize_ = 1152;
							max_lpc_order_ = 0;
							qlp_coeff_precision_ = 0;
							do_qlp_coeff_prec_search_ = false;
							do_escape_coding_ = false;
							do_exhaustive_model_search_ = false;
							min_residual_partition_order_ = 0;
							max_residual_partition_order_ = 3;
							rice_parameter_search_dist_ = 0;
							break;
						case 3:
							do_mid_side_ = false;
							adaptive_mid_side_ = false;
							blocksize_ = 4608;
							max_lpc_order_ = 6;
							qlp_coeff_precision_ = 0;
							do_qlp_coeff_prec_search_ = false;
							do_escape_coding_ = false;
							do_exhaustive_model_search_ = false;
							min_residual_partition_order_ = 3;
							max_residual_partition_order_ = 3;
							rice_parameter_search_dist_ = 0;
							break;
						case 4:
							do_mid_side_ = true;
							adaptive_mid_side_ = true;
							blocksize_ = 4608;
							max_lpc_order_ = 8;
							qlp_coeff_precision_ = 0;
							do_qlp_coeff_prec_search_ = false;
							do_escape_coding_ = false;
							do_exhaustive_model_search_ = false;
							min_residual_partition_order_ = 3;
							max_residual_partition_order_ = 3;
							rice_parameter_search_dist_ = 0;
							break;
						case 5:
							do_mid_side_ = true;
							adaptive_mid_side_ = false;
							blocksize_ = 4608;
							max_lpc_order_ = 8;
							qlp_coeff_precision_ = 0;
							do_qlp_coeff_prec_search_ = false;
							do_escape_coding_ = false;
							do_exhaustive_model_search_ = false;
							min_residual_partition_order_ = 3;
							max_residual_partition_order_ = 3;
							rice_parameter_search_dist_ = 0;
							break;
						case 6:
							do_mid_side_ = true;
							adaptive_mid_side_ = false;
							blocksize_ = 4608;
							max_lpc_order_ = 8;
							qlp_coeff_precision_ = 0;
							do_qlp_coeff_prec_search_ = false;
							do_escape_coding_ = false;
							do_exhaustive_model_search_ = false;
							min_residual_partition_order_ = 0;
							max_residual_partition_order_ = 4;
							rice_parameter_search_dist_ = 0;
							break;
						case 7:
							do_mid_side_ = true;
							adaptive_mid_side_ = false;
							blocksize_ = 4608;
							max_lpc_order_ = 8;
							qlp_coeff_precision_ = 0;
							do_qlp_coeff_prec_search_ = false;
							do_escape_coding_ = false;
							do_exhaustive_model_search_ = true;
							min_residual_partition_order_ = 0;
							max_residual_partition_order_ = 6;
							rice_parameter_search_dist_ = 0;
							break;
						default:
							do_mid_side_ = true;
							adaptive_mid_side_ = false;
							blocksize_ = 4608;
							max_lpc_order_ = 12;
							qlp_coeff_precision_ = 0;
							do_qlp_coeff_prec_search_ = false;
							do_escape_coding_ = false;
							do_exhaustive_model_search_ = true;
							min_residual_partition_order_ = 0;
							max_residual_partition_order_ = 6;
							rice_parameter_search_dist_ = 0;
							break;
					}

					assert(verify().length() == 0);
				}

				std::string FLAC::verify() const
				{
#if 0
					//@@@ do something with these?
					bool do_mid_side_;
					bool adaptive_mid_side_; // requires that do_mid_side is true also
					unsigned blocksize_;
					unsigned max_lpc_order_;
					unsigned qlp_coeff_precision_;
					bool do_qlp_coeff_prec_search_;
					bool do_escape_coding_; // deprecated in libFLAC
					bool do_exhaustive_model_search_;
					unsigned min_residual_partition_order_;
					unsigned max_residual_partition_order_;
					unsigned rice_parameter_search_dist_; // deprecated in libFLAC
#endif
					return "";
				}

				OggFLAC::OggFLAC(unsigned level):
				Prototype()
				{
					verify_ = true;
					subset_ = true;

					switch(level) {
						case 0:
							do_mid_side_ = false;
							adaptive_mid_side_ = false;
							blocksize_ = 1152;
							max_lpc_order_ = 0;
							qlp_coeff_precision_ = 0;
							do_qlp_coeff_prec_search_ = false;
							do_escape_coding_ = false;
							do_exhaustive_model_search_ = false;
							min_residual_partition_order_ = 2;
							max_residual_partition_order_ = 2;
							rice_parameter_search_dist_ = 0;
							break;
						case 1:
							do_mid_side_ = true;
							adaptive_mid_side_ = true;
							blocksize_ = 1152;
							max_lpc_order_ = 0;
							qlp_coeff_precision_ = 0;
							do_qlp_coeff_prec_search_ = false;
							do_escape_coding_ = false;
							do_exhaustive_model_search_ = false;
							min_residual_partition_order_ = 2;
							max_residual_partition_order_ = 2;
							rice_parameter_search_dist_ = 0;
							break;
						case 2:
							do_mid_side_ = true;
							adaptive_mid_side_ = false;
							blocksize_ = 1152;
							max_lpc_order_ = 0;
							qlp_coeff_precision_ = 0;
							do_qlp_coeff_prec_search_ = false;
							do_escape_coding_ = false;
							do_exhaustive_model_search_ = false;
							min_residual_partition_order_ = 0;
							max_residual_partition_order_ = 3;
							rice_parameter_search_dist_ = 0;
							break;
						case 3:
							do_mid_side_ = false;
							adaptive_mid_side_ = false;
							blocksize_ = 4608;
							max_lpc_order_ = 6;
							qlp_coeff_precision_ = 0;
							do_qlp_coeff_prec_search_ = false;
							do_escape_coding_ = false;
							do_exhaustive_model_search_ = false;
							min_residual_partition_order_ = 3;
							max_residual_partition_order_ = 3;
							rice_parameter_search_dist_ = 0;
							break;
						case 4:
							do_mid_side_ = true;
							adaptive_mid_side_ = true;
							blocksize_ = 4608;
							max_lpc_order_ = 8;
							qlp_coeff_precision_ = 0;
							do_qlp_coeff_prec_search_ = false;
							do_escape_coding_ = false;
							do_exhaustive_model_search_ = false;
							min_residual_partition_order_ = 3;
							max_residual_partition_order_ = 3;
							rice_parameter_search_dist_ = 0;
							break;
						case 5:
							do_mid_side_ = true;
							adaptive_mid_side_ = false;
							blocksize_ = 4608;
							max_lpc_order_ = 8;
							qlp_coeff_precision_ = 0;
							do_qlp_coeff_prec_search_ = false;
							do_escape_coding_ = false;
							do_exhaustive_model_search_ = false;
							min_residual_partition_order_ = 3;
							max_residual_partition_order_ = 3;
							rice_parameter_search_dist_ = 0;
							break;
						case 6:
							do_mid_side_ = true;
							adaptive_mid_side_ = false;
							blocksize_ = 4608;
							max_lpc_order_ = 8;
							qlp_coeff_precision_ = 0;
							do_qlp_coeff_prec_search_ = false;
							do_escape_coding_ = false;
							do_exhaustive_model_search_ = false;
							min_residual_partition_order_ = 0;
							max_residual_partition_order_ = 4;
							rice_parameter_search_dist_ = 0;
							break;
						case 7:
							do_mid_side_ = true;
							adaptive_mid_side_ = false;
							blocksize_ = 4608;
							max_lpc_order_ = 8;
							qlp_coeff_precision_ = 0;
							do_qlp_coeff_prec_search_ = false;
							do_escape_coding_ = false;
							do_exhaustive_model_search_ = true;
							min_residual_partition_order_ = 0;
							max_residual_partition_order_ = 6;
							rice_parameter_search_dist_ = 0;
							break;
						default:
							do_mid_side_ = true;
							adaptive_mid_side_ = false;
							blocksize_ = 4608;
							max_lpc_order_ = 12;
							qlp_coeff_precision_ = 0;
							do_qlp_coeff_prec_search_ = false;
							do_escape_coding_ = false;
							do_exhaustive_model_search_ = true;
							min_residual_partition_order_ = 0;
							max_residual_partition_order_ = 6;
							rice_parameter_search_dist_ = 0;
							break;
					}

					serial_number_ = 0;

					assert(verify().length() == 0);
				}

				std::string OggFLAC::verify() const
				{
					return "";
				}

				std::string OggVorbis::verify() const
				{
#if 0
					//@@@ do something with these?
					double quality_;
#endif
					return "";
				}

				std::string MP3::verify() const
				{
#if 0
					//@@@ do something with these?
					double quality_;
					unsigned target_bps_;
#endif
					return "";
				}

				std::string RiffWave::verify() const
				{
					return "";
				}

				Prototype *clone(const Prototype *obj)
				{
					Prototype *ret;
					const FLAC *flac = dynamic_cast<const FLAC *>(obj);
					const MP3 *mp3 = dynamic_cast<const MP3 *>(obj);
					const OggFLAC *ogg_flac = dynamic_cast<const OggFLAC *>(obj);
					const OggVorbis *ogg_vorbis = dynamic_cast<const OggVorbis *>(obj);
					const RiffWave *wave = dynamic_cast<const RiffWave *>(obj);
					if(0 != flac)
						ret = new FLAC(*flac);
					else if(0 != mp3)
						ret = new MP3(*mp3);
					else if(0 != ogg_flac)
						ret = new OggFLAC(*ogg_flac);
					else if(0 != ogg_vorbis)
						ret = new OggVorbis(*ogg_vorbis);
					else if(0 != wave)
						ret = new RiffWave(*wave);
					else
						assert(0);
					return ret;
				}

			};

		}; // namespace Options
	}; // namepace Codec
}; // namepace FUI
