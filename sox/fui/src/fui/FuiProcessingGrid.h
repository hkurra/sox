/* fui - FLAC User Interface
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef FUI__FuiProcessingGrid_h
#define FUI__FuiProcessingGrid_h

#include "wx/grid.h"
#include "wx/thread.h" // for wxCriticalSection
#include "FUI/Transducer/FIFO.h"

class FuiProcessingGridTable;

class FuiProcessingGrid : public wxGrid {
public:
	class FIFO_Locker : public wxCriticalSectionLocker {
	public:
		inline FIFO_Locker(FuiProcessingGrid &grid);
	};

	FuiProcessingGrid(wxFrame *parent);

	void dimensions_changed(int rows_delta, int pos = -1); //@@@ make into message?

	inline const FUI::Transducer::FIFO &fifo() const;
	inline FUI::Transducer::FIFO &fifo();
protected:
	FUI::Transducer::FIFO fifo_;
	FuiProcessingGridTable *table_;
	wxCriticalSection fifo_critical_section_;
};

inline const FUI::Transducer::FIFO &FuiProcessingGrid::fifo() const
{ return fifo_; }

inline FUI::Transducer::FIFO &FuiProcessingGrid::fifo()
{ return fifo_; }

inline FuiProcessingGrid::FIFO_Locker::FIFO_Locker(FuiProcessingGrid &grid):
wxCriticalSectionLocker(grid.fifo_critical_section_)
{ }

#endif
