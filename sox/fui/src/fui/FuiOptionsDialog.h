/* fui - FLAC User Interface
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef FUI__FuiOptionsDialog_h
#define FUI__FuiOptionsDialog_h

class wxPanel;
class wxSizer;
namespace FUI {
	namespace Configuration {
		class Instance;
	};
};
#include "wx/dialog.h"

class FuiOptionsDialog : public wxDialog {
public:
	FuiOptionsDialog(FUI::Configuration::Instance *cfg, wxWindow *parent, const wxPoint& pos = wxDefaultPosition);
	~FuiOptionsDialog();

	bool Validate();

	void OnOk(wxCommandEvent &event);
	void OnCancel(wxCommandEvent &event);
	void OnPanelFLAC(wxCommandEvent &event);
	void OnPanelOggFLAC(wxCommandEvent &event);

private:
	DECLARE_EVENT_TABLE()

	FUI::Configuration::Instance *cfg_;

	class Controls;

	Controls *controls_;

	void build_global_controls(wxPanel *panel, wxSizer *sizer);

	void build_flac_controls(wxPanel *panel, wxSizer *sizer);
	void build_flac_encoding_controls(wxPanel *panel, wxSizer *sizer);
	void build_flac_decoding_controls(wxPanel *panel, wxSizer *sizer);
	void build_flac_tagging_controls(wxPanel *panel, wxSizer *sizer);

	void build_ogg_flac_controls(wxPanel *panel, wxSizer *sizer);
	void build_ogg_flac_encoding_controls(wxPanel *panel, wxSizer *sizer);
	void build_ogg_flac_decoding_controls(wxPanel *panel, wxSizer *sizer);
	void build_ogg_flac_tagging_controls(wxPanel *panel, wxSizer *sizer);
};

#endif
