/* fui - FLAC User Interface
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "Configuration.h"
#include "FuiFrame.h"
#include "wx/app.h"

class FuiApp : public wxApp
{
public:
	virtual bool OnInit();
	virtual int OnExit();
private:
	FUI::Configuration::Instance *config_instance_;
};

IMPLEMENT_APP(FuiApp)

bool FuiApp::OnInit()
{
	SetVendorName("FLAC");
	SetAppName("fui");

	config_instance_ = new FUI::Configuration::Instance();

	FuiFrame *frame = new FuiFrame(config_instance_, _T("fui"), wxPoint(10, 10), wxSize(800, 600));

	frame->Show(TRUE);

	SetTopWindow(frame);

	return TRUE;
}

int FuiApp::OnExit()
{
	delete config_instance_;
	return 0;
}
