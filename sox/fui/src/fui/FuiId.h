/* fui - FLAC User Interface
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef FUI__FuiId_h
#define FUI__FuiId_h

#include "wx/defs.h"

enum {
	// use wxID_ABOUT so that is shows up in the Apple menu on Macs
	FuiFrame_MenuId__About = wxID_ABOUT,

	FuiFrame_MenuId__Exit = wxID_HIGHEST + 1,
	FuiFrame_MenuId__AddFiles,
	FuiFrame_MenuId__Start,
	FuiFrame_MenuId__Pause,
	FuiFrame_MenuId__Stop,
	FuiFrame_MenuId__Options,
	FuiFrame_CommandId__ProcessingThreadEvent_ElementProgress,
	FuiFrame_CommandId__ProcessingThreadEvent_Exit
};

#endif
