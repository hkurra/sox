/* fui - FLAC User Interface
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef FUI__FuiProcessingThread_h
#define FUI__FuiProcessingThread_h

#include "FUI/ThreadContext.h"
#include "wx/thread.h"

namespace FUI {
	namespace Transducer {
		class Element;
	};
};
class FuiFrame;
class FuiProcessingGrid;

class FuiProcessingThread : public wxThread, public FUI::ThreadContext
{
public:
	FuiProcessingThread(FuiFrame *frame, FuiProcessingGrid *processing_grid);

	// from wxThread
	virtual void *Entry();
	virtual void OnExit();

	// from FUI::ThreadContext
	inline bool check_for_abort() { return TestDestroy(); }
	void progress_report(unsigned percent_complete);
protected:
	FUI::Transducer::Element *get_first_pending_element();

	FuiFrame *frame_;
	FuiProcessingGrid *processing_grid_;
	FUI::Transducer::Element *working_element_;
};

#endif
