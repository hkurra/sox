/* fui - FLAC User Interface
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef FUI__Configuration_h
#define FUI__Configuration_h

class wxConfigBase;
#include "FUI/Codec/Options.h"
#include "FUI/Codec/Type.h"
#include "FUI/Tag/Options.h"
#include "FUI/Tag/Type.h"

namespace FUI {
	namespace Configuration {

		class Instance {
		public:
			Instance();
			~Instance();

			void load_from_persistent_db();
			void store_to_persistent_db();

		protected:
			// These are options for the various Effort aliases for FLAC encoding:
			FUI::Codec::Options::Encode::FLAC encode_options_flac_low_;
			FUI::Codec::Options::Encode::FLAC encode_options_flac_medium_;
			FUI::Codec::Options::Encode::FLAC encode_options_flac_high_;
			FUI::Codec::Options::Encode::OggFLAC encode_options_ogg_flac_low_;
			FUI::Codec::Options::Encode::OggFLAC encode_options_ogg_flac_medium_;
			FUI::Codec::Options::Encode::OggFLAC encode_options_ogg_flac_high_;

			// These are pointers into options_
			FUI::Codec::Options::Decode::Prototype *codec_decode_options_[FUI::Codec::TYPE_MAX];
			FUI::Codec::Options::Encode::Prototype *codec_encode_options_[FUI::Codec::TYPE_MAX];
			FUI::Tag::Options::Decode::Prototype *tag_decode_options_[FUI::Tag::TYPE_MAX];
			FUI::Tag::Options::Encode::Prototype *tag_encode_options_[FUI::Tag::TYPE_MAX];

			//@@@@ need FLAC seektable options everywhere
		public:
			struct Options {
				struct Global {
					std::string output_directory;
				} global;
				struct Codec {
					struct Decode {
						FUI::Codec::Options::Decode::FLAC flac;
						FUI::Codec::Options::Decode::MP3 mp3;
						FUI::Codec::Options::Decode::OggFLAC ogg_flac;
						FUI::Codec::Options::Decode::OggVorbis ogg_vorbis;
						FUI::Codec::Options::Decode::RiffWave wave;
					} decode;
					struct Encode {
						struct FLAC {
							enum Effort { LOW, MEDIUM, HIGH, CUSTOM } effort;
							FUI::Codec::Options::Encode::FLAC custom;
						} flac;
						FUI::Codec::Options::Encode::MP3 mp3;
						struct OggFLAC {
							enum Effort { LOW, MEDIUM, HIGH, CUSTOM } effort;
							FUI::Codec::Options::Encode::OggFLAC custom;
						} ogg_flac;
						FUI::Codec::Options::Encode::OggVorbis ogg_vorbis;
						FUI::Codec::Options::Encode::RiffWave wave;
					} encode;
				} codec;
				struct Tag {
					struct Decode {
						FUI::Tag::Options::Decode::ID3 id3;
						FUI::Tag::Options::Decode::Null null;
						FUI::Tag::Options::Decode::VorbisComment vorbis_comment;
					} decode;
					struct Encode {
						FUI::Tag::Options::Encode::ID3 id3;
						FUI::Tag::Options::Encode::Null null;
						FUI::Tag::Options::Encode::VorbisComment vorbis_comment;
					} encode;
				} tag;
			} options_;

			const FUI::Codec::Options::Decode::Prototype *codec_decode_options(FUI::Codec::Type type) const;
			FUI::Codec::Options::Decode::Prototype *codec_decode_options(FUI::Codec::Type type);
			const FUI::Codec::Options::Encode::Prototype *codec_encode_options(FUI::Codec::Type type) const;
			FUI::Codec::Options::Encode::Prototype *codec_encode_options(FUI::Codec::Type type);

			const FUI::Tag::Options::Decode::Prototype *tag_decode_options(FUI::Tag::Type type) const;
			FUI::Tag::Options::Decode::Prototype *tag_decode_options(FUI::Tag::Type type);
			const FUI::Tag::Options::Encode::Prototype *tag_encode_options(FUI::Tag::Type type) const;
			FUI::Tag::Options::Encode::Prototype *tag_encode_options(FUI::Tag::Type type);

			const FUI::Tag::Options::Decode::Prototype *tag_decode_options(FUI::Codec::Type type) const;
			FUI::Tag::Options::Decode::Prototype *tag_decode_options(FUI::Codec::Type type);
			const FUI::Tag::Options::Encode::Prototype *tag_encode_options(FUI::Codec::Type type) const;
			FUI::Tag::Options::Encode::Prototype *tag_encode_options(FUI::Codec::Type type);
		private:
			void load_global(wxConfigBase *);
			void load_codec(wxConfigBase *);
			void load_codec_options(wxConfigBase *);
			void load_codec_options_decode(wxConfigBase *);
			void load_codec_options_decode_flac(wxConfigBase *, FUI::Codec::Options::Decode::FLAC &);
			void load_codec_options_decode_mp3(wxConfigBase *, FUI::Codec::Options::Decode::MP3 &);
			void load_codec_options_decode_ogg_flac(wxConfigBase *, FUI::Codec::Options::Decode::OggFLAC &);
			void load_codec_options_decode_ogg_vorbis(wxConfigBase *, FUI::Codec::Options::Decode::OggVorbis &);
			void load_codec_options_decode_wave(wxConfigBase *, FUI::Codec::Options::Decode::RiffWave &);
			void load_codec_options_encode(wxConfigBase *);
			void load_codec_options_encode_flac(wxConfigBase *, Instance::Options::Codec::Encode::FLAC &);
			void load_codec_options_encode_mp3(wxConfigBase *, FUI::Codec::Options::Encode::MP3 &);
			void load_codec_options_encode_ogg_flac(wxConfigBase *, Instance::Options::Codec::Encode::OggFLAC &);
			void load_codec_options_encode_ogg_vorbis(wxConfigBase *, FUI::Codec::Options::Encode::OggVorbis &);
			void load_codec_options_encode_wave(wxConfigBase *, FUI::Codec::Options::Encode::RiffWave &);
			void load_tag(wxConfigBase *);
			void load_tag_options(wxConfigBase *);
			void load_tag_options_decode(wxConfigBase *);
			void load_tag_options_decode_id3(wxConfigBase *, FUI::Tag::Options::Decode::ID3 &);
			void load_tag_options_decode_null(wxConfigBase *, FUI::Tag::Options::Decode::Null &);
			void load_tag_options_decode_vorbis_comment(wxConfigBase *, FUI::Tag::Options::Decode::VorbisComment &);
			void load_tag_options_encode(wxConfigBase *);
			void load_tag_options_encode_id3(wxConfigBase *, FUI::Tag::Options::Encode::ID3 &);
			void load_tag_options_encode_null(wxConfigBase *, FUI::Tag::Options::Encode::Null &);
			void load_tag_options_encode_vorbis_comment(wxConfigBase *, FUI::Tag::Options::Encode::VorbisComment &);

			void store_global(wxConfigBase *);
			void store_codec(wxConfigBase *);
			void store_codec_options(wxConfigBase *);
			void store_codec_options_decode(wxConfigBase *);
			void store_codec_options_decode_flac(wxConfigBase *, const FUI::Codec::Options::Decode::FLAC &);
			void store_codec_options_decode_mp3(wxConfigBase *, const FUI::Codec::Options::Decode::MP3 &);
			void store_codec_options_decode_ogg_flac(wxConfigBase *, const FUI::Codec::Options::Decode::OggFLAC &);
			void store_codec_options_decode_ogg_vorbis(wxConfigBase *, const FUI::Codec::Options::Decode::OggVorbis &);
			void store_codec_options_decode_wave(wxConfigBase *, const FUI::Codec::Options::Decode::RiffWave &);
			void store_codec_options_encode(wxConfigBase *);
			void store_codec_options_encode_flac(wxConfigBase *, const Instance::Options::Codec::Encode::FLAC &);
			void store_codec_options_encode_mp3(wxConfigBase *, const FUI::Codec::Options::Encode::MP3 &);
			void store_codec_options_encode_ogg_flac(wxConfigBase *, const Instance::Options::Codec::Encode::OggFLAC &);
			void store_codec_options_encode_ogg_vorbis(wxConfigBase *, const FUI::Codec::Options::Encode::OggVorbis &);
			void store_codec_options_encode_wave(wxConfigBase *, const FUI::Codec::Options::Encode::RiffWave &);
			void store_tag(wxConfigBase *);
			void store_tag_options(wxConfigBase *);
			void store_tag_options_decode(wxConfigBase *);
			void store_tag_options_decode_id3(wxConfigBase *, const FUI::Tag::Options::Decode::ID3 &);
			void store_tag_options_decode_null(wxConfigBase *, const FUI::Tag::Options::Decode::Null &);
			void store_tag_options_decode_vorbis_comment(wxConfigBase *, const FUI::Tag::Options::Decode::VorbisComment &);
			void store_tag_options_encode(wxConfigBase *);
			void store_tag_options_encode_id3(wxConfigBase *, const FUI::Tag::Options::Encode::ID3 &);
			void store_tag_options_encode_null(wxConfigBase *, const FUI::Tag::Options::Encode::Null &);
			void store_tag_options_encode_vorbis_comment(wxConfigBase *, const FUI::Tag::Options::Encode::VorbisComment &);

			void load_bool(wxConfigBase *cfg, const char *key, bool &value);
			void load_double(wxConfigBase *cfg, const char *key, double &value);
			void load_string(wxConfigBase *cfg, const char *key, std::string &value);
			void load_unsigned(wxConfigBase *cfg, const char *key, unsigned &value);

			void store_bool(wxConfigBase *cfg, const char *key, bool value);
			void store_double(wxConfigBase *cfg, const char *key, double value);
			void store_string(wxConfigBase *cfg, const char *key, const std::string &value);
			void store_unsigned(wxConfigBase *cfg, const char *key, unsigned value);
		};

	};
};

#endif
