/* fui - FLAC User Interface
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "FuiId.h"
#include "FuiProcessingGrid.h"
#include "wx/frame.h"


class FuiProcessingGridCellAttrProvider: public wxGridCellAttrProvider
{
public:
	FuiProcessingGridCellAttrProvider();
	virtual ~FuiProcessingGridCellAttrProvider();

	virtual wxGridCellAttr *GetAttr(int row, int col, wxGridCellAttr::wxAttrKind kind) const;

private:
	wxGridCellAttr *attr_odd_rows_;
};

FuiProcessingGridCellAttrProvider::FuiProcessingGridCellAttrProvider():
wxGridCellAttrProvider(),
attr_odd_rows_(new wxGridCellAttr)
{
	attr_odd_rows_->SetBackgroundColour(*wxLIGHT_GREY);
}

FuiProcessingGridCellAttrProvider::~FuiProcessingGridCellAttrProvider()
{
	attr_odd_rows_->DecRef();
}

wxGridCellAttr *FuiProcessingGridCellAttrProvider::GetAttr(int row, int col, wxGridCellAttr::wxAttrKind kind /* = wxGridCellAttr::Any */) const
{
	wxGridCellAttr *attr = wxGridCellAttrProvider::GetAttr(row, col, kind);

	if(row % 2) {
		if (!attr) {
			attr = attr_odd_rows_;
			attr->IncRef();
		}
		else {
			if(!attr->HasBackgroundColour()) {
				wxGridCellAttr *attrNew = attr->Clone();
				attr->DecRef();
				attr = attrNew;
				attr->SetBackgroundColour(*wxLIGHT_GREY);
			}
		}
	}

	return attr;
}


class FuiProcessingGridTable : public wxGridTableBase
{
private:
	FuiProcessingGrid &parent_;
public:
	FuiProcessingGridTable(FuiProcessingGrid &parent): parent_(parent) { }

	int GetNumberRows()
	{
		// FIFO critical section
		FuiProcessingGrid::FIFO_Locker locker(parent_);
		return parent_.fifo().size();
	}

	int GetNumberCols() { return 3; }

	wxString GetValue(int row, int col)
	{
		// FIFO critical section
		FuiProcessingGrid::FIFO_Locker locker(parent_);

		if(row > (int)parent_.fifo().size())
			return "@@@ ROW ERROR @@@";

		if(col == 0)
			return parent_.fifo().find((unsigned)row)->input_path_.c_str();
		else if(col == 1) {
			if(parent_.fifo().find((unsigned)row)->state() == FUI::Transducer::Element::WORKING) {
				return wxString::Format("%u%%", parent_.fifo().find((unsigned)row)->percent_complete());
			}
			else
				return parent_.fifo().find((unsigned)row)->state_as_string();
		}
		else if(col == 2)
			return parent_.fifo().find((unsigned)row)->output_path_.c_str();
		else
			return "@@@ COLUMN ERROR @@@";
	}

	void SetValue(int, int, const wxString&) { /* ignore */ }
	bool IsEmptyCell(int, int) { return FALSE; }
};


FuiProcessingGrid::FuiProcessingGrid(wxFrame *parent):
wxGrid(parent, -1)
{
	table_ = new FuiProcessingGridTable(*this);
	table_->SetAttrProvider(new FuiProcessingGridCellAttrProvider);
	SetTable(table_, /*takeOwnership=*/true, wxGrid::wxGridSelectRows);

	DisableDragGridSize();
	DisableDragRowSize();
	EnableEditing(false);
	EnableGridLines(false);
	SetRowLabelSize(0);
}

void FuiProcessingGrid::dimensions_changed(int rows_delta, int pos)
{
	assert(rows_delta != 0);

	if(pos == -1) {
		assert(rows_delta > 0);
		pos = GetNumberRows();
	}
	wxGridTableMessage msg(table_, wxGRIDTABLE_NOTIFY_ROWS_INSERTED, pos, rows_delta);
	ProcessTableMessage(msg);
}
