/* fui - FLAC User Interface
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef FUI__FuiFrame_h
#define FUI__FuiFrame_h

class wxToolBarToolBase;
class FuiProcessingGrid;
class FuiProcessingThread;
namespace FUI {
	namespace Configuration {
		class Instance;
	};
};

#include "wx/frame.h"

class FuiFrame : public wxFrame {
public:
	FuiFrame(FUI::Configuration::Instance *cfg, const wxString& title, const wxPoint& pos, const wxSize& size);

	void OnAbout(wxCommandEvent& event);
	void OnAddFiles(wxCommandEvent& event);
	void OnExit(wxCommandEvent& event);
	void OnProcessingThreadEvent_ElementProgress(wxCommandEvent& event);
	void OnProcessingThreadEvent_Exit(wxCommandEvent& event);
	void OnStart(wxCommandEvent& event);
	void OnPause(wxCommandEvent& event);
	void OnStop(wxCommandEvent& event);
	void OnOptions(wxCommandEvent& event);
private:
	DECLARE_EVENT_TABLE()

	FUI::Configuration::Instance *cfg_;

	enum { RUNNING, PAUSED, STOPPED } state_;

	wxBitmap bitmap_start_;
	wxBitmap bitmap_pause_;
	wxBitmap bitmap_stop_;

	wxToolBarToolBase *toolBarTool_start_;
	wxToolBarToolBase *toolBarTool_pause_;
	wxToolBarToolBase *toolBarTool_stop_;

	FuiProcessingGrid *processing_grid_;
	FuiProcessingThread *processing_thread_;
};

#endif
