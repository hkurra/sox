/* fui - FLAC User Interface
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "Configuration.h"
#include "FuiId.h"
#include "FuiOptionsDialog.h"
#include "FLAC/format.h"
#include "wx/button.h"
#include "wx/checkbox.h"
#include "wx/choice.h"
#include "wx/msgdlg.h"
#include "wx/notebook.h"
#include "wx/panel.h"
#include "wx/radiobox.h"
#include "wx/sizer.h"
#include "wx/statbox.h"
#include "wx/stattext.h"
#include "wx/textctrl.h"


class FuiOptionsDialog::Controls {
public:
	wxNotebook *notebook;

	struct {
		wxTextCtrl *output_directory;
	} global;
	struct {
		struct {
			wxChoice *effort;
			wxCheckBox *verify;
			wxCheckBox *do_mid_side;
			wxCheckBox *adaptive_mid_side;
			wxChoice *blocksize;
			wxTextCtrl *max_lpc_order;
			wxCheckBox *do_qlp_coeff_prec_search;
			wxTextCtrl *qlp_coeff_precision;
			wxCheckBox *do_escape_coding;
			wxCheckBox *do_exhaustive_model_search;
			wxTextCtrl *min_residual_partition_order;
			wxTextCtrl *max_residual_partition_order;
		} encoding;
		struct {
			wxCheckBox *verify_md5;
		} decoding;
		struct {
			//@@@@@@ these go bye-bye
			wxCheckBox *id3v1;
			wxCheckBox *id3v2;
			wxCheckBox *vorbiscomments;
		} tagging;
	} flac;
	struct {
		struct {
			wxChoice *effort;
			wxCheckBox *verify;
			wxCheckBox *do_mid_side;
			wxCheckBox *adaptive_mid_side;
			wxChoice *blocksize;
			wxTextCtrl *max_lpc_order;
			wxCheckBox *do_qlp_coeff_prec_search;
			wxTextCtrl *qlp_coeff_precision;
			wxCheckBox *do_escape_coding;
			wxCheckBox *do_exhaustive_model_search;
			wxTextCtrl *min_residual_partition_order;
			wxTextCtrl *max_residual_partition_order;
			wxTextCtrl *serial_number;
		} encoding;
		struct {
			wxCheckBox *verify_md5;
		} decoding;
		struct {
			//@@@@@@ these go bye-bye
			wxCheckBox *vorbiscomments;
		} tagging;
	} ogg_flac;

	// apply the widget values to the configuration instance
	void apply(FUI::Configuration::Instance *cfg);

	// populate the widget values from the configuration instance
	void populate(const FUI::Configuration::Instance *cfg);

	// propagate constraints across widgets
	void propagate();
};


namespace local {

	static bool string_to_unsigned(const wxString &s, unsigned &u)
	{
		// don't trust wxString::ToULong() to return false for negative numbers
		long x;
		if(!s.ToLong(&x))
			return false;
		if(x < 0)
			return false;
		u = (unsigned)x;
		return true;
	}

	static inline bool validate_error(wxNotebook *n, int page, const wxString &s)
	{
		n->SetSelection(page);
		(void) wxMessageBox(s, "fui: Error");
		return false;
	}

	static inline unsigned lookup(unsigned val, unsigned def, const unsigned *vals, unsigned nvals)
	{
		for(unsigned i = 0; i < nvals; i++)
			if(vals[i] == val)
				return i;
			else if(vals[i] > val)
				break;
		return lookup(def, vals[0], vals, nvals);
	}

	static unsigned valid_flac_blocksize[] = { 192, 256, 512, 576, 1024, 1152, 2048, 2304, 4096, 4608, 8192, 16384 };
	static const wxString valid_flac_blocksize_strings[] = { _T("192"), _T("256"), _T("512"), _T("576"), _T("1024"), _T("1152"), _T("2048"), _T("2304"), _T("4096"), _T("4608"), _T("8192"), _T("16384") };
	static const unsigned n_valid_flac_blocksize = sizeof(valid_flac_blocksize) / sizeof(valid_flac_blocksize[0]);
	static inline unsigned lookup_valid_flac_blocksize(unsigned blocksize)
	{
		return lookup(blocksize, 4608, valid_flac_blocksize, n_valid_flac_blocksize);
	}

	static wxBoxSizer *labeled_control(wxPanel *parent, const char *label, wxControl *control)
	{
		wxBoxSizer *box_sizer = new wxBoxSizer(wxHORIZONTAL);
		box_sizer->Add(new wxStaticText(parent, -1, label), 0, (wxALL & ~wxRIGHT), 5);
		box_sizer->Add(control, 0, (wxALL & ~wxLEFT), 5);
		return box_sizer;
	}

};


// We only need IDs for widgets that require constraints to be propagated
enum LOCAL__ID {
   	LOCAL__ID_FLAC_EFFORT = wxID_HIGHEST + 1000,
   	LOCAL__ID_FLAC_DO_MID_SIDE,
   	LOCAL__ID_FLAC_DO_QLP_COEFF_PREC_SEARCH,
   	LOCAL__ID_OGG_FLAC_EFFORT,
   	LOCAL__ID_OGG_FLAC_DO_MID_SIDE,
   	LOCAL__ID_OGG_FLAC_DO_QLP_COEFF_PREC_SEARCH
};

BEGIN_EVENT_TABLE(FuiOptionsDialog, wxDialog)
	EVT_BUTTON(wxID_OK, FuiOptionsDialog::OnOk)
	EVT_BUTTON(wxID_CANCEL, FuiOptionsDialog::OnCancel)
	EVT_CHOICE(LOCAL__ID_FLAC_EFFORT, FuiOptionsDialog::OnPanelFLAC)
	EVT_CHECKBOX(LOCAL__ID_FLAC_DO_MID_SIDE, FuiOptionsDialog::OnPanelFLAC)
	EVT_CHECKBOX(LOCAL__ID_FLAC_DO_QLP_COEFF_PREC_SEARCH, FuiOptionsDialog::OnPanelFLAC)
	EVT_CHOICE(LOCAL__ID_OGG_FLAC_EFFORT, FuiOptionsDialog::OnPanelOggFLAC)
	EVT_CHECKBOX(LOCAL__ID_OGG_FLAC_DO_MID_SIDE, FuiOptionsDialog::OnPanelOggFLAC)
	EVT_CHECKBOX(LOCAL__ID_OGG_FLAC_DO_QLP_COEFF_PREC_SEARCH, FuiOptionsDialog::OnPanelOggFLAC)
END_EVENT_TABLE()


void FuiOptionsDialog::Controls::apply(FUI::Configuration::Instance *cfg)
{
	fprintf(stderr, "@@@ FuiOptionsDialog::Controls::apply()\n");
	//
	// global
	//
	cfg->options_.global.output_directory = global.output_directory->GetValue().c_str();

	//
	// FLAC
	//
	{
		int effort = flac.encoding.effort->GetSelection();
		assert(effort >= 0 && effort <= FUI::Configuration::Instance::Options::Codec::Encode::FLAC::CUSTOM);
		cfg->options_.codec.encode.flac.effort = (FUI::Configuration::Instance::Options::Codec::Encode::FLAC::Effort)effort;
	}
	cfg->options_.codec.encode.flac.custom.verify_ = flac.encoding.verify->GetValue();
	cfg->options_.codec.encode.flac.custom.do_mid_side_ = flac.encoding.do_mid_side->GetValue();
	cfg->options_.codec.encode.flac.custom.adaptive_mid_side_ = flac.encoding.adaptive_mid_side->GetValue();
	cfg->options_.codec.encode.flac.custom.blocksize_ = local::valid_flac_blocksize[(unsigned)flac.encoding.blocksize->GetSelection()];
	(void) local::string_to_unsigned(flac.encoding.max_lpc_order->GetValue(), cfg->options_.codec.encode.flac.custom.max_lpc_order_);
	cfg->options_.codec.encode.flac.custom.do_qlp_coeff_prec_search_ = flac.encoding.do_qlp_coeff_prec_search->GetValue();
	(void) local::string_to_unsigned(flac.encoding.qlp_coeff_precision->GetValue(), cfg->options_.codec.encode.flac.custom.qlp_coeff_precision_);
	cfg->options_.codec.encode.flac.custom.do_escape_coding_ = flac.encoding.do_escape_coding->GetValue();
	cfg->options_.codec.encode.flac.custom.do_exhaustive_model_search_ = flac.encoding.do_exhaustive_model_search->GetValue();
	(void) local::string_to_unsigned(flac.encoding.min_residual_partition_order->GetValue(), cfg->options_.codec.encode.flac.custom.min_residual_partition_order_);
	(void) local::string_to_unsigned(flac.encoding.max_residual_partition_order->GetValue(), cfg->options_.codec.encode.flac.custom.max_residual_partition_order_);
	cfg->options_.codec.decode.flac.verify_md5_ = flac.decoding.verify_md5->GetValue();
	cfg->store_to_persistent_db();

	//
	// Ogg FLAC
	//
	{
		int effort = ogg_flac.encoding.effort->GetSelection();
		assert(effort >= 0 && effort <= FUI::Configuration::Instance::Options::Codec::Encode::OggFLAC::CUSTOM);
		cfg->options_.codec.encode.ogg_flac.effort = (FUI::Configuration::Instance::Options::Codec::Encode::OggFLAC::Effort)effort;
	}
	cfg->options_.codec.encode.ogg_flac.custom.verify_ = ogg_flac.encoding.verify->GetValue();
	cfg->options_.codec.encode.ogg_flac.custom.do_mid_side_ = ogg_flac.encoding.do_mid_side->GetValue();
	cfg->options_.codec.encode.ogg_flac.custom.adaptive_mid_side_ = ogg_flac.encoding.adaptive_mid_side->GetValue();
	cfg->options_.codec.encode.ogg_flac.custom.blocksize_ = local::valid_flac_blocksize[(unsigned)ogg_flac.encoding.blocksize->GetSelection()];
	(void) local::string_to_unsigned(ogg_flac.encoding.max_lpc_order->GetValue(), cfg->options_.codec.encode.ogg_flac.custom.max_lpc_order_);
	cfg->options_.codec.encode.ogg_flac.custom.do_qlp_coeff_prec_search_ = ogg_flac.encoding.do_qlp_coeff_prec_search->GetValue();
	(void) local::string_to_unsigned(ogg_flac.encoding.qlp_coeff_precision->GetValue(), cfg->options_.codec.encode.ogg_flac.custom.qlp_coeff_precision_);
	cfg->options_.codec.encode.ogg_flac.custom.do_escape_coding_ = ogg_flac.encoding.do_escape_coding->GetValue();
	cfg->options_.codec.encode.ogg_flac.custom.do_exhaustive_model_search_ = ogg_flac.encoding.do_exhaustive_model_search->GetValue();
	(void) local::string_to_unsigned(ogg_flac.encoding.min_residual_partition_order->GetValue(), cfg->options_.codec.encode.ogg_flac.custom.min_residual_partition_order_);
	(void) local::string_to_unsigned(ogg_flac.encoding.max_residual_partition_order->GetValue(), cfg->options_.codec.encode.ogg_flac.custom.max_residual_partition_order_);
	(void) local::string_to_unsigned(ogg_flac.encoding.serial_number->GetValue(), cfg->options_.codec.encode.ogg_flac.custom.serial_number_);
	cfg->options_.codec.decode.ogg_flac.verify_md5_ = ogg_flac.decoding.verify_md5->GetValue();
	cfg->store_to_persistent_db();
}

void FuiOptionsDialog::Controls::populate(const FUI::Configuration::Instance *cfg)
{
	fprintf(stderr, "@@@ FuiOptionsDialog::Controls::populate()\n");
	//
	// global
	//
	global.output_directory->Clear();
	*global.output_directory << cfg->options_.global.output_directory.c_str();

	//
	// FLAC
	//
	flac.encoding.effort->SetSelection((int)cfg->options_.codec.encode.flac.effort);
	flac.encoding.verify->SetValue(cfg->options_.codec.encode.flac.custom.verify_);
	flac.encoding.do_mid_side->SetValue(cfg->options_.codec.encode.flac.custom.do_mid_side_);
	flac.encoding.adaptive_mid_side->SetValue(cfg->options_.codec.encode.flac.custom.adaptive_mid_side_);
	flac.encoding.blocksize->SetSelection((int)local::lookup_valid_flac_blocksize(cfg->options_.codec.encode.flac.custom.blocksize_));
	flac.encoding.max_lpc_order->Clear();
	*flac.encoding.max_lpc_order << (int)cfg->options_.codec.encode.flac.custom.max_lpc_order_;
	flac.encoding.do_qlp_coeff_prec_search->SetValue(cfg->options_.codec.encode.flac.custom.do_qlp_coeff_prec_search_);
	flac.encoding.qlp_coeff_precision->Clear();
	*flac.encoding.qlp_coeff_precision << (int)cfg->options_.codec.encode.flac.custom.qlp_coeff_precision_;
	flac.encoding.do_escape_coding->SetValue(cfg->options_.codec.encode.flac.custom.do_escape_coding_);
	flac.encoding.do_exhaustive_model_search->SetValue(cfg->options_.codec.encode.flac.custom.do_exhaustive_model_search_);
	flac.encoding.min_residual_partition_order->Clear();
	*flac.encoding.min_residual_partition_order << (int)cfg->options_.codec.encode.flac.custom.min_residual_partition_order_;
	flac.encoding.max_residual_partition_order->Clear();
	*flac.encoding.max_residual_partition_order << (int)cfg->options_.codec.encode.flac.custom.max_residual_partition_order_;
	flac.decoding.verify_md5->SetValue(cfg->options_.codec.decode.flac.verify_md5_);

	//
	// Ogg FLAC
	//
	ogg_flac.encoding.effort->SetSelection((int)cfg->options_.codec.encode.ogg_flac.effort);
	ogg_flac.encoding.verify->SetValue(cfg->options_.codec.encode.ogg_flac.custom.verify_);
	ogg_flac.encoding.do_mid_side->SetValue(cfg->options_.codec.encode.ogg_flac.custom.do_mid_side_);
	ogg_flac.encoding.adaptive_mid_side->SetValue(cfg->options_.codec.encode.ogg_flac.custom.adaptive_mid_side_);
	ogg_flac.encoding.blocksize->SetSelection((int)local::lookup_valid_flac_blocksize(cfg->options_.codec.encode.ogg_flac.custom.blocksize_));
	ogg_flac.encoding.max_lpc_order->Clear();
	*ogg_flac.encoding.max_lpc_order << (int)cfg->options_.codec.encode.ogg_flac.custom.max_lpc_order_;
	ogg_flac.encoding.do_qlp_coeff_prec_search->SetValue(cfg->options_.codec.encode.ogg_flac.custom.do_qlp_coeff_prec_search_);
	ogg_flac.encoding.qlp_coeff_precision->Clear();
	*ogg_flac.encoding.qlp_coeff_precision << (int)cfg->options_.codec.encode.ogg_flac.custom.qlp_coeff_precision_;
	ogg_flac.encoding.do_escape_coding->SetValue(cfg->options_.codec.encode.ogg_flac.custom.do_escape_coding_);
	ogg_flac.encoding.do_exhaustive_model_search->SetValue(cfg->options_.codec.encode.ogg_flac.custom.do_exhaustive_model_search_);
	ogg_flac.encoding.min_residual_partition_order->Clear();
	*ogg_flac.encoding.min_residual_partition_order << (int)cfg->options_.codec.encode.ogg_flac.custom.min_residual_partition_order_;
	ogg_flac.encoding.max_residual_partition_order->Clear();
	*ogg_flac.encoding.max_residual_partition_order << (int)cfg->options_.codec.encode.ogg_flac.custom.max_residual_partition_order_;
	ogg_flac.encoding.serial_number->Clear();
	*ogg_flac.encoding.serial_number << (int)cfg->options_.codec.encode.ogg_flac.custom.serial_number_; //@@@@ 31-bit limit, how to fix?  setting the control value doesn't work (it doesn't get displayed, even after calling the control's Refresk method()
	ogg_flac.decoding.verify_md5->SetValue(cfg->options_.codec.decode.ogg_flac.verify_md5_);

	propagate();
}

void FuiOptionsDialog::Controls::propagate()
{
	fprintf(stderr, "@@@ FuiOptionsDialog::Controls::propagate()\n");

	//
	// FLAC
	//
	{
		int effort = flac.encoding.effort->GetSelection();
		assert(effort >= 0 && effort <= FUI::Configuration::Instance::Options::Codec::Encode::FLAC::CUSTOM);
		bool custom = (effort == FUI::Configuration::Instance::Options::Codec::Encode::FLAC::CUSTOM);

		bool do_mid_side = flac.encoding.do_mid_side->GetValue();
		bool do_qlp_coeff_prec_search = flac.encoding.do_qlp_coeff_prec_search->GetValue();

		flac.encoding.verify->Enable(custom);
		flac.encoding.do_mid_side->Enable(custom);
		flac.encoding.adaptive_mid_side->Enable(custom && do_mid_side);
		flac.encoding.blocksize->Enable(custom);
		flac.encoding.max_lpc_order->Enable(custom);
		flac.encoding.do_qlp_coeff_prec_search->Enable(custom);
		flac.encoding.qlp_coeff_precision->Enable(custom && !do_qlp_coeff_prec_search);
		flac.encoding.do_escape_coding->Enable(custom);
		flac.encoding.do_exhaustive_model_search->Enable(custom);
		flac.encoding.min_residual_partition_order->Enable(custom);
		flac.encoding.max_residual_partition_order->Enable(custom);

		if(!do_mid_side)
			flac.encoding.adaptive_mid_side->SetValue(false);

		if(do_qlp_coeff_prec_search) {
			flac.encoding.qlp_coeff_precision->Clear();
			*flac.encoding.qlp_coeff_precision << 0;
		}
	}

	//
	// Ogg FLAC
	//
	{
		int effort = ogg_flac.encoding.effort->GetSelection();
		assert(effort >= 0 && effort <= FUI::Configuration::Instance::Options::Codec::Encode::OggFLAC::CUSTOM);
		bool custom = (effort == FUI::Configuration::Instance::Options::Codec::Encode::OggFLAC::CUSTOM);

		bool do_mid_side = ogg_flac.encoding.do_mid_side->GetValue();
		bool do_qlp_coeff_prec_search = ogg_flac.encoding.do_qlp_coeff_prec_search->GetValue();

		ogg_flac.encoding.verify->Enable(custom);
		ogg_flac.encoding.do_mid_side->Enable(custom);
		ogg_flac.encoding.adaptive_mid_side->Enable(custom && do_mid_side);
		ogg_flac.encoding.blocksize->Enable(custom);
		ogg_flac.encoding.max_lpc_order->Enable(custom);
		ogg_flac.encoding.do_qlp_coeff_prec_search->Enable(custom);
		ogg_flac.encoding.qlp_coeff_precision->Enable(custom && !do_qlp_coeff_prec_search);
		ogg_flac.encoding.do_escape_coding->Enable(custom);
		ogg_flac.encoding.do_exhaustive_model_search->Enable(custom);
		ogg_flac.encoding.min_residual_partition_order->Enable(custom);
		ogg_flac.encoding.max_residual_partition_order->Enable(custom);

		if(!do_mid_side)
			ogg_flac.encoding.adaptive_mid_side->SetValue(false);

		if(do_qlp_coeff_prec_search) {
			ogg_flac.encoding.qlp_coeff_precision->Clear();
			*ogg_flac.encoding.qlp_coeff_precision << 0;
		}
	}
}

FuiOptionsDialog::FuiOptionsDialog(FUI::Configuration::Instance *cfg, wxWindow *parent, const wxPoint& pos):
wxDialog(parent, -1, "fui options", pos, wxDefaultSize, wxDEFAULT_DIALOG_STYLE | wxRESIZE_BORDER, "FuiOptionsDialog"),
cfg_(cfg),
controls_(new Controls)
{
	assert(0 != controls_);

	wxBoxSizer *sizer_root = new wxBoxSizer(wxVERTICAL);

	controls_->notebook = new wxNotebook(this, -1);
	wxNotebookSizer *notebook_sizer = new wxNotebookSizer(controls_->notebook);
	sizer_root->Add(notebook_sizer, 1, wxGROW);
#if 1
    sizer_root->Add(CreateButtonSizer(wxOK|wxCANCEL), 0, wxCENTRE | wxALL, 10);
#else
	//@@@ use this method if we need to add custom buttons like 'save as defaults' 'restore from defaults'
	wxButton *button = new wxButton(this, wxID_OK, "OK");
	sizer_root->Add(button, 0, wxALIGN_RIGHT | wxALL, 10);
#endif

	wxPanel *panel;

	panel = new wxPanel(controls_->notebook);
	controls_->notebook->AddPage(panel, "Global");
	wxBoxSizer *sizer_global = new wxBoxSizer(wxVERTICAL);
	panel->SetAutoLayout(TRUE);
	panel->SetSizer(sizer_global);
	build_global_controls(panel, sizer_global);


	panel = new wxPanel(controls_->notebook);
	controls_->notebook->AddPage(panel, "FLAC");
	wxBoxSizer *sizer_flac = new wxBoxSizer(wxHORIZONTAL);
	panel->SetAutoLayout(TRUE);
	panel->SetSizer(sizer_flac);
	build_flac_controls(panel, sizer_flac);


	panel = new wxPanel(controls_->notebook);
	controls_->notebook->AddPage(panel, "MP3");
	wxBoxSizer *sizer_mp3 = new wxBoxSizer(wxHORIZONTAL);
	panel->SetAutoLayout(TRUE);
	panel->SetSizer(sizer_mp3);


	panel = new wxPanel(controls_->notebook);
	controls_->notebook->AddPage(panel, "Ogg FLAC");
	wxBoxSizer *sizer_ogg_flac = new wxBoxSizer(wxHORIZONTAL);
	panel->SetAutoLayout(TRUE);
	panel->SetSizer(sizer_ogg_flac);
	build_ogg_flac_controls(panel, sizer_ogg_flac);


	panel = new wxPanel(controls_->notebook);
	controls_->notebook->AddPage(panel, "Ogg Vorbis");
	wxBoxSizer *sizer_ogg_vorbis = new wxBoxSizer(wxHORIZONTAL);
	panel->SetAutoLayout(TRUE);
	panel->SetSizer(sizer_ogg_vorbis);


	panel = new wxPanel(controls_->notebook);
	controls_->notebook->AddPage(panel, "WAVE");
	wxBoxSizer *sizer_wave = new wxBoxSizer(wxHORIZONTAL);
	panel->SetAutoLayout(TRUE);
	panel->SetSizer(sizer_wave);

	controls_->populate(cfg_);

	SetAutoLayout(TRUE);
	SetSizer(sizer_root);
	sizer_root->Fit(this);
	sizer_root->SetSizeHints(this);

    Centre(wxBOTH | wxCENTER_FRAME);
}

FuiOptionsDialog::~FuiOptionsDialog()
{
	delete controls_;
}

bool FuiOptionsDialog::Validate()
{
	fprintf(stderr, "@@@ FuiOptionsDialog::Validate()\n");
	
	//
	// FLAC page
	//
	{
		unsigned u, u2;
		if(!local::string_to_unsigned(controls_->flac.encoding.max_lpc_order->GetValue(), u) || u > FLAC__MAX_LPC_ORDER)
			return local::validate_error(controls_->notebook, 1, wxString::Format("Maximum LPC order must be between 0 and %u", FLAC__MAX_LPC_ORDER));
		if(!local::string_to_unsigned(controls_->flac.encoding.qlp_coeff_precision->GetValue(), u) || (u > 0 && u < FLAC__MIN_QLP_COEFF_PRECISION) || (u >= (1u<<FLAC__SUBFRAME_LPC_QLP_COEFF_PRECISION_LEN)))
			return local::validate_error(controls_->notebook, 1, wxString::Format("QLP coefficient precision must be either 0 or between %u and %u", FLAC__MIN_QLP_COEFF_PRECISION, (1u<<FLAC__SUBFRAME_LPC_QLP_COEFF_PRECISION_LEN)));
		if(!local::string_to_unsigned(controls_->flac.encoding.min_residual_partition_order->GetValue(), u) || u > FLAC__SUBSET_MAX_RICE_PARTITION_ORDER)
			return local::validate_error(controls_->notebook, 1, wxString::Format("Minimum residual partition order must be between 0 and %u", FLAC__SUBSET_MAX_RICE_PARTITION_ORDER));
		if(!local::string_to_unsigned(controls_->flac.encoding.max_residual_partition_order->GetValue(), u2) || u2 > FLAC__SUBSET_MAX_RICE_PARTITION_ORDER)
			return local::validate_error(controls_->notebook, 1, wxString::Format("Maximum residual partition order must be between 0 and %u", FLAC__SUBSET_MAX_RICE_PARTITION_ORDER));
		if(u > u2)
			return local::validate_error(controls_->notebook, 1, wxString::Format("Maximum residual partition order must be >= minimum residual partition order"));
	}
	
	//
	// Ogg FLAC page
	//
	{
		unsigned u, u2;
		if(!local::string_to_unsigned(controls_->ogg_flac.encoding.max_lpc_order->GetValue(), u) || u > FLAC__MAX_LPC_ORDER)
			return local::validate_error(controls_->notebook, 3, wxString::Format("Maximum LPC order must be between 0 and %u", FLAC__MAX_LPC_ORDER));
		if(!local::string_to_unsigned(controls_->ogg_flac.encoding.qlp_coeff_precision->GetValue(), u) || (u > 0 && u < FLAC__MIN_QLP_COEFF_PRECISION) || (u >= (1u<<FLAC__SUBFRAME_LPC_QLP_COEFF_PRECISION_LEN)))
			return local::validate_error(controls_->notebook, 3, wxString::Format("QLP coefficient precision must be either 0 or between %u and %u", FLAC__MIN_QLP_COEFF_PRECISION, (1u<<FLAC__SUBFRAME_LPC_QLP_COEFF_PRECISION_LEN)));
		if(!local::string_to_unsigned(controls_->ogg_flac.encoding.min_residual_partition_order->GetValue(), u) || u > FLAC__SUBSET_MAX_RICE_PARTITION_ORDER)
			return local::validate_error(controls_->notebook, 3, wxString::Format("Minimum residual partition order must be between 0 and %u", FLAC__SUBSET_MAX_RICE_PARTITION_ORDER));
		if(!local::string_to_unsigned(controls_->ogg_flac.encoding.max_residual_partition_order->GetValue(), u2) || u2 > FLAC__SUBSET_MAX_RICE_PARTITION_ORDER)
			return local::validate_error(controls_->notebook, 3, wxString::Format("Maximum residual partition order must be between 0 and %u", FLAC__SUBSET_MAX_RICE_PARTITION_ORDER));
		if(u > u2)
			return local::validate_error(controls_->notebook, 3, wxString::Format("Maximum residual partition order must be >= minimum residual partition order"));
		if(!local::string_to_unsigned(controls_->ogg_flac.encoding.serial_number->GetValue(), u))
			return local::validate_error(controls_->notebook, 3, wxString::Format("Serial number must be an unsigned 32-bit integer"));
	}
	return true;
}

void FuiOptionsDialog::OnOk(wxCommandEvent& WXUNUSED(event))
{
	fprintf(stderr, "@@@ FuiOptionsDialog::OnOk()\n");
	if(Validate()) {
		controls_->apply(cfg_);
		if(IsModal())
			EndModal(wxID_OK);
		else {
			SetReturnCode(wxID_OK);
			this->Show(FALSE);
		}
	}
}

void FuiOptionsDialog::OnCancel(wxCommandEvent& WXUNUSED(event))
{
	EndModal(wxID_CANCEL);
}

void FuiOptionsDialog::OnPanelFLAC(wxCommandEvent &event)
{
	fprintf(stderr, "@@@@ FuiOptionsDialog::OnPanelFLAC() id=%d\n", event.GetId());
	controls_->propagate();
}

void FuiOptionsDialog::OnPanelOggFLAC(wxCommandEvent &event)
{
	fprintf(stderr, "@@@@ FuiOptionsDialog::OnPanelOggFLAC() id=%d\n", event.GetId());
	controls_->propagate();
}

void FuiOptionsDialog::build_global_controls(wxPanel *panel, wxSizer *sizer)
{
	controls_->global.output_directory = new wxTextCtrl(panel, -1, "", wxDefaultPosition, wxDefaultSize);
	#if wxUSE_TOOLTIPS
	controls_->global.output_directory->SetToolTip("@@@ needs work still");
	#endif // wxUSE_TOOLTIPS
	sizer->Add(local::labeled_control(panel, "&Output directory", controls_->global.output_directory), 0, wxLEFT | wxRIGHT, 5);
}

void FuiOptionsDialog::build_flac_controls(wxPanel *panel, wxSizer *sizer)
{
	wxSizer *sizer_encoding = new wxStaticBoxSizer(new wxStaticBox(panel, -1, _T("&Encoding")), wxVERTICAL);
	wxBoxSizer *sizer_both = new wxBoxSizer(wxVERTICAL);
	wxSizer *sizer_decoding = new wxStaticBoxSizer(new wxStaticBox(panel, -1, _T("&Decoding")), wxVERTICAL);
	wxSizer *sizer_tagging = new wxStaticBoxSizer(new wxStaticBox(panel, -1, _T("&Tagging")), wxVERTICAL);

	build_flac_encoding_controls(panel, sizer_encoding);
	build_flac_decoding_controls(panel, sizer_decoding);
	build_flac_tagging_controls(panel, sizer_tagging);

	sizer->Add(sizer_encoding, 0, wxGROW | (wxALL & ~wxLEFT), 10);
	sizer->Add(sizer_both, 1, wxGROW | (wxALL & ~wxRIGHT), 10);

	sizer_both->Add(sizer_decoding, 1, wxGROW);
	sizer_both->Add(sizer_tagging, 1, wxGROW);
}

void FuiOptionsDialog::build_flac_encoding_controls(wxPanel *panel, wxSizer *sizer)
{
	static const wxString effort_strings[] = {
		_T("low"),
		_T("medium"),
		_T("high"),
		_T("custom")
	};
	controls_->flac.encoding.effort = new wxChoice(panel, LOCAL__ID_FLAC_EFFORT, wxDefaultPosition, wxDefaultSize, WXSIZEOF(effort_strings), effort_strings, wxCB_DROPDOWN| wxCB_READONLY);
	sizer->Add(local::labeled_control(panel, "Effort", controls_->flac.encoding.effort), 0, wxLEFT | wxRIGHT, 5);

	controls_->flac.encoding.verify = new wxCheckBox(panel, -1, "Verify encoding result");
	sizer->Add(controls_->flac.encoding.verify, 0, wxLEFT | wxRIGHT, 5);

	controls_->flac.encoding.do_mid_side = new wxCheckBox(panel, LOCAL__ID_FLAC_DO_MID_SIDE, "Do mid-side coding");
	sizer->Add(controls_->flac.encoding.do_mid_side, 0, wxLEFT | wxRIGHT, 5);

	controls_->flac.encoding.adaptive_mid_side = new wxCheckBox(panel, -1, "Use adaptive mid-side switching");
	sizer->Add(controls_->flac.encoding.adaptive_mid_side, 0, wxLEFT | wxRIGHT, 5);

	controls_->flac.encoding.blocksize = new wxChoice(panel, -1, wxDefaultPosition, wxDefaultSize, WXSIZEOF(local::valid_flac_blocksize_strings), local::valid_flac_blocksize_strings, wxCB_DROPDOWN| wxCB_READONLY);
	sizer->Add(local::labeled_control(panel, "Blocksize", controls_->flac.encoding.blocksize), 0, wxLEFT | wxRIGHT, 5);

	controls_->flac.encoding.max_lpc_order = new wxTextCtrl(panel, -1, "", wxDefaultPosition, wxDefaultSize);
	controls_->flac.encoding.max_lpc_order->SetMaxLength(2);
	sizer->Add(local::labeled_control(panel, "Maximum LPC order", controls_->flac.encoding.max_lpc_order), 0, wxLEFT | wxRIGHT, 5);

	controls_->flac.encoding.do_qlp_coeff_prec_search = new wxCheckBox(panel, LOCAL__ID_FLAC_DO_QLP_COEFF_PREC_SEARCH, "Do QLP coefficient precision search");
	sizer->Add(controls_->flac.encoding.do_qlp_coeff_prec_search, 0, wxLEFT | wxRIGHT, 5);

	controls_->flac.encoding.qlp_coeff_precision = new wxTextCtrl(panel, -1, "", wxDefaultPosition, wxDefaultSize);
	controls_->flac.encoding.qlp_coeff_precision->SetMaxLength(2);
	sizer->Add(local::labeled_control(panel, "QLP coefficient precision", controls_->flac.encoding.qlp_coeff_precision), 0, wxLEFT | wxRIGHT, 5);

	controls_->flac.encoding.do_escape_coding = new wxCheckBox(panel, -1, "Do escape coding");
	sizer->Add(controls_->flac.encoding.do_escape_coding, 0, wxLEFT | wxRIGHT, 5);

	controls_->flac.encoding.do_exhaustive_model_search = new wxCheckBox(panel, -1, "Do exhaustive model search");
	sizer->Add(controls_->flac.encoding.do_exhaustive_model_search, 0, wxLEFT | wxRIGHT, 5);

	controls_->flac.encoding.min_residual_partition_order = new wxTextCtrl(panel, -1, "", wxDefaultPosition, wxDefaultSize);
	controls_->flac.encoding.min_residual_partition_order->SetMaxLength(2);
	sizer->Add(local::labeled_control(panel, "Minimum residual partition order", controls_->flac.encoding.min_residual_partition_order), 0, wxLEFT | wxRIGHT, 5);

	controls_->flac.encoding.max_residual_partition_order = new wxTextCtrl(panel, -1, "", wxDefaultPosition, wxDefaultSize);
	controls_->flac.encoding.max_residual_partition_order->SetMaxLength(2);
	sizer->Add(local::labeled_control(panel, "Maximum residual partition order", controls_->flac.encoding.max_residual_partition_order), 0, wxLEFT | wxRIGHT, 5);
}

void FuiOptionsDialog::build_flac_decoding_controls(wxPanel *panel, wxSizer *sizer)
{
	controls_->flac.decoding.verify_md5 = new wxCheckBox(panel, -1, "Verify MD5 signature");
	sizer->Add(controls_->flac.decoding.verify_md5, 0, wxLEFT | wxRIGHT, 5);
	//sizer->Add(0, 2, 0, wxGROW); // spacer
}

void FuiOptionsDialog::build_flac_tagging_controls(wxPanel *panel, wxSizer *sizer)
{
	controls_->flac.tagging.id3v1 = new wxCheckBox(panel, -1, "Copy ID3v1 tags");
	sizer->Add(controls_->flac.tagging.id3v1, 0, wxLEFT | wxRIGHT, 5);

	controls_->flac.tagging.id3v2 = new wxCheckBox(panel, -1, "Copy ID3v2 tags");
	sizer->Add(controls_->flac.tagging.id3v2, 0, wxLEFT | wxRIGHT, 5);

	controls_->flac.tagging.vorbiscomments = new wxCheckBox(panel, -1, "Copy Vorbis comments");
	sizer->Add(controls_->flac.tagging.vorbiscomments, 0, wxLEFT | wxRIGHT, 5);
}

void FuiOptionsDialog::build_ogg_flac_controls(wxPanel *panel, wxSizer *sizer)
{
	wxSizer *sizer_encoding = new wxStaticBoxSizer(new wxStaticBox(panel, -1, _T("&Encoding")), wxVERTICAL);
	wxBoxSizer *sizer_both = new wxBoxSizer(wxVERTICAL);
	wxSizer *sizer_decoding = new wxStaticBoxSizer(new wxStaticBox(panel, -1, _T("&Decoding")), wxVERTICAL);
	wxSizer *sizer_tagging = new wxStaticBoxSizer(new wxStaticBox(panel, -1, _T("&Tagging")), wxVERTICAL);

	build_ogg_flac_encoding_controls(panel, sizer_encoding);
	build_ogg_flac_decoding_controls(panel, sizer_decoding);
	build_ogg_flac_tagging_controls(panel, sizer_tagging);

	sizer->Add(sizer_encoding, 0, wxGROW | (wxALL & ~wxLEFT), 10);
	sizer->Add(sizer_both, 1, wxGROW | (wxALL & ~wxRIGHT), 10);

	sizer_both->Add(sizer_decoding, 1, wxGROW);
	sizer_both->Add(sizer_tagging, 1, wxGROW);
}

void FuiOptionsDialog::build_ogg_flac_encoding_controls(wxPanel *panel, wxSizer *sizer)
{
	static const wxString effort_strings[] = {
		_T("low"),
		_T("medium"),
		_T("high"),
		_T("custom")
	};
	controls_->ogg_flac.encoding.effort = new wxChoice(panel, LOCAL__ID_OGG_FLAC_EFFORT, wxDefaultPosition, wxDefaultSize, WXSIZEOF(effort_strings), effort_strings, wxCB_DROPDOWN| wxCB_READONLY);
	sizer->Add(local::labeled_control(panel, "Effort", controls_->ogg_flac.encoding.effort), 0, wxLEFT | wxRIGHT, 5);

	controls_->ogg_flac.encoding.verify = new wxCheckBox(panel, -1, "Verify encoding result");
	sizer->Add(controls_->ogg_flac.encoding.verify, 0, wxLEFT | wxRIGHT, 5);

	controls_->ogg_flac.encoding.do_mid_side = new wxCheckBox(panel, LOCAL__ID_OGG_FLAC_DO_MID_SIDE, "Do mid-side coding");
	sizer->Add(controls_->ogg_flac.encoding.do_mid_side, 0, wxLEFT | wxRIGHT, 5);

	controls_->ogg_flac.encoding.adaptive_mid_side = new wxCheckBox(panel, -1, "Use adaptive mid-side switching");
	sizer->Add(controls_->ogg_flac.encoding.adaptive_mid_side, 0, wxLEFT | wxRIGHT, 5);

	controls_->ogg_flac.encoding.blocksize = new wxChoice(panel, -1, wxDefaultPosition, wxDefaultSize, WXSIZEOF(local::valid_flac_blocksize_strings), local::valid_flac_blocksize_strings, wxCB_DROPDOWN| wxCB_READONLY);
	sizer->Add(local::labeled_control(panel, "Blocksize", controls_->ogg_flac.encoding.blocksize), 0, wxLEFT | wxRIGHT, 5);

	controls_->ogg_flac.encoding.max_lpc_order = new wxTextCtrl(panel, -1, "", wxDefaultPosition, wxDefaultSize);
	controls_->ogg_flac.encoding.max_lpc_order->SetMaxLength(2);
	sizer->Add(local::labeled_control(panel, "Maximum LPC order", controls_->ogg_flac.encoding.max_lpc_order), 0, wxLEFT | wxRIGHT, 5);

	controls_->ogg_flac.encoding.do_qlp_coeff_prec_search = new wxCheckBox(panel, LOCAL__ID_OGG_FLAC_DO_QLP_COEFF_PREC_SEARCH, "Do QLP coefficient precision search");
	sizer->Add(controls_->ogg_flac.encoding.do_qlp_coeff_prec_search, 0, wxLEFT | wxRIGHT, 5);

	controls_->ogg_flac.encoding.qlp_coeff_precision = new wxTextCtrl(panel, -1, "", wxDefaultPosition, wxDefaultSize);
	controls_->ogg_flac.encoding.qlp_coeff_precision->SetMaxLength(2);
	sizer->Add(local::labeled_control(panel, "QLP coefficient precision", controls_->ogg_flac.encoding.qlp_coeff_precision), 0, wxLEFT | wxRIGHT, 5);

	controls_->ogg_flac.encoding.do_escape_coding = new wxCheckBox(panel, -1, "Do escape coding");
	sizer->Add(controls_->ogg_flac.encoding.do_escape_coding, 0, wxLEFT | wxRIGHT, 5);

	controls_->ogg_flac.encoding.do_exhaustive_model_search = new wxCheckBox(panel, -1, "Do exhaustive model search");
	sizer->Add(controls_->ogg_flac.encoding.do_exhaustive_model_search, 0, wxLEFT | wxRIGHT, 5);

	controls_->ogg_flac.encoding.min_residual_partition_order = new wxTextCtrl(panel, -1, "", wxDefaultPosition, wxDefaultSize);
	controls_->ogg_flac.encoding.min_residual_partition_order->SetMaxLength(2);
	sizer->Add(local::labeled_control(panel, "Minimum residual partition order", controls_->ogg_flac.encoding.min_residual_partition_order), 0, wxLEFT | wxRIGHT, 5);

	controls_->ogg_flac.encoding.max_residual_partition_order = new wxTextCtrl(panel, -1, "", wxDefaultPosition, wxDefaultSize);
	controls_->ogg_flac.encoding.max_residual_partition_order->SetMaxLength(2);
	sizer->Add(local::labeled_control(panel, "Maximum residual partition order", controls_->ogg_flac.encoding.max_residual_partition_order), 0, wxLEFT | wxRIGHT, 5);

	controls_->ogg_flac.encoding.serial_number = new wxTextCtrl(panel, -1, "", wxDefaultPosition, wxDefaultSize);
	controls_->ogg_flac.encoding.serial_number->SetMaxLength(10);
	sizer->Add(local::labeled_control(panel, "Stream serial number", controls_->ogg_flac.encoding.serial_number), 0, wxLEFT | wxRIGHT, 5);
}

void FuiOptionsDialog::build_ogg_flac_decoding_controls(wxPanel *panel, wxSizer *sizer)
{
	controls_->ogg_flac.decoding.verify_md5 = new wxCheckBox(panel, -1, "Verify MD5 signature");
	sizer->Add(controls_->ogg_flac.decoding.verify_md5, 0, wxLEFT | wxRIGHT, 5);
	//sizer->Add(0, 2, 0, wxGROW); // spacer
}

void FuiOptionsDialog::build_ogg_flac_tagging_controls(wxPanel *panel, wxSizer *sizer)
{
	controls_->ogg_flac.tagging.vorbiscomments = new wxCheckBox(panel, -1, "Copy Vorbis comments");
	sizer->Add(controls_->ogg_flac.tagging.vorbiscomments, 0, wxLEFT | wxRIGHT, 5);
}
