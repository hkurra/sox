/* fui - FLAC User Interface
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "FuiId.h"
#include "FuiFrame.h"
#include "FuiProcessingGrid.h"
#include "FuiProcessingThread.h"
#include "FUI/Codec/Transducer.h"
#include "wx/app.h" // for wxPostEvent()

#if !wxUSE_THREADS
	#error "fui requires thread support!"
//@@@ this check needs to go in configure
#endif // wxUSE_THREADS

FuiProcessingThread::FuiProcessingThread(FuiFrame *frame, FuiProcessingGrid *processing_grid):
wxThread(),
frame_(frame),
processing_grid_(processing_grid),
working_element_(0)
{
	assert(0 != frame);
	assert(0 != processing_grid);
}

void FuiProcessingThread::OnExit()
{
	fprintf(stderr,"@@@ FuiProcessingThread::OnExit() start\n");
	// this is a synthetic event; wxCommandEvent:wxEVT_COMMAND_MENU_SELECTED is used for convenience
	wxCommandEvent event(wxEVT_COMMAND_MENU_SELECTED, FuiFrame_CommandId__ProcessingThreadEvent_Exit);
	event.SetInt(0);
	wxPostEvent(frame_, event);
	fprintf(stderr,"@@@ FuiProcessingThread::OnExit() end\n");
}

void *FuiProcessingThread::Entry()
{
	bool ok = true;
	while(ok && 0 != (working_element_ = get_first_pending_element())) {
		assert(FUI::Codec::Transducer::is_legal(working_element_->input_format_, working_element_->output_format_));

		{ // FIFO critical section
			FuiProcessingGrid::FIFO_Locker locker(*processing_grid_);
			working_element_->set_state(FUI::Transducer::Element::WORKING);
			working_element_->set_percent_complete(0);
		}

		{
			// this is a synthetic event; wxCommandEvent:wxEVT_COMMAND_MENU_SELECTED is used for convenience
			wxCommandEvent event(wxEVT_COMMAND_MENU_SELECTED, FuiFrame_CommandId__ProcessingThreadEvent_ElementProgress);
			event.SetInt((int)working_element_->primary_key());
			wxPostEvent(frame_, event);
		}

		fprintf(stderr,"@@@ transduce(%s -> %s) start\n",working_element_->input_path_.c_str(),working_element_->output_path_.c_str());
		FUI::Codec::Transducer::Result result =
			FUI::Codec::Transducer::transduce(
				working_element_->input_format_,
				working_element_->input_path_,
				*working_element_->codec_decode_options_,
				*working_element_->tag_decode_options_,
				working_element_->output_format_,
				working_element_->output_path_,
				*working_element_->codec_encode_options_,
				*working_element_->tag_encode_options_,
				/*thread_context=*/this
			);
		ok = result.ok_;
		fprintf(stderr,"@@@ transduce(%s -> %s) end, ok=%s\n",working_element_->input_path_.c_str(),working_element_->output_path_.c_str(),ok?"true":"false");
		if(ok) {
			{ // FIFO critical section
				FuiProcessingGrid::FIFO_Locker locker(*processing_grid_);
				working_element_->set_state(FUI::Transducer::Element::FINISHED);
				working_element_->set_percent_complete(100);
			}

			{
				// this is a synthetic event; wxCommandEvent:wxEVT_COMMAND_MENU_SELECTED is used for convenience
				wxCommandEvent event(wxEVT_COMMAND_MENU_SELECTED, FuiFrame_CommandId__ProcessingThreadEvent_ElementProgress);
				event.SetInt((int)working_element_->primary_key());
				wxPostEvent(frame_, event);
			}
		}
		else {
			fprintf(stderr, "@@@ transduce failed, reason={%s}\n", result.description_.c_str());
			{ // FIFO critical section
				FuiProcessingGrid::FIFO_Locker locker(*processing_grid_);
				working_element_->set_state(FUI::Transducer::Element::PENDING);
				working_element_->set_percent_complete(0);
			}

			{
				// this is a synthetic event; wxCommandEvent:wxEVT_COMMAND_MENU_SELECTED is used for convenience
				wxCommandEvent event(wxEVT_COMMAND_MENU_SELECTED, FuiFrame_CommandId__ProcessingThreadEvent_ElementProgress);
				event.SetInt((int)working_element_->primary_key());
				wxPostEvent(frame_, event);
			}
		}
	}

	return NULL;
}

FUI::Transducer::Element *FuiProcessingThread::get_first_pending_element()
{
	// FIFO critical section
	FuiProcessingGrid::FIFO_Locker locker(*processing_grid_);
	return processing_grid_->fifo().first_pending();
}

void FuiProcessingThread::progress_report(unsigned percent_complete)
{
	{ // FIFO critical section
		FuiProcessingGrid::FIFO_Locker locker(*processing_grid_);
		working_element_->set_percent_complete(percent_complete);
	}

	// this is a synthetic event; wxCommandEvent:wxEVT_COMMAND_MENU_SELECTED is used for convenience
	wxCommandEvent event(wxEVT_COMMAND_MENU_SELECTED, FuiFrame_CommandId__ProcessingThreadEvent_ElementProgress);
	event.SetInt((int)working_element_->primary_key());
	wxPostEvent(frame_, event);
}
