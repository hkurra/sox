/* fui - FLAC User Interface
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "Configuration.h"
#include "FuiId.h"
#include "FuiFrame.h"
#include "FuiOptionsDialog.h"
#include "FuiProcessingGrid.h"
#include "FuiProcessingThread.h"
#include "FUI/Codec/Recognizer.h"
#include "wx/choicdlg.h"
#include "wx/filedlg.h"
#include "wx/icon.h"
#include "wx/menu.h"
#include "wx/msgdlg.h"
#include "wx/toolbar.h"
#include <stdio.h>


#if defined(__WXGTK__) || defined(__WXMOTIF__) || defined(__WXMAC__) || defined(__WXMGL__)
#include "bitmaps/add_files.xpm"
#include "bitmaps/exit.xpm"
#include "bitmaps/fui.xpm"
#include "bitmaps/pause.xpm"
#include "bitmaps/start.xpm"
#include "bitmaps/stop.xpm"
#endif

BEGIN_EVENT_TABLE(FuiFrame, wxFrame)
	EVT_MENU(FuiFrame_MenuId__Exit,     FuiFrame::OnExit)
	EVT_MENU(FuiFrame_MenuId__AddFiles, FuiFrame::OnAddFiles)
	EVT_MENU(FuiFrame_MenuId__Start,    FuiFrame::OnStart)
	EVT_MENU(FuiFrame_MenuId__Pause,    FuiFrame::OnPause)
	EVT_MENU(FuiFrame_MenuId__Stop,     FuiFrame::OnStop)
	EVT_MENU(FuiFrame_MenuId__Options,  FuiFrame::OnOptions)
	EVT_MENU(FuiFrame_MenuId__About,    FuiFrame::OnAbout)

	// These are 'phony' menu events; they are really generic command events
	// used to communicate info from the thread to the frame.
	EVT_MENU(FuiFrame_CommandId__ProcessingThreadEvent_ElementProgress, FuiFrame::OnProcessingThreadEvent_ElementProgress)
	EVT_MENU(FuiFrame_CommandId__ProcessingThreadEvent_Exit, FuiFrame::OnProcessingThreadEvent_Exit)
END_EVENT_TABLE()


namespace local {

	void transform_path(const wxString &in, wxString &out, const wxString &specifier)
	{
		if(specifier == "" || specifier == ".")
			out = in;
		else {
			assert(0); //@@@ need to figure out what to do here
			out = in;
		}
	}

	std::string divine(
		const FUI::Configuration::Instance *cfg_,
		const wxString &input_path,
		wxString &output_path,
		FUI::Codec::Type &input_format,
		FUI::Codec::Type output_format,
		const FUI::Codec::Options::Decode::Prototype *&codec_decode_options,
		const FUI::Codec::Options::Encode::Prototype *&codec_encode_options,
		const FUI::Tag::Options::Decode::Prototype *&tag_decode_options,
		const FUI::Tag::Options::Encode::Prototype *&tag_encode_options
	)
	{
		std::string status = FUI::Codec::Recognizer::divine_format_from_file(input_path.c_str(), input_format, /*use_extension=*/true);

		//
		// set output_path
		//
		transform_path(input_path, output_path, cfg_->options_.global.output_directory.c_str());

		if(status.length() > 0)
			return status;

		int dot = output_path.Find('.', /*fromEnd=*/TRUE);

		// FUI::Codec::Recognizer::divine_format_from_file() should have taken care of this case:
		assert(dot >= 0);

		output_path = output_path.Left(dot);

		if(0 == strcmp(FUI::Codec::TypeExtension[input_format], FUI::Codec::TypeExtension[output_format]))
			output_path += "-new";

		output_path += FUI::Codec::TypeExtension[output_format];

		//
		// set options
		//
		codec_decode_options = cfg_->codec_decode_options(input_format);
		codec_encode_options = cfg_->codec_encode_options(output_format);
		tag_decode_options = cfg_->tag_decode_options(input_format);
		tag_encode_options = cfg_->tag_encode_options(output_format);

		return status;
	}

};


FuiFrame::FuiFrame(FUI::Configuration::Instance *cfg, const wxString& title, const wxPoint& pos, const wxSize& size):
wxFrame(NULL, -1, title, pos, size),
cfg_(cfg)
{
	state_ = STOPPED;
	processing_thread_ = 0;

	SetIcon(wxICON(fui));

	wxMenu *menuFile = new wxMenu;
	menuFile->Append(FuiFrame_MenuId__AddFiles, _T("&Add Files\tAlt-A"), _T("Add files"));
	menuFile->AppendSeparator();
	menuFile->Append(FuiFrame_MenuId__Start, _T("&Start"), _T("Start processing"));
	menuFile->Append(FuiFrame_MenuId__Pause, _T("P&ause"), _T("Pause processing"));
	menuFile->Append(FuiFrame_MenuId__Stop, _T("S&top"), _T("Stop processing"));
	menuFile->AppendSeparator();
	menuFile->Append(FuiFrame_MenuId__Options, _T("&Options\tAlt-O"), _T("Edit options"));
	menuFile->AppendSeparator();
	menuFile->Append(FuiFrame_MenuId__Exit, _T("E&xit\tAlt-X"), _T("Exit fui"));

	wxMenu *menuHelp = new wxMenu;
	menuHelp->Append(FuiFrame_MenuId__About, _T("&About...\tF1"), _T("Show 'about' dialog"));

	wxMenuBar *menuBar = new wxMenuBar();
	menuBar->Append(menuFile, _T("&File"));
	menuBar->Append(menuHelp, _T("&Help"));

	SetMenuBar(menuBar);

	CreateToolBar(wxNO_BORDER | wxTB_HORIZONTAL | wxTB_3DBUTTONS);
	CreateStatusBar(1);

	wxBitmap bitmapExit;
	wxBitmap bitmapAddFiles;

	bitmapExit = wxBitmap(exit_xpm);
	bitmapAddFiles = wxBitmap(add_files_xpm);
	bitmap_start_ = wxBitmap(start_xpm);
	bitmap_pause_ = wxBitmap(pause_xpm);
	bitmap_stop_ = wxBitmap(stop_xpm);

	GetToolBar()->AddTool(FuiFrame_MenuId__Exit, bitmapExit, _T("Exit fui"), _T("Exit fui"));
	GetToolBar()->AddTool(FuiFrame_MenuId__AddFiles, bitmapAddFiles, _T("Add files"), _T("Add files to end of processing queue"));
	toolBarTool_start_ = GetToolBar()->AddTool(FuiFrame_MenuId__Start, bitmap_start_, _T("Start"), _T("Start processing files"));
	toolBarTool_pause_ = GetToolBar()->AddTool(FuiFrame_MenuId__Pause, bitmap_pause_, _T("Pause"), _T("Pause processing files"));
	toolBarTool_stop_ = GetToolBar()->AddTool(FuiFrame_MenuId__Stop, bitmap_stop_, _T("Stop"), _T("Stop processing files"));

	GetToolBar()->EnableTool(FuiFrame_MenuId__Start, true);
	GetToolBar()->EnableTool(FuiFrame_MenuId__Pause, false);
	GetToolBar()->EnableTool(FuiFrame_MenuId__Stop, false);

	GetToolBar()->Realize();

	processing_grid_ = new FuiProcessingGrid(this);
}

void FuiFrame::OnAbout(wxCommandEvent& WXUNUSED(event))
{
	wxString msg;
	msg.Printf( _T("fui %s"), VERSION);

	wxMessageBox(msg, _T("About fui"), wxOK | wxICON_INFORMATION, this);
}

void FuiFrame::OnAddFiles(wxCommandEvent& WXUNUSED(event))
{
	fprintf(stderr, "@@@ FuiFrame::OnAddFiles()\n");

	wxFileDialog file_dialog(
		this,
		_T("@@@ some message @@@"),
		_T(""),
		_T(""),
		_T("FLAC files (*.flac)|*.flac|MP3 files (*.mp3)|*.mp3|Ogg Vorbis, Ogg FLAC (*.ogg)|*.ogg|WAVE files (*.wav)|*.wav|All supported audio|*.flac;*.mp3;*.ogg;*.wav"),
		wxMULTIPLE
	);

	if(file_dialog.ShowModal() == wxID_OK) {
		wxArrayString paths, filenames;

		file_dialog.GetPaths(paths);
		file_dialog.GetFilenames(filenames);

		//@@@ magic list
		const wxString codec_strings[] = { "FLAC", "MP3", "Ogg FLAC", "Ogg Vorbis", "RIFF WAVE" } ;
		wxSingleChoiceDialog codec_dialog(this, "Choose an output format to encode the added files to:", "Output format", WXSIZEOF(codec_strings), codec_strings);

		codec_dialog.SetSelection(0);

		if (codec_dialog.ShowModal() == wxID_OK) {
			wxString s = codec_dialog.GetStringSelection();

			//@@@ generalize (i.e. make a loop-based search instead of a bunch of "if"s):
			FUI::Codec::Type output_format;
			if(s == codec_strings[0])
				output_format = FUI::Codec::FLAC;
			else if(s == codec_strings[1])
				output_format = FUI::Codec::MP3;
			else if(s == codec_strings[2])
				output_format = FUI::Codec::OGG_FLAC;
			else if(s == codec_strings[3])
				output_format = FUI::Codec::OGG_VORBIS;
			else if(s == codec_strings[4])
				output_format = FUI::Codec::RIFF_WAVE;

			{
				FuiProcessingGrid::FIFO_Locker locker(*processing_grid_);
				for(size_t n = 0; n < paths.GetCount(); n++) {
					wxString input_path = paths[n];
					wxString output_path;
					FUI::Codec::Type input_format;
					const FUI::Codec::Options::Decode::Prototype *codec_decode_options;
					const FUI::Codec::Options::Encode::Prototype *codec_encode_options;
					const FUI::Tag::Options::Decode::Prototype *tag_decode_options;
					const FUI::Tag::Options::Encode::Prototype *tag_encode_options;
					std::string status = local::divine(cfg_, input_path, output_path, input_format, output_format, codec_decode_options, codec_encode_options, tag_decode_options, tag_encode_options);
					if(status.length() > 0) {
						fprintf(stderr,"@@@@ divine() error: %s\n", status.c_str());
					}
					else {
						FUI::Transducer::Element *new_element = new FUI::Transducer::Element(input_path.c_str(), output_path.c_str(), input_format, output_format, codec_decode_options, codec_encode_options, tag_decode_options, tag_encode_options);
						processing_grid_->fifo().enqueue(new_element);
						fprintf(stderr,"@@@@ enqueue(input=%s(%s), output=%s(%s))\n",new_element->input_path_.c_str(), FUI::Codec::TypeString[input_format], new_element->output_path_.c_str(), FUI::Codec::TypeString[output_format]);
					}
				}
			}
			processing_grid_->dimensions_changed(paths.GetCount(), -1);
		}
	}
}

void FuiFrame::OnExit(wxCommandEvent& WXUNUSED(event))
{
	Close(/*force=*/TRUE);
}

void FuiFrame::OnProcessingThreadEvent_ElementProgress(wxCommandEvent& WXUNUSED(event))
{
	processing_grid_->ForceRefresh();
}

void FuiFrame::OnProcessingThreadEvent_Exit(wxCommandEvent& WXUNUSED(event))
{
	fprintf(stderr,"@@@ FuiFrame::OnProcessingThreadEvent_Exit() start\n");
	processing_thread_ = 0;
	state_ = STOPPED;
	GetToolBar()->EnableTool(FuiFrame_MenuId__Exit, true);
	GetToolBar()->EnableTool(FuiFrame_MenuId__Start, true);
	GetToolBar()->EnableTool(FuiFrame_MenuId__Pause, false);
	GetToolBar()->EnableTool(FuiFrame_MenuId__Stop, false);
	fprintf(stderr,"@@@ FuiFrame::OnProcessingThreadEvent_Exit() end\n");
}

void FuiFrame::OnStart(wxCommandEvent& WXUNUSED(event))
{
	fprintf(stderr, "@@@ FuiFrame::OnStart()\n");

	if(state_ == RUNNING)
		return;

	GetToolBar()->EnableTool(FuiFrame_MenuId__Exit, false);
	GetToolBar()->EnableTool(FuiFrame_MenuId__Start, false);
	GetToolBar()->EnableTool(FuiFrame_MenuId__Pause, true);
	GetToolBar()->EnableTool(FuiFrame_MenuId__Stop, true);

	if(0 == processing_thread_) {
		processing_thread_ = new FuiProcessingThread(this, processing_grid_);

		if(processing_thread_->Create() != wxTHREAD_NO_ERROR) {
			(void) wxMessageBox("Can't create thread!", "fui: Error");
			return;
		}
		else
			processing_thread_->Run();
	}
	else
		processing_thread_->Resume();

	state_ = RUNNING;
}

void FuiFrame::OnPause(wxCommandEvent& WXUNUSED(event))
{
	fprintf(stderr, "@@@ FuiFrame::OnPause()\n");

	if(state_ != RUNNING)
		return;

	assert(0 != processing_thread_);

	GetToolBar()->EnableTool(FuiFrame_MenuId__Exit, false);
	GetToolBar()->EnableTool(FuiFrame_MenuId__Start, true);
	GetToolBar()->EnableTool(FuiFrame_MenuId__Pause, false);
	GetToolBar()->EnableTool(FuiFrame_MenuId__Stop, false);

	processing_thread_->Pause();

	state_ = PAUSED;
}

void FuiFrame::OnStop(wxCommandEvent& WXUNUSED(event))
{
	fprintf(stderr, "@@@ FuiFrame::OnStop()\n");

	if(state_ != RUNNING)
		return;

	assert(0 != processing_thread_);

	processing_thread_->Delete();
}

void FuiFrame::OnOptions(wxCommandEvent& WXUNUSED(event))
{
	fprintf(stderr, "@@@ FuiFrame::OnOptions()\n");

	FuiOptionsDialog dialog(cfg_, this);

	(void) dialog.ShowModal();
}
