/* fui - FLAC User Interface
 * Copyright (C) 2002,2003  Josh Coalson
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "Configuration.h"
#include "FUI/Codec/TagMapping.h"
#include "wx/confbase.h"

namespace FUI {
	namespace Configuration {

		Instance::Instance():
		encode_options_flac_low_(1),
		encode_options_flac_medium_(5),
		encode_options_flac_high_(8),
		encode_options_ogg_flac_low_(1),
		encode_options_ogg_flac_medium_(5),
		encode_options_ogg_flac_high_(8)
		{
			options_.global.output_directory = "";
			options_.codec.encode.flac.effort = Instance::Options::Codec::Encode::FLAC::MEDIUM;
			options_.codec.encode.ogg_flac.effort = Instance::Options::Codec::Encode::OggFLAC::MEDIUM;

			fprintf(stderr, "@@@ FUI::Configuration::Instance::Instance() enter\n");
			codec_decode_options_[FUI::Codec::FLAC] = &options_.codec.decode.flac;
			codec_decode_options_[FUI::Codec::MP3] = &options_.codec.decode.mp3;
			codec_decode_options_[FUI::Codec::OGG_FLAC] = &options_.codec.decode.ogg_flac;
			codec_decode_options_[FUI::Codec::OGG_VORBIS] = &options_.codec.decode.ogg_vorbis;
			codec_decode_options_[FUI::Codec::RIFF_WAVE] = &options_.codec.decode.wave;

			codec_encode_options_[FUI::Codec::FLAC] = &options_.codec.encode.flac.custom;
			codec_encode_options_[FUI::Codec::MP3] = &options_.codec.encode.mp3;
			codec_encode_options_[FUI::Codec::OGG_FLAC] = &options_.codec.encode.ogg_flac.custom;
			codec_encode_options_[FUI::Codec::OGG_VORBIS] = &options_.codec.encode.ogg_vorbis;
			codec_encode_options_[FUI::Codec::RIFF_WAVE] = &options_.codec.encode.wave;

			tag_decode_options_[FUI::Tag::NULL_TAG] = &options_.tag.decode.null;
			tag_decode_options_[FUI::Tag::FLAC] = &options_.tag.decode.vorbis_comment;
			tag_decode_options_[FUI::Tag::ID3] = &options_.tag.decode.id3;
			tag_decode_options_[FUI::Tag::OGG_FLAC] = &options_.tag.decode.vorbis_comment;
			tag_decode_options_[FUI::Tag::OGG_VORBIS] = &options_.tag.decode.vorbis_comment;

			tag_encode_options_[FUI::Tag::NULL_TAG] = &options_.tag.encode.null;
			tag_encode_options_[FUI::Tag::FLAC] = &options_.tag.encode.vorbis_comment;
			tag_encode_options_[FUI::Tag::ID3] = &options_.tag.encode.id3;
			tag_encode_options_[FUI::Tag::OGG_FLAC] = &options_.tag.encode.vorbis_comment;
			tag_encode_options_[FUI::Tag::OGG_VORBIS] = &options_.tag.encode.vorbis_comment;

			wxConfigBase *cfg = wxConfigBase::Get(/*CreateOnDemand=*/TRUE);
			if(cfg->GetNumberOfEntries(/*bRecursive=*/TRUE) == 0)
				store_to_persistent_db();
			else
				load_from_persistent_db();
			fprintf(stderr, "@@@ FUI::Configuration::Instance::Instance() exit\n");
		}

		Instance::~Instance()
		{
			fprintf(stderr, "@@@ FUI::Configuration::Instance::~Instance()\n");
		}

		const FUI::Codec::Options::Decode::Prototype *Instance::codec_decode_options(FUI::Codec::Type type) const
		{
			assert(type < FUI::Codec::TYPE_MAX);
			return codec_decode_options_[type];
		}

		FUI::Codec::Options::Decode::Prototype *Instance::codec_decode_options(FUI::Codec::Type type)
		{
			assert(type < FUI::Codec::TYPE_MAX);
			return codec_decode_options_[type];
		}

		const FUI::Codec::Options::Encode::Prototype *Instance::codec_encode_options(FUI::Codec::Type type) const
		{
			assert(type < FUI::Codec::TYPE_MAX);
			if(type == FUI::Codec::FLAC && options_.codec.encode.flac.effort != Instance::Options::Codec::Encode::FLAC::CUSTOM) {
				switch(options_.codec.encode.flac.effort) {
					case Instance::Options::Codec::Encode::FLAC::LOW:
						return &encode_options_flac_low_;
					case Instance::Options::Codec::Encode::FLAC::MEDIUM:
						return &encode_options_flac_medium_;
					case Instance::Options::Codec::Encode::FLAC::HIGH:
						return &encode_options_flac_high_;
					default:
						assert(0);
				}
			}
			else if(type == FUI::Codec::OGG_FLAC && options_.codec.encode.ogg_flac.effort != Instance::Options::Codec::Encode::OggFLAC::CUSTOM) {
				switch(options_.codec.encode.ogg_flac.effort) {
					case Instance::Options::Codec::Encode::OggFLAC::LOW:
						return &encode_options_ogg_flac_low_;
					case Instance::Options::Codec::Encode::OggFLAC::MEDIUM:
						return &encode_options_ogg_flac_medium_;
					case Instance::Options::Codec::Encode::OggFLAC::HIGH:
						return &encode_options_ogg_flac_high_;
					default:
						assert(0);
				}
			}
			else
				return codec_encode_options_[type];
		}

		FUI::Codec::Options::Encode::Prototype *Instance::codec_encode_options(FUI::Codec::Type type)
		{
			// Instead of duplicating code, we cheat with a const_cast.
			// Since we own it it's perfectly safe.
			return
				const_cast<FUI::Codec::Options::Encode::Prototype *>(
					((const Instance*)this)->codec_encode_options(type)
				);
		}

		const FUI::Tag::Options::Decode::Prototype *Instance::tag_decode_options(FUI::Tag::Type type) const
		{
			assert(type < FUI::Tag::TYPE_MAX);
			return tag_decode_options_[type];
		}

		FUI::Tag::Options::Decode::Prototype *Instance::tag_decode_options(FUI::Tag::Type type)
		{
			assert(type < FUI::Tag::TYPE_MAX);
			return tag_decode_options_[type];
		}

		const FUI::Tag::Options::Encode::Prototype *Instance::tag_encode_options(FUI::Tag::Type type) const
		{
			assert(type < FUI::Tag::TYPE_MAX);
			return tag_encode_options_[type];
		}

		FUI::Tag::Options::Encode::Prototype *Instance::tag_encode_options(FUI::Tag::Type type)
		{
			assert(type < FUI::Tag::TYPE_MAX);
			return tag_encode_options_[type];
		}

		const FUI::Tag::Options::Decode::Prototype *Instance::tag_decode_options(FUI::Codec::Type type) const
		{
			assert(type < FUI::Codec::TYPE_MAX);
			return tag_decode_options_[FUI::Codec::TagMapping[type]];
		}

		FUI::Tag::Options::Decode::Prototype *Instance::tag_decode_options(FUI::Codec::Type type)
		{
			assert(type < FUI::Codec::TYPE_MAX);
			return tag_decode_options_[FUI::Codec::TagMapping[type]];
		}

		const FUI::Tag::Options::Encode::Prototype *Instance::tag_encode_options(FUI::Codec::Type type) const
		{
			assert(type < FUI::Codec::TYPE_MAX);
			return tag_encode_options_[FUI::Codec::TagMapping[type]];
		}

		FUI::Tag::Options::Encode::Prototype *Instance::tag_encode_options(FUI::Codec::Type type)
		{
			assert(type < FUI::Codec::TYPE_MAX);
			return tag_encode_options_[FUI::Codec::TagMapping[type]];
		}

		void Instance::load_from_persistent_db()
		{
			fprintf(stderr,"@@@ FUI::Configuration::Instance::load_from_persistent_db()\n");
			wxConfigBase *cfg = wxConfigBase::Get();

			assert(0 != cfg);

			load_global(cfg);
			load_codec(cfg);
			load_tag(cfg);
		}

		void Instance::store_to_persistent_db()
		{
			fprintf(stderr,"@@@ FUI::Configuration::Instance::store_to_persistent_db()\n");
			wxConfigBase *cfg = wxConfigBase::Get();

			assert(0 != cfg);

			store_global(cfg);
			store_codec(cfg);
			store_tag(cfg);

			cfg->Flush();
		}

		void Instance::load_global(wxConfigBase *cfg)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Global");

			load_string(cfg, "output_directory", options_.global.output_directory);

			cfg->SetPath(path);
		}

		void Instance::load_codec(wxConfigBase *cfg)
		{
			load_codec_options(cfg);
		}

		void Instance::load_codec_options(wxConfigBase *cfg)
		{
			load_codec_options_decode(cfg);
			load_codec_options_encode(cfg);
		}

		void Instance::load_codec_options_decode(wxConfigBase *cfg)
		{
			load_codec_options_decode_flac(cfg, options_.codec.decode.flac);
			load_codec_options_decode_mp3(cfg, options_.codec.decode.mp3);
			load_codec_options_decode_ogg_flac(cfg, options_.codec.decode.ogg_flac);
			load_codec_options_decode_ogg_vorbis(cfg, options_.codec.decode.ogg_vorbis);
			load_codec_options_decode_wave(cfg, options_.codec.decode.wave);
		}

		void Instance::load_codec_options_decode_flac(wxConfigBase *cfg, FUI::Codec::Options::Decode::FLAC &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Codec/Options/Decode/FLAC");

			load_bool(cfg, "verify_md5", opt.verify_md5_);

			cfg->SetPath(path);
		}

		void Instance::load_codec_options_decode_mp3(wxConfigBase *cfg, FUI::Codec::Options::Decode::MP3 &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Codec/Options/Decode/MP3");

			// no options (yet)
			(void)opt;

			cfg->SetPath(path);
		}

		void Instance::load_codec_options_decode_ogg_flac(wxConfigBase *cfg, FUI::Codec::Options::Decode::OggFLAC &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Codec/Options/Decode/OggFLAC");

			load_bool(cfg, "verify_md5", opt.verify_md5_);

			cfg->SetPath(path);
		}

		void Instance::load_codec_options_decode_ogg_vorbis(wxConfigBase *cfg, FUI::Codec::Options::Decode::OggVorbis &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Codec/Options/Decode/OggVorbis");

			// no options (yet)
			(void)opt;

			cfg->SetPath(path);
		}

		void Instance::load_codec_options_decode_wave(wxConfigBase *cfg, FUI::Codec::Options::Decode::RiffWave &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Codec/Options/Decode/RiffWave");

			// no options (yet)
			(void)opt;

			cfg->SetPath(path);
		}

		void Instance::load_codec_options_encode(wxConfigBase *cfg)
		{
			load_codec_options_encode_flac(cfg, options_.codec.encode.flac);
			load_codec_options_encode_mp3(cfg, options_.codec.encode.mp3);
			load_codec_options_encode_ogg_flac(cfg, options_.codec.encode.ogg_flac);
			load_codec_options_encode_ogg_vorbis(cfg, options_.codec.encode.ogg_vorbis);
			load_codec_options_encode_wave(cfg, options_.codec.encode.wave);
		}

		void Instance::load_codec_options_encode_flac(wxConfigBase *cfg, Instance::Options::Codec::Encode::FLAC &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Codec/Options/Encode/FLAC");

			{
				unsigned e;
				load_unsigned(cfg, "effort", e);
				if(e > Instance::Options::Codec::Encode::FLAC::CUSTOM)
					e = Instance::Options::Codec::Encode::FLAC::CUSTOM;
				opt.effort = (Instance::Options::Codec::Encode::FLAC::Effort)e;
			}

			cfg->SetPath("/Codec/Options/Encode/FLAC/Custom");

			load_bool(cfg, "verify", opt.custom.verify_);
			load_bool(cfg, "subset", opt.custom.subset_);
			load_bool(cfg, "do_mid_side", opt.custom.do_mid_side_);
			load_bool(cfg, "adaptive_mid_side", opt.custom.adaptive_mid_side_);
			load_unsigned(cfg, "blocksize", opt.custom.blocksize_);
			load_unsigned(cfg, "max_lpc_order", opt.custom.max_lpc_order_);
			load_unsigned(cfg, "qlp_coeff_precision", opt.custom.qlp_coeff_precision_);
			load_bool(cfg, "do_qlp_coeff_prec_search", opt.custom.do_qlp_coeff_prec_search_);
			load_bool(cfg, "do_escape_coding", opt.custom.do_escape_coding_); // do_escape_coding is deprecated in libFLAC
			load_bool(cfg, "do_exhaustive_model_search", opt.custom.do_exhaustive_model_search_);
			load_unsigned(cfg, "min_residual_partition_order", opt.custom.min_residual_partition_order_);
			load_unsigned(cfg, "max_residual_partition_order", opt.custom.max_residual_partition_order_);
			// rice_parameter_search_dist is deprecated in libFLAC

			cfg->SetPath(path);
		}

		void Instance::load_codec_options_encode_mp3(wxConfigBase *cfg, FUI::Codec::Options::Encode::MP3 &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Codec/Options/Encode/MP3");

			load_unsigned(cfg, "target_bps", opt.target_bps_);

			cfg->SetPath(path);
		}

		void Instance::load_codec_options_encode_ogg_flac(wxConfigBase *cfg, Instance::Options::Codec::Encode::OggFLAC &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Codec/Options/Encode/OggFLAC");

			{
				unsigned e;
				load_unsigned(cfg, "effort", e);
				if(e > Instance::Options::Codec::Encode::OggFLAC::CUSTOM)
					e = Instance::Options::Codec::Encode::OggFLAC::CUSTOM;
				opt.effort = (Instance::Options::Codec::Encode::OggFLAC::Effort)e;
			}

			cfg->SetPath("/Codec/Options/Encode/OggFLAC/Custom");

			load_bool(cfg, "verify", opt.custom.verify_);
			load_bool(cfg, "subset", opt.custom.subset_);
			load_bool(cfg, "do_mid_side", opt.custom.do_mid_side_);
			load_bool(cfg, "adaptive_mid_side", opt.custom.adaptive_mid_side_);
			load_unsigned(cfg, "blocksize", opt.custom.blocksize_);
			load_unsigned(cfg, "max_lpc_order", opt.custom.max_lpc_order_);
			load_unsigned(cfg, "qlp_coeff_precision", opt.custom.qlp_coeff_precision_);
			load_bool(cfg, "do_qlp_coeff_prec_search", opt.custom.do_qlp_coeff_prec_search_);
			load_bool(cfg, "do_escape_coding", opt.custom.do_escape_coding_); // do_escape_coding is deprecated in libFLAC
			load_bool(cfg, "do_exhaustive_model_search", opt.custom.do_exhaustive_model_search_);
			load_unsigned(cfg, "min_residual_partition_order", opt.custom.min_residual_partition_order_);
			load_unsigned(cfg, "max_residual_partition_order", opt.custom.max_residual_partition_order_);
			// rice_parameter_search_dist is deprecated in libFLAC

			load_unsigned(cfg, "serial_number", opt.custom.serial_number_);

			cfg->SetPath(path);
		}

		void Instance::load_codec_options_encode_ogg_vorbis(wxConfigBase *cfg, FUI::Codec::Options::Encode::OggVorbis &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Codec/Options/Encode/OggVorbis");

			load_unsigned(cfg, "serial_number", opt.serial_number_);
			load_double(cfg, "quality", opt.quality_);

			cfg->SetPath(path);
		}

		void Instance::load_codec_options_encode_wave(wxConfigBase *cfg, FUI::Codec::Options::Encode::RiffWave &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Codec/Options/Encode/RiffWave");

			// no options (yet)
			(void)opt;

			cfg->SetPath(path);
		}

		void Instance::load_tag(wxConfigBase *cfg)
		{
			load_tag_options(cfg);
		}

		void Instance::load_tag_options(wxConfigBase *cfg)
		{
			load_tag_options_decode(cfg);
			load_tag_options_encode(cfg);
		}

		void Instance::load_tag_options_decode(wxConfigBase *cfg)
		{
			load_tag_options_decode_id3(cfg, options_.tag.decode.id3);
			load_tag_options_decode_null(cfg, options_.tag.decode.null);
			load_tag_options_decode_vorbis_comment(cfg, options_.tag.decode.vorbis_comment);
		}

		void Instance::load_tag_options_decode_id3(wxConfigBase *cfg, FUI::Tag::Options::Decode::ID3 &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Tag/Options/Decode/ID3");

			load_bool(cfg, "v1", opt.v1_);
			load_bool(cfg, "v2", opt.v2_);
			load_string(cfg, "v1_encoding", opt.v1_encoding_); //@@@ validate the encoding here?

			cfg->SetPath(path);
		}

		void Instance::load_tag_options_decode_null(wxConfigBase *cfg, FUI::Tag::Options::Decode::Null &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Tag/Options/Decode/Null");

			// no options (yet)
			(void)opt;

			cfg->SetPath(path);
		}

		void Instance::load_tag_options_decode_vorbis_comment(wxConfigBase *cfg, FUI::Tag::Options::Decode::VorbisComment &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Tag/Options/Decode/VorbisComment");

			// no options (yet)
			(void)opt;

			cfg->SetPath(path);
		}

		void Instance::load_tag_options_encode(wxConfigBase *cfg)
		{
			load_tag_options_encode_id3(cfg, options_.tag.encode.id3);
			load_tag_options_encode_null(cfg, options_.tag.encode.null);
			load_tag_options_encode_vorbis_comment(cfg, options_.tag.encode.vorbis_comment);
		}

		void Instance::load_tag_options_encode_id3(wxConfigBase *cfg, FUI::Tag::Options::Encode::ID3 &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Tag/Options/Encode/ID3");

			load_bool(cfg, "v1", opt.v1_);
			load_bool(cfg, "v2", opt.v2_);
			load_string(cfg, "v1_encoding", opt.v1_encoding_); //@@@ validate the encoding here?

			cfg->SetPath(path);
		}

		void Instance::load_tag_options_encode_null(wxConfigBase *cfg, FUI::Tag::Options::Encode::Null &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Tag/Options/Encode/Null");

			// no options (yet)
			(void)opt;

			cfg->SetPath(path);
		}

		void Instance::load_tag_options_encode_vorbis_comment(wxConfigBase *cfg, FUI::Tag::Options::Encode::VorbisComment &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Tag/Options/Encode/VorbisComment");

			// no options (yet)
			(void)opt;

			cfg->SetPath(path);
		}

		void Instance::store_global(wxConfigBase *cfg)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Global");

			store_string(cfg, "output_directory", options_.global.output_directory);

			cfg->SetPath(path);
		}

		void Instance::store_codec(wxConfigBase *cfg)
		{
			store_codec_options(cfg);
		}

		void Instance::store_codec_options(wxConfigBase *cfg)
		{
			store_codec_options_decode(cfg);
			store_codec_options_encode(cfg);
		}

		void Instance::store_codec_options_decode(wxConfigBase *cfg)
		{
			store_codec_options_decode_flac(cfg, options_.codec.decode.flac);
			store_codec_options_decode_mp3(cfg, options_.codec.decode.mp3);
			store_codec_options_decode_ogg_flac(cfg, options_.codec.decode.ogg_flac);
			store_codec_options_decode_ogg_vorbis(cfg, options_.codec.decode.ogg_vorbis);
			store_codec_options_decode_wave(cfg, options_.codec.decode.wave);
		}

		void Instance::store_codec_options_decode_flac(wxConfigBase *cfg, const FUI::Codec::Options::Decode::FLAC &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Codec/Options/Decode/FLAC");

			store_bool(cfg, "verify_md5", opt.verify_md5_);

			cfg->SetPath(path);
		}

		void Instance::store_codec_options_decode_mp3(wxConfigBase *cfg, const FUI::Codec::Options::Decode::MP3 &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Codec/Options/Decode/MP3");

			// no options (yet)
			(void)opt;

			cfg->SetPath(path);
		}

		void Instance::store_codec_options_decode_ogg_flac(wxConfigBase *cfg, const FUI::Codec::Options::Decode::OggFLAC &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Codec/Options/Decode/OggFLAC");

			store_bool(cfg, "verify_md5", opt.verify_md5_);

			cfg->SetPath(path);
		}

		void Instance::store_codec_options_decode_ogg_vorbis(wxConfigBase *cfg, const FUI::Codec::Options::Decode::OggVorbis &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Codec/Options/Decode/OggVorbis");

			// no options (yet)
			(void)opt;

			cfg->SetPath(path);
		}

		void Instance::store_codec_options_decode_wave(wxConfigBase *cfg, const FUI::Codec::Options::Decode::RiffWave &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Codec/Options/Decode/RiffWave");

			// no options (yet)
			(void)opt;

			cfg->SetPath(path);
		}

		void Instance::store_codec_options_encode(wxConfigBase *cfg)
		{
			store_codec_options_encode_flac(cfg, options_.codec.encode.flac);
			store_codec_options_encode_mp3(cfg, options_.codec.encode.mp3);
			store_codec_options_encode_ogg_flac(cfg, options_.codec.encode.ogg_flac);
			store_codec_options_encode_ogg_vorbis(cfg, options_.codec.encode.ogg_vorbis);
			store_codec_options_encode_wave(cfg, options_.codec.encode.wave);
		}

		void Instance::store_codec_options_encode_flac(wxConfigBase *cfg, const Instance::Options::Codec::Encode::FLAC &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Codec/Options/Encode/FLAC");

			store_unsigned(cfg, "effort", opt.effort);

			cfg->SetPath("/Codec/Options/Encode/FLAC/Custom");

			store_bool(cfg, "verify", opt.custom.verify_);
			store_bool(cfg, "subset", opt.custom.subset_);
			store_bool(cfg, "do_mid_side", opt.custom.do_mid_side_);
			store_bool(cfg, "adaptive_mid_side", opt.custom.adaptive_mid_side_);
			store_unsigned(cfg, "blocksize", opt.custom.blocksize_);
			store_unsigned(cfg, "max_lpc_order", opt.custom.max_lpc_order_);
			store_unsigned(cfg, "qlp_coeff_precision", opt.custom.qlp_coeff_precision_);
			store_bool(cfg, "do_qlp_coeff_prec_search", opt.custom.do_qlp_coeff_prec_search_);
			store_bool(cfg, "do_escape_coding", opt.custom.do_escape_coding_); // do_escape_coding is deprecated in libFLAC
			store_bool(cfg, "do_exhaustive_model_search", opt.custom.do_exhaustive_model_search_);
			store_unsigned(cfg, "min_residual_partition_order", opt.custom.min_residual_partition_order_);
			store_unsigned(cfg, "max_residual_partition_order", opt.custom.max_residual_partition_order_);
			// rice_parameter_search_dist is deprecated in libFLAC

			cfg->SetPath(path);
		}

		void Instance::store_codec_options_encode_mp3(wxConfigBase *cfg, const FUI::Codec::Options::Encode::MP3 &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Codec/Options/Encode/MP3");

			store_unsigned(cfg, "target_bps", opt.target_bps_);

			cfg->SetPath(path);
		}

		void Instance::store_codec_options_encode_ogg_flac(wxConfigBase *cfg, const Instance::Options::Codec::Encode::OggFLAC &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Codec/Options/Encode/OggFLAC");

			store_unsigned(cfg, "effort", opt.effort);

			cfg->SetPath("/Codec/Options/Encode/OggFLAC/Custom");

			store_bool(cfg, "verify", opt.custom.verify_);
			store_bool(cfg, "subset", opt.custom.subset_);
			store_bool(cfg, "do_mid_side", opt.custom.do_mid_side_);
			store_bool(cfg, "adaptive_mid_side", opt.custom.adaptive_mid_side_);
			store_unsigned(cfg, "blocksize", opt.custom.blocksize_);
			store_unsigned(cfg, "max_lpc_order", opt.custom.max_lpc_order_);
			store_unsigned(cfg, "qlp_coeff_precision", opt.custom.qlp_coeff_precision_);
			store_bool(cfg, "do_qlp_coeff_prec_search", opt.custom.do_qlp_coeff_prec_search_);
			store_bool(cfg, "do_escape_coding", opt.custom.do_escape_coding_); // do_escape_coding is deprecated in libFLAC
			store_bool(cfg, "do_exhaustive_model_search", opt.custom.do_exhaustive_model_search_);
			store_unsigned(cfg, "min_residual_partition_order", opt.custom.min_residual_partition_order_);
			store_unsigned(cfg, "max_residual_partition_order", opt.custom.max_residual_partition_order_);
			// rice_parameter_search_dist is deprecated in libFLAC

			store_unsigned(cfg, "serial_number", opt.custom.serial_number_);

			cfg->SetPath(path);
		}

		void Instance::store_codec_options_encode_ogg_vorbis(wxConfigBase *cfg, const FUI::Codec::Options::Encode::OggVorbis &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Codec/Options/Encode/OggVorbis");

			store_unsigned(cfg, "serial_number", opt.serial_number_);
			store_double(cfg, "quality", opt.quality_);

			cfg->SetPath(path);
		}

		void Instance::store_codec_options_encode_wave(wxConfigBase *cfg, const FUI::Codec::Options::Encode::RiffWave &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Codec/Options/Encode/RiffWave");

			// no options (yet)
			(void)opt;

			cfg->SetPath(path);
		}

		void Instance::store_tag(wxConfigBase *cfg)
		{
			store_tag_options(cfg);
		}

		void Instance::store_tag_options(wxConfigBase *cfg)
		{
			store_tag_options_decode(cfg);
			store_tag_options_encode(cfg);
		}

		void Instance::store_tag_options_decode(wxConfigBase *cfg)
		{
			store_tag_options_decode_id3(cfg, options_.tag.decode.id3);
			store_tag_options_decode_null(cfg, options_.tag.decode.null);
			store_tag_options_decode_vorbis_comment(cfg, options_.tag.decode.vorbis_comment);
		}

		void Instance::store_tag_options_decode_id3(wxConfigBase *cfg, const FUI::Tag::Options::Decode::ID3 &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Tag/Options/Decode/ID3");

			store_bool(cfg, "v1", opt.v1_);
			store_bool(cfg, "v2", opt.v2_);
			store_string(cfg, "v1_encoding", opt.v1_encoding_);

			cfg->SetPath(path);
		}

		void Instance::store_tag_options_decode_null(wxConfigBase *cfg, const FUI::Tag::Options::Decode::Null &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Tag/Options/Decode/Null");

			// no options (yet)
			(void)opt;

			cfg->SetPath(path);
		}

		void Instance::store_tag_options_decode_vorbis_comment(wxConfigBase *cfg, const FUI::Tag::Options::Decode::VorbisComment &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Tag/Options/Decode/VorbisComment");

			// no options (yet)
			(void)opt;

			cfg->SetPath(path);
		}

		void Instance::store_tag_options_encode(wxConfigBase *cfg)
		{
			store_tag_options_encode_id3(cfg, options_.tag.encode.id3);
			store_tag_options_encode_null(cfg, options_.tag.encode.null);
			store_tag_options_encode_vorbis_comment(cfg, options_.tag.encode.vorbis_comment);
		}

		void Instance::store_tag_options_encode_id3(wxConfigBase *cfg, const FUI::Tag::Options::Encode::ID3 &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Tag/Options/Encode/ID3");

			store_bool(cfg, "v1", opt.v1_);
			store_bool(cfg, "v2", opt.v2_);
			store_string(cfg, "v1_encoding", opt.v1_encoding_);

			cfg->SetPath(path);
		}

		void Instance::store_tag_options_encode_null(wxConfigBase *cfg, const FUI::Tag::Options::Encode::Null &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Tag/Options/Encode/Null");

			// no options (yet)
			(void)opt;

			cfg->SetPath(path);
		}

		void Instance::store_tag_options_encode_vorbis_comment(wxConfigBase *cfg, const FUI::Tag::Options::Encode::VorbisComment &opt)
		{
			wxString path = cfg->GetPath();
			cfg->SetPath("/Tag/Options/Encode/VorbisComment");

			// no options (yet)
			(void)opt;

			cfg->SetPath(path);
		}

		void Instance::load_bool(wxConfigBase *cfg, const char *key, bool &value)
		{
			(void) cfg->Read(key, &value);
		}

		void Instance::load_double(wxConfigBase *cfg, const char *key, double &value)
		{
			(void) cfg->Read(key, &value);
		}

		void Instance::load_string(wxConfigBase *cfg, const char *key, std::string &value)
		{
			wxString val;
			if(cfg->Read(key, &val))
				value = val.c_str();
		}

		void Instance::load_unsigned(wxConfigBase *cfg, const char *key, unsigned &value)
		{
			long x;
			(void) cfg->Read(key, &x);
			if(x >= 0)
				value = (unsigned)x;
		}

		void Instance::store_bool(wxConfigBase *cfg, const char *key, bool value)
		{
			(void) cfg->Write(key, value);
		}

		void Instance::store_double(wxConfigBase *cfg, const char *key, double value)
		{
			(void) cfg->Write(key, value);
		}

		void Instance::store_string(wxConfigBase *cfg, const char *key, const std::string &value)
		{
			(void) cfg->Write(key, value.c_str());
		}

		void Instance::store_unsigned(wxConfigBase *cfg, const char *key, unsigned value)
		{
			long x = (long)value;
			(void) cfg->Write(key, x);
		}

	};
};
