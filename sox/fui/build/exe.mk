#  FUI - FLAC User Interface
#  Copyright (C) 2002,2003  Josh Coalson
#
#  This program is part of FUI; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#
# GNU makefile fragment for building an executable
#

include $(topdir)/build/config.mk

ifeq ($(DARWIN_BUILD),yes)
CC          = cc
CCC         = c++
else
CC          = gcc
CCC         = g++
endif
LINK        = $(CC) $(LINKAGE)
OBJPATH     = $(topdir)/obj
BINPATH     = $(OBJPATH)/$(BUILD)/bin
LIBPATH     = $(OBJPATH)/$(BUILD)/lib
DEBUG_BINPATH   = $(OBJPATH)/debug/bin
DEBUG_LIBPATH   = $(OBJPATH)/debug/lib
RELEASE_BINPATH = $(OBJPATH)/release/bin
RELEASE_LIBPATH = $(OBJPATH)/release/lib
PROGRAM         = $(BINPATH)/$(PROGRAM_NAME)
DEBUG_PROGRAM   = $(DEBUG_BINPATH)/$(PROGRAM_NAME)
RELEASE_PROGRAM = $(RELEASE_BINPATH)/$(PROGRAM_NAME)

debug   : CFLAGS = -g -O0 -DDEBUG $(CONFIG_CFLAGS) $(DEBUG_CFLAGS) -Wall -W -DVERSION=$(VERSION) $(DEFINES) $(INCLUDES)
valgrind: CFLAGS = -g -O0 -DDEBUG $(CONFIG_CFLAGS) $(DEBUG_CFLAGS) -DFUI__VALGRIND_TESTING -Wall -W -DVERSION=$(VERSION) $(DEFINES) $(INCLUDES)
release : CFLAGS = -O3 -fomit-frame-pointer -funroll-loops -finline-functions -DNDEBUG $(CONFIG_CFLAGS) $(RELEASE_CFLAGS) -Wall -W -Winline -DVERSION=$(VERSION) $(DEFINES) $(INCLUDES)

LFLAGS  = -L$(LIBPATH)

DEBUG_OBJS = $(SRCS_C:%.c=%.debug.o) $(SRCS_CC:%.cc=%.debug.o) $(SRCS_CPP:%.cpp=%.debug.o)
RELEASE_OBJS = $(SRCS_C:%.c=%.release.o) $(SRCS_CC:%.cc=%.release.o) $(SRCS_CPP:%.cpp=%.release.o)

debug   : $(DEBUG_PROGRAM)
valgrind: $(DEBUG_PROGRAM)
release : $(RELEASE_PROGRAM)

# by default on OS X we link with static libs as much as possible

$(DEBUG_PROGRAM) : $(DEBUG_OBJS)
ifeq ($(DARWIN_BUILD),yes)
	$(LINK) -o $@ $(DEBUG_OBJS) $(EXPLICIT_LIBS)
else
	$(LINK) -o $@ $(DEBUG_OBJS) $(LFLAGS) $(LIBS)
endif

$(RELEASE_PROGRAM) : $(RELEASE_OBJS)
ifeq ($(DARWIN_BUILD),yes)
	$(LINK) -o $@ $(RELEASE_OBJS) $(EXPLICIT_LIBS)
else
	$(LINK) -o $@ $(RELEASE_OBJS) $(LFLAGS) $(LIBS)
endif

%.debug.o %.release.o : %.c
	$(CC) $(CFLAGS) -c $< -o $@
%.debug.o %.release.o : %.cc
	$(CCC) $(CFLAGS) -c $< -o $@
%.debug.o %.release.o : %.cpp
	$(CCC) $(CFLAGS) -c $< -o $@
%.debug.i %.release.i : %.c
	$(CC) $(CFLAGS) -E $< -o $@
%.debug.i %.release.i : %.cc
	$(CCC) $(CFLAGS) -E $< -o $@
%.debug.i %.release.i : %.cpp
	$(CCC) $(CFLAGS) -E $< -o $@

.PHONY : clean
clean :
	-rm -f $(DEBUG_OBJS) $(RELEASE_OBJS) $(OBJPATH)/*/bin/$(PROGRAM_NAME)

.PHONY : depend
depend:
	makedepend -fMakefile.lite -- $(CFLAGS) $(INCLUDES) -- *.c *.cc *.cpp
