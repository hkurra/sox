#  FUI - FLAC User Interface
#  Copyright (C) 2002,2003  Josh Coalson
#
#  This program is part of FUI; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#
# debug/release selection
#

DEFAULT_BUILD = release

debug    : BUILD = debug
valgrind : BUILD = debug
release  : BUILD = release

# override LINKAGE on OS X until we figure out how to get 'cc -static' to work
ifeq ($(DARWIN_BUILD),yes)
LINKAGE = 
else
debug    : LINKAGE = -static
valgrind : LINKAGE = -dynamic
release  : LINKAGE = -static
endif

all default: $(DEFAULT_BUILD)

#
# GNU makefile fragment for emulating stuff normally done by configure
#

VERSION=\"0.0\"

ifeq ($(DARWIN_BUILD),yes)
CONFIG_CFLAGS=-DHAVE_INTTYPES_H -DHAVE_ICONV -DFLAC__HAS_OGG -DFUI__OLD_STRSTREAM
else
CONFIG_CFLAGS=-DHAVE_INTTYPES_H -DHAVE_ICONV -DFLAC__HAS_OGG
endif

ICONV_INCLUDE_DIR=$(HOME)/local.i18n/include
ICONV_LIB_DIR=$(HOME)/local.i18n/lib

OGG_INCLUDE_DIR=$(HOME)/local/include
OGG_LIB_DIR=$(HOME)/local/lib
